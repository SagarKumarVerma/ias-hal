﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraLeaveAccrual
    Dim GrpId As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True

    'Dim adap, adap1, adap2 As SqlDataAdapter
    'Dim adapA, adapA1 As OleDbDataAdapter
    'Dim ds, ds1, ds2 As DataSet
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            Me.EmployeeGroup1TableAdapter1.Fill(Me.SSSDBDataSet.EmployeeGroup1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        Else
            EmployeeGroupTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.EmployeeGroupTableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup)
            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
        Common.SetGridFont(GridView2, New Font("Tahoma", 11))
    End Sub
   
    Private Sub XtraLeaveAccrual_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        If Common.servername = "Access" Then
            Me.EmployeeGroup1TableAdapter1.Fill(Me.SSSDBDataSet.EmployeeGroup1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            GridControl1.DataSource = SSSDBDataSet.EmployeeGroup1
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        Else
            'EmployeeGroupTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.EmployeeGroupTableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup)
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            GridControl1.DataSource = SSSDBDataSet.EmployeeGroup
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin

        setDefault()
        'TextEditYear.Text = Now.Year
        'If Common.IsNepali = "Y" Then

        '    Dim tmp As DateTime
        '    Dim DC As New DateConverter()
        '    Try
        '        Dim doj As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, 1))
        '        ComboNepaliYear.EditValue = doj.Year
        '    Catch ex As Exception
        '        XtraMessageBox.Show(ulf, "<size=10>Invalid Year</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        ComboNepaliYear.Select()
        '        Exit Sub
        '    End Try
        'End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet
        Dim sSql As String = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION from tblLeaveMaster"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            ds = New DataSet
            adap.Fill(ds)
        End If

        TxtLeave1.Visible = False
        TxtLeave2.Visible = False
        TxtLeave3.Visible = False
        TxtLeave4.Visible = False
        TxtLeave5.Visible = False
        TxtLeave6.Visible = False
        TxtLeave7.Visible = False
        TxtLeave8.Visible = False
        TxtLeave9.Visible = False
        TxtLeave10.Visible = False
        TxtLeave11.Visible = False
        TxtLeave12.Visible = False
        TxtLeave13.Visible = False
        TxtLeave14.Visible = False
        TxtLeave15.Visible = False
        TxtLeave16.Visible = False
        TxtLeave17.Visible = False
        TxtLeave18.Visible = False
        TxtLeave19.Visible = False
        TxtLeave20.Visible = False

        For i As Integer = 1 To ds.Tables(0).Rows.Count
            If i < 8 Then
                Dim index As Integer = Convert.ToInt16(ds.Tables(0).Rows(i - 1).Item("LEAVEFIELD").ToString.TrimStart("L"))
                Dim lbl = PanelControl1.Controls("lbLeave" & index.ToString)
                lbl.Visible = True
                lbl.Text = ds.Tables(0).Rows(i - 1).Item("LEAVEDESCRIPTION").ToString.Trim
                Dim txt = PanelControl1.Controls("TxtLeave" & index.ToString)
                txt.Visible = True
            End If
            If i > 7 And i < 15 Then
                'MsgBox(i)
                PanelControl2.Visible = True
                Dim index As Integer = Convert.ToInt16(ds.Tables(0).Rows(i - 1).Item("LEAVEFIELD").ToString.TrimStart("L"))
                Dim lbl = PanelControl2.Controls("lbLeave" & index.ToString)
                lbl.Visible = True
                lbl.Text = ds.Tables(0).Rows(i - 1).Item("LEAVEDESCRIPTION").ToString.Trim
                Dim txt = PanelControl2.Controls("TxtLeave" & index.ToString)
                txt.Visible = True
            End If
            If i > 14 Then
                PanelControl2.Visible = True
                Dim index As Integer = Convert.ToInt16(ds.Tables(0).Rows(i - 1).Item("LEAVEFIELD").ToString.TrimStart("L"))
                Dim lbl = PanelControl3.Controls("lbLeave" & index.ToString)
                lbl.Visible = True
                lbl.Text = ds.Tables(0).Rows(i - 1).Item("LEAVEDESCRIPTION").ToString.Trim
                Dim txt = PanelControl3.Controls("TxtLeave" & index.ToString)
                txt.Visible = True
            End If
        Next
        updateEmpGrpGrid()
    End Sub
    Private Sub updateEmpGrpGrid()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet
        Dim sSql As String = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION from tblLeaveMaster"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            ds = New DataSet
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Leave Master is Empty. Please create the Leave Master</size>", "<size=9>Error</size>")
            Me.Enabled = False
            Exit Sub
        End If
        Dim s As String = ""
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Employee Group")
        For i As Integer = 1 To ds.Tables(0).Rows.Count
            s = s & "L" & i.ToString("00") & "_ADD , "
            dt.Columns.Add(ds.Tables(0).Rows(i - 1).Item("LEAVECODE").ToString.Trim)
        Next

        Dim adap1 As SqlDataAdapter
        Dim adapA1 As OleDbDataAdapter
        Dim ds1 As DataSet = New DataSet
        Dim sSql1 As String = "select GroupId," & s.Substring(0, s.Length - 2) & " from EmployeeGroupLeaveLedger where  LYEAR=" & TextEditYear.Text.Trim & ""
        'MsgBox(s.Substring(0, s.Length - 2))
        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(sSql1, Common.con)
            adap1.Fill(ds1)
        End If
        For Each dr As DataRow In ds1.Tables(0).Rows
            dt.Rows.Add(dr.ItemArray)
        Next
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl2.DataSource = dt
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            'Dim f As XtraEmpGroupEdit = CType(GridView1.GetRow(selectionRow), XtraEmpGroupEdit)
            'Dim f As GridView1.SSSDBDataSetTableAdapters.row = CType(GridView1.GetRow(selectionRow), GridRow)
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GroupId"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit1.CheckedChanged
        If CheckEdit1.Checked = True Then
            PopupContainerEdit1.Visible = True
            LabelControl1.Text = "Select Group"
            'TextEdit2.Visible = False
            LookUpEdit1.Visible = False
            LabelControl3.Visible = False
            LabelControl4.Visible = False
            LabelControl5.Visible = False
            LabelControl6.Visible = False
            LabelControl7.Visible = False
            LabelControl8.Visible = False
            LabelControl9.Visible = False
            LabelControl10.Visible = False
            LabelControl11.Visible = False
            LabelControl12.Visible = False
            LabelControl13.Visible = False
            LabelControl14.Visible = False
        ElseIf CheckEdit1.Checked = False Then
            PopupContainerEdit1.Visible = False
            LabelControl1.Text = "Select Paycode"
            'TextEdit2.Visible = True
            LookUpEdit1.Visible = True
            LabelControl3.Visible = True
            LabelControl4.Visible = True
            LabelControl5.Visible = True
            LabelControl6.Visible = True
            LabelControl7.Visible = True
            LabelControl8.Visible = True
            LabelControl9.Visible = True
            LabelControl10.Visible = True
            LabelControl11.Visible = True
            LabelControl12.Visible = True
            LabelControl13.Visible = True
            LabelControl14.Visible = True
        End If
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        setSelectValue()
        leaveFlage = True
    End Sub
    Private Sub TextEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditYear.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        setSelectValue()
        leaveFlage = True
    End Sub
    Private Sub setSelectValue()
        TxtLeave1.Select()
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet = New DataSet
        Dim sSql, sSql1 As String
        If Common.IsNepali = "Y" Then
            Dim tmp As DateTime
            Dim DC As New DateConverter()
            Try
                'tmp = DC.ToAD(New Date(ComboNepaliYear.EditValue.ToString.Trim, Now.Month, 1))
                tmp = DC.ToAD(ComboNepaliYear.EditValue.ToString.Trim & "-" & Now.Month & "-" & 1)
                TextEditYear.Text = tmp.Year
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Year</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliYear.Select()
                Exit Sub
            End Try
        End If
        If CheckEdit1.Checked = True Then
            Dim EmpGrp() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
            If EmpGrp.Length = 1 Then
                sSql1 = "select L01_ADD,L02_ADD,L03_ADD,L04_ADD,L05_ADD,L06_ADD,L07_ADD,L08_ADD,L09_ADD,L10_ADD,L11_ADD,L12_ADD,L13_ADD,L14_ADD,L15_ADD,L16_ADD,L17_ADD,L18_ADD,L19_ADD,L20_ADD from EmployeeGroupLeaveLedger where GroupId='" & PopupContainerEdit1.EditValue.Trim & "' and LYEAR=" & TextEditYear.Text.Trim & ""
            Else
                setDefault()
                Exit Sub
            End If
        Else
            sSql1 = "select L01_ADD,L02_ADD,L03_ADD,L04_ADD,L05_ADD,L06_ADD,L07_ADD,L08_ADD,L09_ADD,L10_ADD,L11_ADD,L12_ADD,L13_ADD,L14_ADD,L15_ADD,L16_ADD,L17_ADD,L18_ADD,L19_ADD,L20_ADD from tblLeaveLedger where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and LYEAR=" & TextEditYear.Text.Trim & ""
        End If
        If Common.servername = "Access" Then
            'sSql = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
            If CheckEdit2.Checked = True Then
                sSql = "select T.EMPNAME, T.PRESENTCARDNO, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, C.CATAGORYNAME as CATName, T.DESIGNATION  from TblEmployee T, EmployeeGroup E,  tblDepartment D, tblCatagory C where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            End If
            adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
            adapA1.Fill(ds1)
        Else
            If CheckEdit2.Checked = True Then
                sSql = "select EMPNAME, PRESENTCARDNO, EmpGrp = (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId), DEPName = (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE), CATName = (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT), DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            adap1 = New SqlDataAdapter(sSql1, Common.con)
            adap1.Fill(ds1)
        End If
        If CheckEdit2.Checked = True Then
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "<size=9>Error</size>")
                    LookUpEdit1.Select()
                    setDefault()
                    Exit Sub
                End If
            Else
                LabelControl4.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
                LabelControl6.Text = ds.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString.Trim
                LabelControl8.Text = ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim
                LabelControl10.Text = ds.Tables(0).Rows(0).Item("DEPName").ToString.Trim
                LabelControl12.Text = ds.Tables(0).Rows(0).Item("CATName").ToString.Trim
                LabelControl14.Text = ds.Tables(0).Rows(0).Item("DESIGNATION").ToString.Trim
            End If
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            TxtLeave1.Text = ds1.Tables(0).Rows(0).Item("L01_ADD").ToString()
            TxtLeave2.Text = ds1.Tables(0).Rows(0).Item("L02_ADD").ToString()
            TxtLeave3.Text = ds1.Tables(0).Rows(0).Item("L03_ADD").ToString()
            TxtLeave4.Text = ds1.Tables(0).Rows(0).Item("L04_ADD").ToString()
            TxtLeave5.Text = ds1.Tables(0).Rows(0).Item("L05_ADD").ToString()
            TxtLeave6.Text = ds1.Tables(0).Rows(0).Item("L06_ADD").ToString()
            TxtLeave7.Text = ds1.Tables(0).Rows(0).Item("L07_ADD").ToString()
            TxtLeave8.Text = ds1.Tables(0).Rows(0).Item("L08_ADD").ToString()
            TxtLeave9.Text = ds1.Tables(0).Rows(0).Item("L09_ADD").ToString()
            TxtLeave10.Text = ds1.Tables(0).Rows(0).Item("L10_ADD").ToString()
            TxtLeave11.Text = ds1.Tables(0).Rows(0).Item("L11_ADD").ToString()
            TxtLeave12.Text = ds1.Tables(0).Rows(0).Item("L12_ADD").ToString()
            TxtLeave13.Text = ds1.Tables(0).Rows(0).Item("L13_ADD").ToString()
            TxtLeave14.Text = ds1.Tables(0).Rows(0).Item("L14_ADD").ToString()
            TxtLeave15.Text = ds1.Tables(0).Rows(0).Item("L15_ADD").ToString()
            TxtLeave16.Text = ds1.Tables(0).Rows(0).Item("L16_ADD").ToString()
            TxtLeave17.Text = ds1.Tables(0).Rows(0).Item("L17_ADD").ToString()
            TxtLeave18.Text = ds1.Tables(0).Rows(0).Item("L18_ADD").ToString()
            TxtLeave19.Text = ds1.Tables(0).Rows(0).Item("L19_ADD").ToString()
            TxtLeave20.Text = ds1.Tables(0).Rows(0).Item("L20_ADD").ToString()
        Else
            TxtLeave1.Text = "000.00"
            TxtLeave2.Text = "000.00"
            TxtLeave3.Text = "000.00"
            TxtLeave4.Text = "000.00"
            TxtLeave5.Text = "000.00"
            TxtLeave6.Text = "000.00"
            TxtLeave7.Text = "000.00"
            TxtLeave8.Text = "000.00"
            TxtLeave9.Text = "000.00"
            TxtLeave10.Text = "000.00"
            TxtLeave11.Text = "000.00"
            TxtLeave12.Text = "000.00"
            TxtLeave13.Text = "000.00"
            TxtLeave14.Text = "000.00"
            TxtLeave15.Text = "000.00"
            TxtLeave16.Text = "000.00"
            TxtLeave17.Text = "000.00"
            TxtLeave18.Text = "000.00"
            TxtLeave19.Text = "000.00"
            TxtLeave20.Text = "000.00"
        End If
    End Sub
    Private Sub setDefault()
        TextEditYear.Text = Now.Year
        TextEdit2.Text = ""
        LookUpEdit1.EditValue = ""
        PopupContainerEdit1.EditValue = ""
        LabelControl4.Text = ""
        LabelControl6.Text = ""
        LabelControl8.Text = ""
        LabelControl10.Text = ""
        LabelControl12.Text = ""
        LabelControl14.Text = ""

        TxtLeave1.Text = "000.00"
        TxtLeave2.Text = "000.00"
        TxtLeave3.Text = "000.00"
        TxtLeave4.Text = "000.00"
        TxtLeave5.Text = "000.00"
        TxtLeave6.Text = "000.00"
        TxtLeave7.Text = "000.00"
        TxtLeave8.Text = "000.00"
        TxtLeave9.Text = "000.00"
        TxtLeave10.Text = "000.00"
        TxtLeave11.Text = "000.00"
        TxtLeave12.Text = "000.00"
        TxtLeave13.Text = "000.00"
        TxtLeave14.Text = "000.00"
        TxtLeave15.Text = "000.00"
        TxtLeave16.Text = "000.00"
        TxtLeave17.Text = "000.00"
        TxtLeave18.Text = "000.00"
        TxtLeave19.Text = "000.00"
        TxtLeave20.Text = "000.00"
        If Common.IsNepali = "Y" Then
            TextEditYear.Visible = False
            ComboNepaliYear.Visible = True
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(Now.Year, Now.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
        Else
            TextEditYear.Visible = True
            ComboNepaliYear.Visible = False
        End If
        GridView1.ClearSelection()
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet = New DataSet
        Dim ds2 As DataSet = New DataSet
        Dim sSql As String
        If Common.IsNepali = "Y" Then
            Dim tmp As DateTime
            Dim DC As New DateConverter()
            Try
                'tmp = DC.ToAD(New Date(ComboNepaliYear.EditValue, Now.Month, 1))
                tmp = DC.ToAD(ComboNepaliYear.EditValue & "-" & Now.Month & "-" & 1)
                TextEditYear.Text = tmp.Year
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Year</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliYear.Select()
                Exit Sub
            End Try
        End If
        If CheckEdit1.Checked = True Then
            If PopupContainerEdit1.EditValue = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please select Employee Group</size>", "<size=9>Error</size>")
                PopupContainerEdit1.Select()
                Exit Sub
            End If
            If XtraMessageBox.Show(ulf, "<size=10>Opening balance will be saved for all Employees belong to selected Employee Group " & vbCrLf & "Are you sure to proceed? </size>", "<size=9>Confirmation</size>", _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim EmpGrp() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                End If
                For i As Integer = 0 To EmpGrp.Length - 1
                    'MsgBox(EmpGrp(i).ToString.Trim.Length)
                    Dim sSqlX As String = "SELECT GroupId from EmployeeGroupLeaveLedger WHERE GroupId = '" & EmpGrp(i).ToString.Trim & "' and  LYEAR=" & TextEditYear.Text.Trim & ""
                    ds = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSqlX, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSqlX, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        sSql = "Update EmployeeGroupLeaveLedger set L01_ADD = " & TxtLeave1.Text.Trim & " ,L02_ADD = " & TxtLeave2.Text.Trim & " ,L03_ADD = " & TxtLeave3.Text.Trim & " ,L04_ADD = " & TxtLeave4.Text.Trim & " ,L05_ADD = " & TxtLeave5.Text.Trim & " ,L06_ADD = " & TxtLeave6.Text.Trim & " ,L07_ADD = " & TxtLeave7.Text.Trim & " ,L08_ADD = " & TxtLeave8.Text.Trim & " ,L09_ADD = " & TxtLeave9.Text.Trim & " ,L10_ADD = " & TxtLeave10.Text.Trim & " ,L11_ADD = " & TxtLeave11.Text.Trim & " ,L12_ADD = " & TxtLeave12.Text.Trim & " ,L13_ADD = " & TxtLeave13.Text.Trim & " ,L14_ADD = " & TxtLeave14.Text.Trim & " ,L15_ADD = " & TxtLeave15.Text.Trim & " ,L16_ADD = " & TxtLeave16.Text.Trim & " ,L17_ADD = " & TxtLeave17.Text.Trim & " ,L18_ADD = " & TxtLeave18.Text.Trim & " ,L19_ADD = " & TxtLeave19.Text.Trim & " ,L20_ADD = " & TxtLeave20.Text.Trim & " where GroupId = '" & EmpGrp(i).ToString.Trim & "' and  LYEAR=" & TextEditYear.Text.Trim & ""
                    Else
                        sSql = "insert into EmployeeGroupLeaveLedger (GroupId, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, L01_CASH, L02_CASH, L03_CASH, L04_CASH, L05_CASH, L06_CASH, L07_CASH, L08_CASH, L09_CASH, L10_CASH, L11_CASH, L12_CASH, L13_CASH, L14_CASH, L15_CASH, L16_CASH, L17_CASH, L18_CASH, L19_CASH, L20_CASH, LYEAR) VALUES ('" & EmpGrp(i).ToString.Trim & "', " & TxtLeave1.Text.Trim & "," & TxtLeave2.Text.Trim & "," & TxtLeave3.Text.Trim & "," & TxtLeave4.Text.Trim & "," & TxtLeave5.Text.Trim & "," & TxtLeave6.Text.Trim & "," & TxtLeave7.Text.Trim & "," & TxtLeave8.Text.Trim & "," & TxtLeave9.Text.Trim & "," & TxtLeave10.Text.Trim & "," & TxtLeave11.Text.Trim & "," & TxtLeave12.Text.Trim & "," & TxtLeave13.Text.Trim & "," & TxtLeave14.Text.Trim & "," & TxtLeave15.Text.Trim & "," & TxtLeave16.Text.Trim & "," & TxtLeave17.Text.Trim & "," & TxtLeave18.Text.Trim & "," & TxtLeave19.Text.Trim & "," & TxtLeave20.Text.Trim & ",'N', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'" & TextEditYear.Text.Trim & "') "
                    End If
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                    End If
                    Dim sSql1 As String = "SELECT PAYCODE from TblEmployee where EmployeeGroupId='" & EmpGrp(i).ToString.Trim & "' "
                    ds1 = New DataSet
                    If Common.servername = "Access" Then
                        adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                        adapA1.Fill(ds1)
                    Else
                        adap1 = New SqlDataAdapter(sSql1, Common.con)
                        adap1.Fill(ds1)
                    End If
                    For j As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                        Dim sSql2 As String = "SELECT PAYCODE from tblLeaveLedger where PAYCODE = '" & ds1.Tables(0).Rows(j).Item("PAYCODE").ToString.Trim & "' and LYEAR= " & TextEditYear.Text.Trim & ""
                        ds2 = New DataSet
                        If Common.servername = "Access" Then
                            adapA1 = New OleDbDataAdapter(sSql2, Common.con1)
                            adapA1.Fill(ds2)
                        Else
                            adap1 = New SqlDataAdapter(sSql2, Common.con)
                            adap1.Fill(ds2)
                        End If
                        Dim sSql3 As String
                        If ds2.Tables(0).Rows.Count > 0 Then
                            sSql3 = "Update tblLeaveLedger set L01_ADD = " & TxtLeave1.Text.Trim & " ,L02_ADD = " & TxtLeave2.Text.Trim & " ,L03_ADD = " & TxtLeave3.Text.Trim & " ,L04_ADD = " & TxtLeave4.Text.Trim & " ,L05_ADD = " & TxtLeave5.Text.Trim & " ,L06_ADD = " & TxtLeave6.Text.Trim & " ,L07_ADD = " & TxtLeave7.Text.Trim & " ,L08_ADD = " & TxtLeave8.Text.Trim & " ,L09_ADD = " & TxtLeave9.Text.Trim & " ,L10_ADD = " & TxtLeave10.Text.Trim & " ,L11_ADD = " & TxtLeave11.Text.Trim & " ,L12_ADD = " & TxtLeave12.Text.Trim & " ,L13_ADD = " & TxtLeave13.Text.Trim & " ,L14_ADD = " & TxtLeave14.Text.Trim & " ,L15_ADD = " & TxtLeave15.Text.Trim & " ,L16_ADD = " & TxtLeave16.Text.Trim & " ,L17_ADD = " & TxtLeave17.Text.Trim & " ,L18_ADD = " & TxtLeave18.Text.Trim & " ,L19_ADD = " & TxtLeave19.Text.Trim & " ,L20_ADD = " & TxtLeave20.Text.Trim & " where PAYCODE = '" & ds1.Tables(0).Rows(j).Item("PAYCODE").ToString.Trim & "' and  LYEAR=" & TextEditYear.Text.Trim & ""
                        Else
                            sSql3 = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, L01_CASH, L02_CASH, L03_CASH, L04_CASH, L05_CASH, L06_CASH, L07_CASH, L08_CASH, L09_CASH, L10_CASH, L11_CASH, L12_CASH, L13_CASH, L14_CASH, L15_CASH, L16_CASH, L17_CASH, L18_CASH, L19_CASH, L20_CASH, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES ('" & ds1.Tables(0).Rows(j).Item("PAYCODE").ToString.Trim & "', " & TxtLeave1.Text.Trim & "," & TxtLeave2.Text.Trim & "," & TxtLeave3.Text.Trim & "," & TxtLeave4.Text.Trim & "," & TxtLeave5.Text.Trim & "," & TxtLeave6.Text.Trim & "," & TxtLeave7.Text.Trim & "," & TxtLeave8.Text.Trim & "," & TxtLeave9.Text.Trim & "," & TxtLeave10.Text.Trim & "," & TxtLeave11.Text.Trim & "," & TxtLeave12.Text.Trim & "," & TxtLeave13.Text.Trim & "," & TxtLeave14.Text.Trim & "," & TxtLeave15.Text.Trim & "," & TxtLeave16.Text.Trim & "," & TxtLeave17.Text.Trim & "," & TxtLeave18.Text.Trim & "," & TxtLeave19.Text.Trim & "," & TxtLeave20.Text.Trim & ",'N', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'" & TextEditYear.Text.Trim & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                        End If
                        If Common.servername = "Access" Then
                            cmd1 = New OleDbCommand(sSql3, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql3, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    Next
                Next
                If Common.servername = "Access" Then
                    Common.con1.Close()
                Else
                    Common.con.Close()
                End If
            Else
                Exit Sub
            End If
            updateEmpGrpGrid()
        Else
            sSql = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Paycode</size>", "<size=9>Error</size>")
                Else
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "<size=9>Error</size>")
                End If
                LookUpEdit1.Select()
                Exit Sub
            Else
                sSql = "SELECT PAYCODE from tblLeaveLedger where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and LYEAR= " & TextEditYear.Text.Trim & ""
                If Common.servername = "Access" Then
                    adapA1 = New OleDbDataAdapter(sSql, Common.con1)
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql, Common.con)
                    adap1.Fill(ds1)
                End If
                If ds1.Tables(0).Rows.Count > 0 Then
                    sSql = "Update tblLeaveLedger set L01_ADD = " & TxtLeave1.Text.Trim & " ,L02_ADD = " & TxtLeave2.Text.Trim & " ,L03_ADD = " & TxtLeave3.Text.Trim & " ,L04_ADD = " & TxtLeave4.Text.Trim & " ,L05_ADD = " & TxtLeave5.Text.Trim & " ,L06_ADD = " & TxtLeave6.Text.Trim & " ,L07_ADD = " & TxtLeave7.Text.Trim & " ,L08_ADD = " & TxtLeave8.Text.Trim & " ,L09_ADD = " & TxtLeave9.Text.Trim & " ,L10_ADD = " & TxtLeave10.Text.Trim & " ,L11_ADD = " & TxtLeave11.Text.Trim & " ,L12_ADD = " & TxtLeave12.Text.Trim & " ,L13_ADD = " & TxtLeave13.Text.Trim & " ,L14_ADD = " & TxtLeave14.Text.Trim & " ,L15_ADD = " & TxtLeave15.Text.Trim & " ,L16_ADD = " & TxtLeave16.Text.Trim & " ,L17_ADD = " & TxtLeave17.Text.Trim & " ,L18_ADD = " & TxtLeave18.Text.Trim & " ,L19_ADD = " & TxtLeave19.Text.Trim & " ,L20_ADD = " & TxtLeave20.Text.Trim & " where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and  LYEAR=" & TextEditYear.Text.Trim & ""
                Else
                    sSql = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, L01_CASH, L02_CASH, L03_CASH, L04_CASH, L05_CASH, L06_CASH, L07_CASH, L08_CASH, L09_CASH, L10_CASH, L11_CASH, L12_CASH, L13_CASH, L14_CASH, L15_CASH, L16_CASH, L17_CASH, L18_CASH, L19_CASH, L20_CASH, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES ('" & LookUpEdit1.EditValue.ToString.Trim & "', " & TxtLeave1.Text.Trim & "," & TxtLeave2.Text.Trim & "," & TxtLeave3.Text.Trim & "," & TxtLeave4.Text.Trim & "," & TxtLeave5.Text.Trim & "," & TxtLeave6.Text.Trim & "," & TxtLeave7.Text.Trim & "," & TxtLeave8.Text.Trim & "," & TxtLeave9.Text.Trim & "," & TxtLeave10.Text.Trim & "," & TxtLeave11.Text.Trim & "," & TxtLeave12.Text.Trim & "," & TxtLeave13.Text.Trim & "," & TxtLeave14.Text.Trim & "," & TxtLeave15.Text.Trim & "," & TxtLeave16.Text.Trim & "," & TxtLeave17.Text.Trim & "," & TxtLeave18.Text.Trim & "," & TxtLeave19.Text.Trim & "," & TxtLeave20.Text.Trim & ",'N', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'" & TextEditYear.Text.Trim & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
            End If
        End If
        Common.LogPost("Leave Accrual")
        XtraMessageBox.Show(ulf, "<size=10>Saved successfully</size>", "<size=9>Success</size>")
        setDefault()
    End Sub
    Private Sub ComboNepaliYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYear.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        setSelectValue()
        leaveFlage = True
    End Sub
End Class
