﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraDataMaintenanceEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim comclass As Common = New Common
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView2, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraDataMaintenanceEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'con1 = New OleDbConnection(ConnectionString)
            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            'GridControl1.DataSource = SSSDBDataSet.tblShiftMaster1
            'GridControl2.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            'GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            'con = New SqlConnection(ConnectionString)
            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            'GridControl1.DataSource = SSSDBDataSet.tblShiftMaster
            'GridControl2.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            'GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster
        End If
        If Common.ShiftChange = "Y" Then
            GroupControlShift.Enabled = True
        Else
            GroupControlShift.Enabled = False
        End If
        If Common.Manual_Attendance = "Y" Then
            GroupControl2.Enabled = True
        Else
            GroupControl2.Enabled = False
        End If
        If Common.LeaveApplication = "Y" Then
            GroupControl3.Enabled = True
        Else
            GroupControl3.Enabled = False
        End If
        setdefault()
        LoadPunchHistoryGrid()
        LoadLeaveQuota()
    End Sub
    Private Sub setdefault()
        DateEdit1.DateTime = XtraDataMaintenance.DateEditAtt
        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEdit1.Visible = False
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDate.EditValue = dojTmp(2)
        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEdit1.Visible = True
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "SELECT * from tblTimeRegister where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and DateOFFICE = '" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
        If Common.servername = "Access" Then
            sSql = "SELECT * from tblTimeRegister where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') = '" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows(0).Item("STATUS").ToString = "Off" Then
            CheckEditOff.Checked = True
            LabelControl1.Visible = False
            GridLookUpEdit1.Visible = False
        Else
            CheckEditShift.Checked = True
            LabelControl1.Visible = True
            GridLookUpEdit1.Visible = True
            GridLookUpEdit1.EditValue = ds.Tables(0).Rows(0).Item("SHIFT").ToString
        End If
        If ds.Tables(0).Rows(0).Item("IN1").ToString = "" Then
            TextEdit1.Text = ""
            TextEdit3.Text = ""
        Else
            TextEdit1.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("IN1")).Date.ToString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern())
            TextEdit3.Text = ds.Tables(0).Rows(0).Item("IN1").ToString.Split(" ")(1).Substring(0, 5)
        End If
        If ds.Tables(0).Rows(0).Item("OUT2").ToString = "" Then
            TextEdit2.Text = ""
            TextEdit4.Text = ""
        Else
            TextEdit2.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OUT2")).Date.ToString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern())
            TextEdit4.Text = ds.Tables(0).Rows(0).Item("OUT2").ToString.Split(" ")(1).Substring(0, 5)
        End If

        If ds.Tables(0).Rows(0).Item("OUT1").ToString = "" Then
            TextEdit7.Text = ""
            TextEdit5.Text = ""
        Else
            TextEdit7.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OUT1")).Date.ToString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern())
            TextEdit5.Text = ds.Tables(0).Rows(0).Item("OUT1").ToString.Split(" ")(1).Substring(0, 5)
        End If

        If ds.Tables(0).Rows(0).Item("IN2").ToString = "" Then
            TextEdit8.Text = ""
            TextEdit6.Text = ""
        Else
            TextEdit8.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("IN2")).Date.ToString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern())
            TextEdit6.Text = ds.Tables(0).Rows(0).Item("IN2").ToString.Split(" ")(1).Substring(0, 5)
        End If
        If ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim = "" Then
            TextEdit9.Text = ""
        Else
            TextEdit9.Text = (ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim \ 60).ToString("00") & ":" & (ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim Mod 60).ToString("00")
        End If
        TextEdit10.Text = ds.Tables(0).Rows(0).Item("STATUS").ToString

        CheckEdit4.Checked = True
        CheckEdit5.Visible = False
        CheckEdit6.Visible = False

    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit1.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("SHIFT").ToString & (" - " + Convert.ToDateTime(row("STARTTIME").ToString).ToString("HH:mm")) & (" - " + Convert.ToDateTime(row("ENDTIME").ToString).ToString("HH:mm")))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit2_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit2.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("LeaveCode").ToString & (" - " + row("LeaveDescription").ToString) & (" - " + row("LeaveLeft").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub LoadPunchHistoryGrid()
        Dim DateOFFICE As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
        Dim machineRawPunch As String
        If Common.servername = "Access" Then
            machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & XtraDataMaintenance.cardnoformaintaince & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 00:00:00') >= '" & DateOFFICE.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 23:59:59') <='" & DateOFFICE.ToString("yyyy-MM-dd 23:59:59") & "' " 'AND ISMANUAL = 'Y'"
        Else
            machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & XtraDataMaintenance.cardnoformaintaince & "' and OFFICEPUNCH >= '" & DateOFFICE.ToString("yyyy-MM-dd 00:00:00") & "' and OFFICEPUNCH <='" & DateOFFICE.ToString("yyyy-MM-dd 23:59:59") & "' " 'AND ISMANUAL = 'Y'"
        End If
        Dim WTDataTable1 As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(machineRawPunch, Common.con1)
            WTDataTable1 = New DataTable("MachineRawPunch")
            dataAdapter.Fill(WTDataTable1)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(machineRawPunch, Common.con)
            WTDataTable1 = New DataTable("MachineRawPunch")
            dataAdapter.Fill(WTDataTable1)
        End If
        GridControl2.DataSource = WTDataTable1
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Cursor = Cursors.WaitCursor
        Dim OFFICEPUNCH As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextEdit11.Text.Trim & ":00")
        'MsgBox(OFFICEPUNCH.ToString)
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter("select CARDNO from MachineRawPunch where CARDNO='" & XtraDataMaintenance.cardnoformaintaince & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss')='" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'", Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter("select CARDNO from MachineRawPunch where CARDNO='" & XtraDataMaintenance.cardnoformaintaince & "' and OFFICEPUNCH='" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'", Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Punch already present for date and time.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        Dim sSql As String = "insert INTO MachineRawPunch (CARDNO,OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE) VALUES('" & XtraDataMaintenance.cardnoformaintaince & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & XtraDataMaintenance.paycodeformaintaince & "')"
        Dim sSql1 As String = "insert INTO MachineRawPunchAll (CARDNO,OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE) VALUES('" & XtraDataMaintenance.cardnoformaintaince & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & XtraDataMaintenance.paycodeformaintaince & "')"
        If Common.servername = "Access" Then
            'sSql = "insert INTO MachineRawPunch (CARDNO,FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss'),P_DAY,ISMANUAL,PAYCODE) VALUES('" & TextEdit2.Text.Trim & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & TextEdit1.Text.Trim & "')"
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            Try
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Common.con1.Close()
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            Try
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Common.con.Close()
        End If
        Common.LogPost("Manual Punch Entry; Paycode=" & XtraDataMaintenance.paycodeformaintaince & ", Punch Time=" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss"))

        If Common.IsParallel = "Y" Then
            Dim cardNo As String
            If IsNumeric(TextEdit2.Text.Trim) Then
                cardNo = Convert.ToDouble(TextEdit2.Text.Trim)
            Else
                cardNo = TextEdit2.Text.Trim
            End If
            Common.parallelInsert(cardNo, XtraDataMaintenance.paycodeformaintaince, OFFICEPUNCH, "N", "Y", XtraDataMaintenance.nameformaintaince, "0", "")
        End If

        Dim adap1 As SqlDataAdapter
        Dim adapA1 As OleDbDataAdapter
        Dim ds1 As DataSet = New DataSet
        Dim getRTC As String = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & XtraDataMaintenance.paycodeformaintaince & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
        '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE='" & XtraDataMaintenance.paycodeformaintaince & "'"
        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter(getRTC, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(getRTC, Common.con)
            adap1.Fill(ds1)
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            If ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                comclass.Process_AllRTC(DateEdit1.DateTime.AddDays(-1), Now, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
            Else
                comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
                If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                    comclass.Process_AllnonRTCMulti(DateEdit1.DateTime, DateEdit1.DateTime, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
                End If
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        LoadPunchHistoryGrid()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub CheckEditShift_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditShift.CheckedChanged
        If CheckEditShift.Checked = True Then
            LabelControl1.Visible = True
            GridLookUpEdit1.Visible = True
        Else
            LabelControl1.Visible = False
            GridLookUpEdit1.Visible = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim shift As String
        Dim SHIFTATTENDED As String
        Dim LEAVEVALUE As Integer
        Dim PRESENTVALUE As Integer
        Dim ABSENTVALUE As Integer
        Dim HOLIDAY_VALUE As Integer
        Dim WO_VALUE As Integer
        Dim STATUS As String
        If CheckEditOff.Checked = True Then
            shift = "OFF"
            SHIFTATTENDED = "OFF"
            LEAVEVALUE = 0
            PRESENTVALUE = 0
            ABSENTVALUE = 0
            HOLIDAY_VALUE = 0
            WO_VALUE = 1
            STATUS = "WO"
        Else
            shift = GridLookUpEdit1.EditValue
        End If

        Dim sSql As String
        If shift = "OFF" Then
            sSql = "update tblTimeRegister set SHIFT = '" & shift & "', STATUS='" & STATUS & "', SHIFTATTENDED = '" & SHIFTATTENDED & "', LEAVEVALUE = " & LEAVEVALUE & ", PRESENTVALUE = " & PRESENTVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", HOLIDAY_VALUE = " & HOLIDAY_VALUE & ", WO_VALUE=" & WO_VALUE & " where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and DateOFFICE ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
        Else
            sSql = "update tblTimeRegister set SHIFT = '" & shift & "', SHIFTATTENDED = '" & shift & "' where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and DateOFFICE ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
        End If
        If Common.servername = "Access" Then
            'If shift = "OFF" Then
            '    sSql = "update tblTimeRegister set SHIFT = '" & shift & "', STATUS='WO' where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
            'Else
            '    sSql = "update tblTimeRegister set SHIFT = '" & shift & "' where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
            'End If
            If shift = "OFF" Then
                sSql = "update tblTimeRegister set SHIFT = '" & shift & "', STATUS='" & STATUS & "', SHIFTATTENDED = '" & SHIFTATTENDED & "', LEAVEVALUE = " & LEAVEVALUE & ", PRESENTVALUE = " & PRESENTVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", HOLIDAY_VALUE = " & HOLIDAY_VALUE & ", WO_VALUE=" & WO_VALUE & " where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
            Else
                sSql = "update tblTimeRegister set SHIFT = '" & shift & "', SHIFTATTENDED = '" & shift & "' where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') ='" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.LogPost("Shift Change; Paycode:" & XtraDataMaintenance.paycodeformaintaince & ", Day:" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).ToString("yyyy-MM-dd"))
        Dim adap1 As SqlDataAdapter
        Dim adapA1 As OleDbDataAdapter
        Dim ds1 As DataSet = New DataSet
        Dim getRTC As String = "select  tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
        '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE='" & XtraDataMaintenance.paycodeformaintaince & "'"
        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter(getRTC, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(getRTC, Common.con)
            adap1.Fill(ds1)
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            If ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                comclass.Process_AllRTC(Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).AddDays(-1), Now, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
            Else
                comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
                If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                    comclass.Process_AllnonRTCMulti(DateEdit1.DateTime, DateEdit1.DateTime, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds1.Tables(0).Rows(0).Item("Id"))
                End If
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Me.Close()
    End Sub
    Private Sub CheckEdit2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit2.CheckedChanged, CheckEditShift.CheckedChanged
        If CheckEdit2.Checked = True Then
            CheckEdit5.Visible = True
            CheckEdit6.Visible = True
        Else
            CheckEdit5.Visible = False
            CheckEdit6.Visible = False
        End If
    End Sub
    Private Sub LoadLeaveQuota()
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet
        Dim sSql As String = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION from tblLeaveMaster"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            ds = New DataSet
            adap.Fill(ds)
        End If
        Dim LDate As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
        Dim s As String = ""
        Dim s1 As String = ""
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("LeaveCode")
        dt.Columns.Add("LeaveDescription")
        dt.Columns.Add("LeaveLeft")
        For i As Integer = 1 To ds.Tables(0).Rows.Count
            s = "L" & i.ToString("00")
            s1 = "L" & i.ToString("00") & "_ADD "
            Dim sS As String = "select " & s & " , " & s1 & " from tblLeaveLedger where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and LYEAR = " & LDate.Year & ""
            'MsgBox(sS)
            If Common.servername = "Access" Then
                adapA1 = New OleDbDataAdapter(sS, Common.con1)
                ds1 = New DataSet
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sS, Common.con)
                ds1 = New DataSet
                adap1.Fill(ds1)
            End If
            Dim lftLEave As String
            If ds1.Tables(0).Rows.Count = 0 Then
                'Try
                sSql = "select EmployeeGroupId from TblEmployee where PAYCODE ='" & XtraDataMaintenance.paycodeformaintaince & "'"
                Dim adapX As SqlDataAdapter
                Dim adapAX As OleDbDataAdapter
                Dim dsX As DataSet = New DataSet
                Dim dsX1 As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    adapAX.Fill(dsX1)
                Else
                    adapX = New SqlDataAdapter(sSql, Common.con)
                    adapX.Fill(dsX1)
                End If
                sSql = "SELECT * from EmployeeGroupLeaveLedger where GroupId = '" & dsX1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString.Trim & "' and LYEAR=" & Now.Year & " "
                dsX = New DataSet
                If Common.servername = "Access" Then
                    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    adapAX.Fill(dsX)
                Else
                    adapX = New SqlDataAdapter(sSql, Common.con)
                    adapX.Fill(dsX)
                End If
                If dsX.Tables(0).Rows.Count = 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Leave Accrual Not Prsent. Please Create Leave Accrual.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Close()
                Else
                    Dim sSql3 As String = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES " & _
                                          "('" & XtraDataMaintenance.paycodeformaintaince & "', " & dsX.Tables(0).Rows(0).Item("L01_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L02_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L03_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L04_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L05_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L06_ADD").ToString.Trim & ", " & _
                                         "" & dsX.Tables(0).Rows(0).Item("L07_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L08_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L09_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L10_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L11_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L12_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L13_ADD").ToString.Trim & ", " & _
                                         "" & dsX.Tables(0).Rows(0).Item("L14_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L15_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L16_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L17_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L18_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L19_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L20_ADD").ToString.Trim & ",'N','" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).Year & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql3, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql3, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
                'Dim sSql3 As String = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES ('" & XtraDataMaintenance.paycodeformaintaince & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'N','" & Convert.ToDateTime(XtraDataMaintenance.DateEditAtt).Year & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                'If Common.servername = "Access" Then
                '    If Common.con1.State <> ConnectionState.Open Then
                '        Common.con1.Open()
                '    End If
                '    cmd1 = New OleDbCommand(sSql3, Common.con1)
                '    cmd1.ExecuteNonQuery()
                '    If Common.con1.State <> ConnectionState.Closed Then
                '        Common.con1.Close()
                '    End If
                'Else
                '    If Common.con.State <> ConnectionState.Open Then
                '        Common.con.Open()
                '    End If
                '    cmd = New SqlCommand(sSql3, Common.con)
                '    cmd.ExecuteNonQuery()
                '    If Common.con.State <> ConnectionState.Closed Then
                '        Common.con.Close()
                '    End If
                'End If


                'Catch ex As Exception

                'End Try
                'XtraMessageBox.Show(ulf, "<size=10>No opening balance created. Please create opening balance in Leave Accrual.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'setdefault()
                'Exit Sub
                lftLEave = "0"
            Else
                lftLEave = ds1.Tables(0).Rows(0).Item(1).ToString - ds1.Tables(0).Rows(0).Item(0).ToString
            End If
            dt.Rows.Add(ds.Tables(0).Rows(i - 1).Item("LEAVECODE").ToString.Trim, ds.Tables(0).Rows(i - 1).Item("LEAVEDESCRIPTION").ToString.Trim, lftLEave)
        Next
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridLookUpEdit2.Properties.DataSource = dt
        GridLookUpEdit2.Properties.DisplayMember = "LeaveCode"
        GridLookUpEdit2.Properties.ValueMember = "LeaveCode"
        GridLookUpEdit2.EditValue = GridLookUpEdit2.Properties.GetKeyValue(0)
    End Sub
    'Private Sub btnUpdateLeave_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateLeave.Click
    '    Dim voucherNo As String
    '    Dim adapX As SqlDataAdapter
    '    Dim adapAX As OleDbDataAdapter
    '    Dim dsX As DataSet = New DataSet
    '    Dim sSql As String = "select max(VOUCHER_NO) from tblTimeRegister "
    '    If Common.servername = "Access" Then
    '        adapAX = New OleDbDataAdapter(sSql, Common.con1)
    '        adapAX.Fill(dsX)
    '    Else
    '        adapX = New SqlDataAdapter(sSql, Common.con)
    '        adapX.Fill(dsX)
    '    End If
    '    If dsX.Tables(0).Rows.Count = 0 Or dsX.Tables(0).Rows(0).Item(0).ToString = "" Then
    '        voucherNo = "0000000001"
    '    Else
    '        Dim voucher_num As Integer = Convert.ToInt32(dsX.Tables(0).Rows(0).Item(0).ToString) + 1
    '        voucherNo = (Convert.ToInt32(dsX.Tables(0).Rows(0).Item(0).ToString) + 1).ToString("0000000000")
    '    End If

    '    Dim PAYCODE As String = XtraDataMaintenance.paycodeformaintaince
    '    Dim FromDate As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
    '    Dim ToDate As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
    '    Dim ApproveDate As DateTime = Now.ToString("yyyy-MM-dd 00:00:00")

    '    Dim LeaveCode As String = GridLookUpEdit2.EditValue.ToString.Trim
    '    Dim appliedfor As String
    '    Dim halfttpe As String = ""
    '    Dim days As Double
    '    Dim reason As String = TextEditReasonLeave.Text.Trim

    '    Dim adap, adap1, adap2, adap3, adap4 As SqlDataAdapter
    '    Dim adapA, adapA1, adapA2, adapA3, adapA4 As OleDbDataAdapter
    '    Dim ds, ds1, WOHLD, SelSts, Quota As DataSet
    '    ds = New DataSet
    '    Dim leaveFieldSql As String = "select LEAVEFIELD, LEAVETYPE, ISOFFINCLUDE, ISHOLIDAYINCLUDE, ISLEAVEACCRUAL, FIXED, SMIN, SMAX from tblLeaveMaster where LEAVECODE='" & GridLookUpEdit2.EditValue & "'"
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(leaveFieldSql, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        adap = New SqlDataAdapter(leaveFieldSql, Common.con)
    '        adap.Fill(ds)
    '    End If

    '    Dim status As String
    '    Dim LEAVEVALUE As Double
    '    Dim ABSENTVALUE As Double
    '    Dim LEAVEFIELD As String = ds.Tables(0).Rows(0).Item("LEAVEFIELD").ToString.Trim
    '    Dim LEAVETYPE As String = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
    '    Dim LEAVETYPE1 As String = Nothing
    '    Dim LEAVETYPE2 As String = Nothing
    '    Dim LEAVEAMOUNT As Double = days
    '    Dim LEAVEAMOUNT1 As Double = 0
    '    Dim LEAVEAMOUNT2 As Double = 0
    '    Dim LEAVEAPRDate As DateTime = Now.ToString("yyyy-MM-dd 00:00:00")
    '    Dim LUNCHSTARTTIME As DateTime
    '    Dim LUNCHENDTIME As DateTime
    '    Dim SHIFTSTARTTIME As DateTime
    '    Dim SHIFTENDTIME As DateTime
    '    Dim FIRSTHALFLEAVECODE As String = Nothing
    '    Dim SECONDHALFLEAVECODE As String = Nothing
    '    Dim VOUCHER_NO As String = voucherNo

    '    If CheckEdit1.Checked = True Then
    '        appliedfor = "Quater"
    '        days = 0.25
    '        status = "Q_" & GridLookUpEdit2.EditValue.ToString.Trim
    '        LEAVEVALUE = 0.25
    '        ABSENTVALUE = 0.75
    '    ElseIf CheckEdit2.Checked = True Then
    '        appliedfor = "Half"
    '        If CheckEdit5.Checked Then
    '            halfttpe = "First Half"
    '            LEAVETYPE1 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
    '            LEAVEAMOUNT1 = 0.5
    '            FIRSTHALFLEAVECODE = GridLookUpEdit2.EditValue.ToString.Trim
    '        Else
    '            halfttpe = "Second Half"
    '            LEAVETYPE2 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
    '            LEAVEAMOUNT2 = 0.5
    '            SECONDHALFLEAVECODE = GridLookUpEdit2.EditValue.ToString.Trim
    '        End If
    '        days = 0.5
    '        status = "H_" & GridLookUpEdit2.EditValue.ToString.Trim
    '        LEAVEVALUE = 0.5
    '        ABSENTVALUE = 0.5
    '    ElseIf CheckEdit3.Checked = True Then
    '        appliedfor = "Three Forth"
    '        days = 0.75
    '        status = "T_" & GridLookUpEdit2.EditValue.ToString.Trim
    '        LEAVEVALUE = 0.75
    '        ABSENTVALUE = 0.25
    '    ElseIf CheckEdit4.Checked = True Then
    '        appliedfor = "Full Day"
    '        days = 1
    '        status = GridLookUpEdit2.EditValue.ToString.Trim
    '        LEAVEVALUE = 1
    '        ABSENTVALUE = 0
    '    End If
    '    'Dim datediff As TimeSpan = DateEdit2.DateTime.Subtract(DateEdit1.DateTime)

    '    Dim WOHLDAlowdSql As String = "SELECT ISOFFINCLUDE, ISHOLIDAYINCLUDE from tblLeaveMaster where LEAVECODE = '" & GridLookUpEdit2.EditValue.ToString.Trim & "'"
    '    WOHLD = New DataSet
    '    If Common.servername = "Access" Then
    '        adapA2 = New OleDbDataAdapter(WOHLDAlowdSql, Common.con1)
    '        adapA2.Fill(WOHLD)
    '    Else
    '        adap2 = New SqlDataAdapter(WOHLDAlowdSql, Common.con)
    '        adap2.Fill(WOHLD)
    '    End If
    '    'Dim WO As Integer = 0
    '    'Dim HLD As Integer = 0
    '    Dim SelectStatus As String
    '    'Dim tmpDate As DateTime
    '    'For x As Integer = 0 To datediff.Days
    '    'tmpDate = FromDate.AddDays(x).ToString("yyyy-MM-dd 00:00:00")
    '    SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
    '    SelSts = New DataSet
    '    If Common.servername = "Access" Then
    '        SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') ='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
    '        adapA3 = New OleDbDataAdapter(SelectStatus, Common.con1)
    '        adapA3.Fill(SelSts)
    '    Else
    '        adap3 = New SqlDataAdapter(SelectStatus, Common.con)
    '        adap3.Fill(SelSts)
    '    End If
    '    If SelSts.Tables(0).Rows.Count = 0 Then
    '        XtraMessageBox.Show(ulf, "<size=10>No Roster present for employee." & vbCrLf & "Create Roster or leave apply date is less than date of joining of employee.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        setdefault()
    '        Exit Sub
    '    End If
    '    'If SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Then
    '    '    If WOHLD.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString = "N" Then
    '    '        WO = WO + 1
    '    '    End If
    '    'ElseIf SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
    '    '    If WOHLD.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString = "N" Then
    '    '        HLD = HLD + 1
    '    '    End If
    '    'End If
    '    'Next

    '    Dim totaldays As Double = days
    '    Dim findQuotaSql As String = "SELECT " & LEAVEFIELD & "_ADD - " & LEAVEFIELD & " from tblLeaveLedger where PAYCODE='" & PAYCODE & "' and LYEAR = " & FromDate.Year & ""
    '    Quota = New DataSet
    '    If Common.servername = "Access" Then
    '        adapA4 = New OleDbDataAdapter(findQuotaSql, Common.con1)
    '        adapA4.Fill(Quota)
    '    Else
    '        adap4 = New SqlDataAdapter(findQuotaSql, Common.con)
    '        adap4.Fill(Quota)
    '    End If
    '    'MsgBox(findQuotaSql)
    '    'MsgBox(Quota.Tables(0).Rows(0).Item(0))
    '    If totaldays > Quota.Tables(0).Rows(0).Item(0) Then
    '        If XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance " & vbCrLf & " Still want to continue</size>", "<size=9>Confirm</size>", _
    '                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
    '            'setdefault()
    '            Exit Sub
    '        End If
    '        'XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance</size>", "<size=9>Error</size>")
    '    End If
    '    'MsgBox(datediff.Days)
    '    Dim InsertinleaveApp As String
    '    Dim updatetblregiter As String
    '    If Common.servername = "Access" Then
    '        If Common.con1.State <> ConnectionState.Open Then
    '            Common.con1.Open()
    '        End If
    '    Else
    '        If Common.con.State <> ConnectionState.Open Then
    '            Common.con.Open()
    '        End If
    '    End If
    '    'Dim DateOFFICE As DateTime
    '    Dim FindShiftSql As String
    '    'Dim i As Integer = 0
    '    'For i = 0 To DateDiff.Days
    '    'DateOFFICE = FromDate.AddDays(i).ToString("yyyy-MM-dd 00:00:00")
    '    ds1 = New DataSet
    '    FindShiftSql = "select (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
    '                "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
    '                "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
    '                "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
    '                "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
    '    If Common.servername = "Access" Then
    '        FindShiftSql = "select (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
    '               "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
    '               "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
    '               "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
    '               "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00')='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
    '        adapA1 = New OleDbDataAdapter(FindShiftSql, Common.con1)
    '        adapA1.Fill(ds1)
    '    Else
    '        adap1 = New SqlDataAdapter(FindShiftSql, Common.con)
    '        adap1.Fill(ds1)
    '    End If
    '    'MsgBox(ds1.Tables(0).Rows(0).Item("STATUS").ToString & " , " & ds1.Tables(0).Rows(0).Item("STATUS").ToString.Length)
    '    If ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Or ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
    '        XtraMessageBox.Show(ulf, "<size=10>WO or Holiday assigned on date " & FromDate.ToString("dd MMM yyyy") & "</size>")
    '        Exit Sub
    '        'Continue For
    '    End If
    '    'MsgBox(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString)
    '    'MsgBox(Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
    '    SHIFTSTARTTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
    '    SHIFTENDTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftendtime").ToString).ToString("HH:mm:ss"))

    '    LUNCHSTARTTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchstarttime").ToString).ToString("HH:mm:ss"))
    '    LUNCHENDTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchendtime").ToString).ToString("HH:mm:ss"))

    '    'updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
    '    '        "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
    '    '        "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
    '    '        "where PAYCODE='" & PAYCODE & "' and DateOFFICE ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"

    '    updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
    '           "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
    '           "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
    '           "where PAYCODE='" & PAYCODE & "' and "

    '    InsertinleaveApp = "insert INTO LeaveApplication (VOUCHER_NO, PayCode, LeaveCode, ForDate, Days, AppliedFor, HalfType, Reason, ApprovedDate, LastModifiedBy, LastModifiedDate) VALUES " & _
    '           "('" & VOUCHER_NO & "','" & PAYCODE & "','" & LeaveCode & "','" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'," & days & ",'" & appliedfor & "','" & halfttpe & "','" & reason & "','" & ApproveDate.ToString("yyyy-MM-dd HH:mm:ss") & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
    '    If Common.servername = "Access" Then
    '        updatetblregiter = updatetblregiter & "FORMAT(DateOFFICE, 'yyyy-MM-dd HH:mm:ss') ='" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"

    '        cmd1 = New OleDbCommand(updatetblregiter, Common.con1)
    '        cmd1.ExecuteNonQuery()

    '        cmd1 = New OleDbCommand(InsertinleaveApp, Common.con1)
    '        cmd1.ExecuteNonQuery()
    '    Else
    '        updatetblregiter = updatetblregiter & "DateOFFICE ='" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
    '        cmd = New SqlCommand(updatetblregiter, Common.con)
    '        cmd.ExecuteNonQuery()

    '        cmd = New SqlCommand(InsertinleaveApp, Common.con)
    '        cmd.ExecuteNonQuery()
    '    End If
    '    'Next
    '    'Dim x As Double = i * days
    '    Dim updateLeaveLedger As String = "update tblLeaveLedger set " & LEAVEFIELD & " = " & LEAVEFIELD & " + " & totaldays & "  where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and LYEAR = " & FromDate.Year & ""
    '    'MsgBox("i = " & i & vbCrLf & "days = " & days & vbCrLf & "x = " & x & vbCrLf & updateLeaveLedger)
    '    If Common.servername = "Access" Then
    '        cmd1 = New OleDbCommand(updateLeaveLedger, Common.con1)
    '        cmd1.ExecuteNonQuery()
    '    Else
    '        cmd = New SqlCommand(updateLeaveLedger, Common.con)
    '        cmd.ExecuteNonQuery()
    '    End If

    '    If Common.servername = "Access" Then
    '        Common.con1.Close()
    '    Else
    '        Common.con.Close()
    '    End If

    '    XtraMessageBox.Show(ulf, "<size = 10> Saved Successfully </size> ", "<size = 9> Success </size>")
    '    setdefault()
    '    Me.Close()
    '    'MsgBox(GridLookUpEdit1.EditValue)
    'End Sub
    Private Sub btnUpdateLeave_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateLeave.Click
        Dim voucherNo As String
        Dim adapX As SqlDataAdapter
        Dim adapAX As OleDbDataAdapter
        Dim dsX As DataSet = New DataSet
        Dim sSql As String = "select max(VOUCHER_NO) from tblTimeRegister "
        If Common.servername = "Access" Then
            adapAX = New OleDbDataAdapter(sSql, Common.con1)
            adapAX.Fill(dsX)
        Else
            adapX = New SqlDataAdapter(sSql, Common.con)
            adapX.Fill(dsX)
        End If
        If dsX.Tables(0).Rows.Count = 0 Or dsX.Tables(0).Rows(0).Item(0).ToString = "" Then
            voucherNo = "0000000001"
        Else
            Dim voucher_num As Integer = Convert.ToInt32(dsX.Tables(0).Rows(0).Item(0).ToString) + 1
            voucherNo = (Convert.ToInt32(dsX.Tables(0).Rows(0).Item(0).ToString) + 1).ToString("0000000000")
        End If

        Dim PAYCODE As String = XtraDataMaintenance.paycodeformaintaince
        Dim FromDate As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
        Dim ToDate As DateTime = Convert.ToDateTime(XtraDataMaintenance.DateEditAtt)
        Dim ApproveDate As DateTime = Now.ToString("yyyy-MM-dd 00:00:00")

        Dim LeaveCode As String = GridLookUpEdit2.EditValue.ToString.Trim
        Dim appliedfor As String
        Dim halfttpe As String = ""
        Dim days As Double
        Dim reason As String = TextEditReasonLeave.Text.Trim

        Dim adap, adap1, adap2, adap3, adap4 As SqlDataAdapter
        Dim adapA, adapA1, adapA2, adapA3, adapA4 As OleDbDataAdapter
        Dim ds, ds1, WOHLD, SelSts, Quota As DataSet
        ds = New DataSet
        Dim leaveFieldSql As String = "select LEAVEFIELD, LEAVETYPE, ISOFFINCLUDE, ISHOLIDAYINCLUDE, ISLEAVEACCRUAL, FIXED, SMIN, SMAX from tblLeaveMaster where LEAVECODE='" & GridLookUpEdit2.EditValue & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(leaveFieldSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(leaveFieldSql, Common.con)
            adap.Fill(ds)
        End If

        Dim status As String
        Dim LEAVEVALUE As Double
        Dim ABSENTVALUE As Double
        Dim LEAVEFIELD As String = ds.Tables(0).Rows(0).Item("LEAVEFIELD").ToString.Trim
        Dim LEAVETYPE As String = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
        Dim LEAVETYPE1 As String = Nothing
        Dim LEAVETYPE2 As String = Nothing
        Dim LEAVEAMOUNT As Double = days
        Dim LEAVEAMOUNT1 As Double = 0
        Dim LEAVEAMOUNT2 As Double = 0
        Dim LEAVEAPRDate As DateTime = Now.ToString("yyyy-MM-dd 00:00:00")
        Dim LUNCHSTARTTIME As DateTime
        Dim LUNCHENDTIME As DateTime
        Dim SHIFTSTARTTIME As DateTime
        Dim SHIFTENDTIME As DateTime
        Dim FIRSTHALFLEAVECODE As String = Nothing
        Dim SECONDHALFLEAVECODE As String = Nothing
        Dim VOUCHER_NO As String = voucherNo

        If CheckEdit1.Checked = True Then
            appliedfor = "Quater"
            days = 0.25
            status = "Q_" & GridLookUpEdit2.EditValue.ToString.Trim
            LEAVEVALUE = 0.25
            ABSENTVALUE = 0.75
        ElseIf CheckEdit2.Checked = True Then
            appliedfor = "Half"
            If CheckEdit5.Checked Then
                halfttpe = "First Half"
                LEAVETYPE1 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
                LEAVEAMOUNT1 = 0.5
                FIRSTHALFLEAVECODE = GridLookUpEdit2.EditValue.ToString.Trim
            Else
                halfttpe = "Second Half"
                LEAVETYPE2 = ds.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim
                LEAVEAMOUNT2 = 0.5
                SECONDHALFLEAVECODE = GridLookUpEdit2.EditValue.ToString.Trim
            End If
            days = 0.5
            status = "H_" & GridLookUpEdit2.EditValue.ToString.Trim
            LEAVEVALUE = 0.5
            ABSENTVALUE = 0.5
        ElseIf CheckEdit3.Checked = True Then
            appliedfor = "Three Forth"
            days = 0.75
            status = "T_" & GridLookUpEdit2.EditValue.ToString.Trim
            LEAVEVALUE = 0.75
            ABSENTVALUE = 0.25
        ElseIf CheckEdit4.Checked = True Then
            appliedfor = "Full Day"
            days = 1
            status = GridLookUpEdit2.EditValue.ToString.Trim
            LEAVEVALUE = 1
            ABSENTVALUE = 0
        End If
        'Dim datediff As TimeSpan = DateEdit2.DateTime.Subtract(DateEdit1.DateTime)

        Dim WOHLDAlowdSql As String = "SELECT ISOFFINCLUDE, ISHOLIDAYINCLUDE from tblLeaveMaster where LEAVECODE = '" & GridLookUpEdit2.EditValue.ToString.Trim & "'"
        WOHLD = New DataSet
        If Common.servername = "Access" Then
            adapA2 = New OleDbDataAdapter(WOHLDAlowdSql, Common.con1)
            adapA2.Fill(WOHLD)
        Else
            adap2 = New SqlDataAdapter(WOHLDAlowdSql, Common.con)
            adap2.Fill(WOHLD)
        End If
        'Dim WO As Integer = 0
        'Dim HLD As Integer = 0
        Dim SelectStatus As String
        'Dim tmpDate As DateTime
        'For x As Integer = 0 To datediff.Days
        'tmpDate = FromDate.AddDays(x).ToString("yyyy-MM-dd 00:00:00")
        SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
        SelSts = New DataSet
        If Common.servername = "Access" Then
            SelectStatus = "select STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') ='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
            adapA3 = New OleDbDataAdapter(SelectStatus, Common.con1)
            adapA3.Fill(SelSts)
        Else
            adap3 = New SqlDataAdapter(SelectStatus, Common.con)
            adap3.Fill(SelSts)
        End If
        If SelSts.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Roster present for employee." & vbCrLf & "Create Roster or leave apply date is less than date of joining of employee.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            setdefault()
            Exit Sub
        End If
        'If SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Then
        '    If WOHLD.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString = "N" Then
        '        WO = WO + 1
        '    End If
        'ElseIf SelSts.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
        '    If WOHLD.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString = "N" Then
        '        HLD = HLD + 1
        '    End If
        'End If
        'Next

        Dim totaldays As Double = days '(datediff.Days - WO - HLD + 1) * days

        If ds.Tables(0).Rows(0).Item("ISLEAVEACCRUAL").ToString.Trim = "Y" And ds.Tables(0).Rows(0).Item("FIXED").ToString.Trim = "Y" Then
            If totaldays < ds.Tables(0).Rows(0).Item("SMIN").ToString.Trim Or totaldays > ds.Tables(0).Rows(0).Item("SMAX").ToString.Trim Then
                XtraMessageBox.Show(ulf, "<size=10>Total days is not in range of Leave accrual limites</size>")
                Exit Sub
            End If
        End If
        Dim findQuotaSql As String = "SELECT " & LEAVEFIELD & "_ADD - " & LEAVEFIELD & " from tblLeaveLedger where PAYCODE='" & PAYCODE & "' and LYEAR = " & FromDate.Year & ""
        Quota = New DataSet
        If Common.servername = "Access" Then
            adapA4 = New OleDbDataAdapter(findQuotaSql, Common.con1)
            adapA4.Fill(Quota)
        Else
            adap4 = New SqlDataAdapter(findQuotaSql, Common.con)
            adap4.Fill(Quota)
        End If
        'MsgBox(findQuotaSql)
        'MsgBox(Quota.Tables(0).Rows(0).Item(0))
        If totaldays > Quota.Tables(0).Rows(0).Item(0) Then
            If XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance " & vbCrLf & " Still want to continue</size>", "<size=9>Confirm</size>", _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                'setdefault()
                Exit Sub
            End If
            'XtraMessageBox.Show(ulf, "<size=10>Insufficient leave balance</size>", "<size=9>Error</size>")
        End If
        'MsgBox(datediff.Days)
        Dim InsertinleaveApp As String
        Dim updatetblregiter As String
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
        End If
        'Dim DateOFFICE As DateTime
        Dim FindShiftSql As String
        'Dim i As Integer = 0
        'For i = 0 To DateDiff.Days
        'DateOFFICE = FromDate.AddDays(i).ToString("yyyy-MM-dd 00:00:00")
        ds1 = New DataSet
        FindShiftSql = "select tblTimeRegister.SHIFT, (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
                "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
                "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
                "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
                "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
        If Common.servername = "Access" Then
            FindShiftSql = "select tblTimeRegister.SHIFT, (select STARTTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftstarttime, " & _
               "(select ENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as shiftendtime, " & _
               "(select LUNCHTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchstarttime, " & _
               "(select LUNCHENDTIME from tblShiftMaster where tblShiftMaster.SHIFT = tblTimeRegister.SHIFT) as lunchendtime, " & _
               "STATUS from tblTimeRegister where PAYCODE='" & PAYCODE & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00')='" & FromDate.ToString("yyyy-MM-dd 00:00:00") & "' "
            adapA1 = New OleDbDataAdapter(FindShiftSql, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(FindShiftSql, Common.con)
            adap1.Fill(ds1)
        End If
        'MsgBox(ds1.Tables(0).Rows(0).Item("STATUS").ToString & " , " & ds1.Tables(0).Rows(0).Item("STATUS").ToString.Length)
        If ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "WO" Or ds1.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "HLD" Then
            If ds.Tables(0).Rows(0).Item("ISOFFINCLUDE").ToString.Trim = "Y" Then
                updatetblregiter = "update tblTimeRegister set  STATUS='" & status & "', " & _
              "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
              "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
              "where PAYCODE='" & PAYCODE & "' and "
                GoTo tmp
            End If
            If ds.Tables(0).Rows(0).Item("ISHOLIDAYINCLUDE").ToString.Trim = "Y" Then
                updatetblregiter = "update tblTimeRegister set  STATUS='" & status & "', " & _
           "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
           "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
           "where PAYCODE='" & PAYCODE & "' and "
                GoTo tmp
            End If
            XtraMessageBox.Show(ulf, "<size=10>WO or Holiday assigned on date " & FromDate.ToString("dd MMM yyyy") & "</size>")
            Exit Sub
            'Continue For
        End If
        'MsgBox(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString)
        'MsgBox(Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
        SHIFTSTARTTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftstarttime").ToString).ToString("HH:mm:ss"))
        SHIFTENDTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("shiftendtime").ToString).ToString("HH:mm:ss"))

        LUNCHSTARTTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchstarttime").ToString).ToString("HH:mm:ss"))
        LUNCHENDTIME = Convert.ToDateTime(FromDate.ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("lunchendtime").ToString).ToString("HH:mm:ss"))

        'updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
        '        "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
        '        "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
        '        "where PAYCODE='" & PAYCODE & "' and DateOFFICE ='" & DateOFFICE.ToString("yyyy-MM-dd HH:mm:ss") & "'"

        updatetblregiter = "update tblTimeRegister set SHIFTSTARTTIME = '" & SHIFTSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', SHIFTENDTIME ='" & SHIFTENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHSTARTTIME='" & LUNCHSTARTTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', LUNCHENDTIME='" & LUNCHENDTIME.ToString("yyyy-MM-dd HH:mm:ss") & "', STATUS='" & status & "', " & _
               "LEAVETYPE='" & LEAVETYPE & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', REASON='" & reason & "', LEAVEVALUE=" & LEAVEVALUE & ", ABSENTVALUE=" & ABSENTVALUE & ", LEAVEAMOUNT=" & LEAVEAMOUNT & ", LEAVEAMOUNT1=" & LEAVEAMOUNT1 & ", " & _
               "LEAVEAMOUNT2=" & LEAVEAMOUNT2 & ", LEAVECODE='" & LeaveCode & "', LEAVEAPRDate='" & LEAVEAPRDate.ToString("yyyy-MM-dd HH:mm:ss") & "', VOUCHER_NO = '" & VOUCHER_NO & "', FIRSTHALFLEAVECODE = '" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "' " & _
               "where PAYCODE='" & PAYCODE & "' and "

tmp:    InsertinleaveApp = "insert INTO LeaveApplication (VOUCHER_NO, PayCode, LeaveCode, ForDate, Days, AppliedFor, HalfType, Reason, ApprovedDate, LastModifiedBy, LastModifiedDate) VALUES " & _
           "('" & VOUCHER_NO & "','" & PAYCODE & "','" & LeaveCode & "','" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'," & days & ",'" & appliedfor & "','" & halfttpe & "','" & reason & "','" & ApproveDate.ToString("yyyy-MM-dd HH:mm:ss") & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        If Common.servername = "Access" Then
            updatetblregiter = updatetblregiter & "FORMAT(DateOFFICE, 'yyyy-MM-dd HH:mm:ss') ='" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"

            cmd1 = New OleDbCommand(updatetblregiter, Common.con1)
            cmd1.ExecuteNonQuery()

            cmd1 = New OleDbCommand(InsertinleaveApp, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            updatetblregiter = updatetblregiter & "DateOFFICE ='" & FromDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            cmd = New SqlCommand(updatetblregiter, Common.con)
            cmd.ExecuteNonQuery()

            cmd = New SqlCommand(InsertinleaveApp, Common.con)
            cmd.ExecuteNonQuery()
        End If
        'Next
        'Dim x As Double = i * days
        Dim updateLeaveLedger As String = "update tblLeaveLedger set " & LEAVEFIELD & " = " & LEAVEFIELD & " + " & totaldays & "  where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "' and LYEAR = " & FromDate.Year & ""
        'MsgBox("i = " & i & vbCrLf & "days = " & days & vbCrLf & "x = " & x & vbCrLf & updateLeaveLedger)
        If Common.servername = "Access" Then
            cmd1 = New OleDbCommand(updateLeaveLedger, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            cmd = New SqlCommand(updateLeaveLedger, Common.con)
            cmd.ExecuteNonQuery()
        End If

        If Common.servername = "Access" Then
            Common.con1.Close()
        Else
            Common.con.Close()
        End If

        Dim comclass As Common = New Common
        sSql = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & XtraDataMaintenance.paycodeformaintaince & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
        '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & XtraDataMaintenance.paycodeformaintaince & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                comclass.Process_AllRTC(FromDate.AddDays(-1), Now.Date, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds.Tables(0).Rows(0).Item("Id"))
            Else
                comclass.Process_AllnonRTC(FromDate.AddDays(-1), FromDate, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds.Tables(0).Rows(0).Item("Id"))
                If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                    comclass.Process_AllnonRTCMulti(FromDate.AddDays(-1), FromDate, XtraDataMaintenance.paycodeformaintaince, XtraDataMaintenance.paycodeformaintaince, ds.Tables(0).Rows(0).Item("Id"))
                End If
            End If
        End If
        Common.LogPost("Leave Apply; Paycode:'" & XtraDataMaintenance.paycodeformaintaince)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size = 10> Saved Successfully </size> ", "<size = 9> Success </size>")
        setdefault()
        Me.Close()
        'MsgBox(GridLookUpEdit1.EditValue)
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If Common.IsNepali = "Y" Then
            Me.Cursor = Cursors.WaitCursor
            If e.Column.Caption = "Date" Then
                If row("OFFICEPUNCH").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OFFICEPUNCH").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & ComboBoxEditNEpaliMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                    Catch ex As Exception
                    End Try
                End If
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub
End Class