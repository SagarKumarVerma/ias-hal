﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Public Class XtraTimeZoneBio
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared TZID As String
    Public Shared TZIP As String
    Public Shared TZTime As String
    Public Shared TZDeviceType As String
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView3, New Font("Tahoma", 10))
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraTimeZone_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    GridControlEmp.DataSource = SSSDBDataSet.TblEmployee
        'End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControl1.DataSource = Common.MachineNonAdmin

        GridControl3.DataSource = Nothing
        TextSun.Text = ""
        TextMon.Text = ""
        TextTue.Text = ""
        PopupContainerEditEmp.EditValue = ""
        setDeviceGrid()
    End Sub
    Private Sub setDeviceGrid()
        Dim gridtblregisterselet As String
        If Common.USERTYPE = "A" Then
            gridtblregisterselet = "select * from tblMachine where DeviceType = 'Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872'"
        Else
            'gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            gridtblregisterselet = "select tblMachine.ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "') and DeviceType = 'Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872'"
        End If

        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditEmp_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditEmp.QueryPopUp
        Dim val As Object = PopupContainerEditEmp.EditValue
        If (val Is Nothing) Then
            GridViewEmp.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewEmp.LocateByValue("PAYCODE", text.Trim)
                GridViewEmp.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnGetTZ_Click(sender As System.Object, e As System.EventArgs) Handles btnGetTZ.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        ElseIf GridView1.GetSelectedRows.Count > 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select One Machine at a Time</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        GetTZ()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GetTZ()
        GridControl3.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Device IP")
        dt.Columns.Add("TimeZone ID")

        dt.Columns.Add("TZ1 Start")
        dt.Columns.Add("TZ1 End")

        dt.Columns.Add("TZ2 Start")
        dt.Columns.Add("TZ2 End")

        dt.Columns.Add("TZ3 Start")
        dt.Columns.Add("TZ3 End")

        dt.Columns.Add("TZ4 Start")
        dt.Columns.Add("TZ4 End")

        dt.Columns.Add("TZ5 Start")
        dt.Columns.Add("TZ5 End")

        dt.Columns.Add("TZ6 Start")
        dt.Columns.Add("TZ6 End")

        'dt.Columns.Add("SAT Start")
        'dt.Columns.Add("SAT End")

        'dt.Rows.Add(ds.Tables(0).Rows(i).Item("HDate").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("HOLIDAY").ToString, ds.Tables(0).Rows(i).Item("companycode").ToString)

        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim nCommHandleIndex As Long
        Dim vPrivilege As Long


        Dim sSql As String = ""
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim

                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        nCommHandleIndex = vRet
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then

                        Dim bytTimeZone(SIZE_TIME_ZONE_STRUCT - 1) As Byte
                        Dim vTimeZone As TIME_ZONE
                        Dim TimeZoneId As Integer
                        Dim nFKRetCode As Integer

                        nFKRetCode = FK_EnableDevice(vRet, 0)
                        If nFKRetCode <> RUN_SUCCESS Then
                            Continue For
                        End If
                        For x As Integer = 0 To 249
                            TimeZoneId = x + 1 '1 'cmbTZN.SelectedIndex + 1                       
                            vTimeZone.Init()
                            vTimeZone.TimeZoneId = TimeZoneId
                            ConvertStructureToByteArray(vTimeZone, bytTimeZone)
                            nFKRetCode = FK_HS_GetTimeZone(vRet, bytTimeZone)
                            If nFKRetCode = RUN_SUCCESS Then
                                vTimeZone = ConvertByteArrayToStructure(bytTimeZone, GetType(TIME_ZONE))
                                'ShowTimeZoneValue(vTimeZone)

                                'mtxtStartHour(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).StartHour)
                                'mtxtStartMinute(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).StartMinute)
                                'mtxtEndHour(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).EndHour)
                                'mtxtEndMinute(k).Text = Convert.ToString(vTimeZone.TimeSlots(k).EndMinute)

                                dt.Rows.Add(vpszIPAddress, x + 1, Convert.ToString(vTimeZone.TimeSlots(0).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(0).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(0).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(0).EndMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(1).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(1).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(1).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(1).EndMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(2).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(2).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(2).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(2).EndMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(3).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(3).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(3).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(3).EndMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(4).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(4).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(4).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(4).EndMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(5).StartHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(5).StartMinute.ToString("00")), _
                                            Convert.ToString(vTimeZone.TimeSlots(5).EndHour.ToString("00")) & ":" & Convert.ToString(vTimeZone.TimeSlots(5).EndMinute.ToString("00")))
                                ' Convert.ToString(vTimeZone.TimeSlots(6).StartHour), Convert.ToString(vTimeZone.TimeSlots(6).StartMinute))
                            Else
                                ' lblMessage.Text = ReturnResultPrint(nFKRetCode)
                            End If
                        Next

                        FK_EnableDevice(vRet, 1)
                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    ''FK623Attend.Disconnect
                    FK_DisConnect(nCommHandleIndex)
                End If
            End If
        Next i
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl3.DataSource = dt
        GridView3.Columns(0).Width = 120
    End Sub
    Private Sub btnSaveTZ_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveTZ.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridViewEmp.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim iTZ1 As Integer '= Convert.ToInt32(TextEditTZ1.Text.Trim)
        Dim iTZ2 As Integer '= Convert.ToInt32(TextEditTZ2.Text.Trim)
        Dim iTZ3 As Integer '= Convert.ToInt32(TextEditTZ3.Text.Trim)
        If TextSun.Text.Trim = "" Then
            iTZ1 = 0
        Else
            iTZ1 = Convert.ToInt32(TextSun.Text.Trim)
        End If
        If TextMon.Text.Trim = "" Then
            iTZ2 = 0
        Else
            iTZ2 = Convert.ToInt32(TextMon.Text.Trim)
        End If
        If TextTue.Text.Trim = "" Then
            iTZ3 = 0
        Else
            iTZ3 = Convert.ToInt32(TextTue.Text.Trim)
        End If

        If iTZ1 < 0 OrElse iTZ1 > 50 OrElse iTZ2 < 0 OrElse iTZ2 > 50 OrElse iTZ3 < 0 OrElse iTZ3 > 50 Then
            XtraMessageBox.Show(ulf, "<size=10>Timezone index error!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        'Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                Common.LogPost("Employee TimeZone Set in Device; Device Id='" & LstMachineId)
                'commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                'If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                Dim bConn As Boolean = False
                vnMachineNumber = LstMachineId
                vnLicense = 1261 '1789 '
                lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                vpszIPAddress = Trim(lpszIPAddress)
                vpszNetPort = CLng("5005")
                vpszNetPassword = CLng("0")
                vnTimeOut = CLng("5000")
                vnProtocolType = 0 'PROTOCOL_TCPIP
                'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                    vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)                  
                Else
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0
                    vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If vRet > 0 Then
                    bConn = True
                Else
                    bConn = False
                End If
                If bConn Then
                    Dim nCommHandleIndex As Long = vRet
                    FK_EnableDevice(nCommHandleIndex, 0)
                    Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
                    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    For x As Integer = 0 To selectedRowsEmp.Length - 1
                        'LstEmployeesTarget.ListIndex = e
                        Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                        If Not GridViewEmp.IsGroupRow(rowHandleEmp) Then
                            Dim EnrollNumber As String = GridViewEmp.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim
                            XtraMasterTest.LabelControlStatus.Text = "Setting Time Zone to " & EnrollNumber
                            Application.DoEvents()
                            Dim bytUserPassTime(SIZE_USER_WEEK_PASS_TIME_STRUCT - 1) As Byte
                            Dim vUserPassTime As USER_WEEK_PASS_TIME
                            Dim nFKRetCode As Integer
                            vUserPassTime.Init()
                            GetUserWeekPassTimeValue(EnrollNumber, vUserPassTime)
                            nFKRetCode = FK_EnableDevice(nCommHandleIndex, 0)
                            If nFKRetCode <> RUN_SUCCESS Then
                                Return
                            End If
                            ConvertStructureToByteArray(vUserPassTime, bytUserPassTime)
                            nFKRetCode = FK_HS_SetUserWeekPassTime(nCommHandleIndex, bytUserPassTime)
                            If nFKRetCode = RUN_SUCCESS Then
                                'lblMessage.Text = "Success !"
                            Else
                                'lblMessage.Text = ReturnResultPrint(nFKRetCode)
                                XtraMessageBox.Show(ulf, "<size=10>" & ReturnResultPrint(nFKRetCode) & "</size>", "Error")
                                Exit Sub
                            End If
                            FK_EnableDevice(nCommHandleIndex, 1)
                        End If
                    Next x
                    FK_EnableDevice(nCommHandleIndex, 1)
                    FK_DisConnect(nCommHandleIndex)
                Else
                    'MsgBox("Device No: " & LstMachineId & " Not connected..")
                    XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")

                End If
                ''FK623Attend.Disconnect
                'FK_DisConnect(vRet)
                'End If
            End If
        Next i
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub GridView3_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView3.EditFormShowing
        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)
        TZID = row("TimeZone ID").ToString.Trim
        TZIP = row("Device IP").ToString.Trim
        TZTime = row("TZ1 Start").ToString.Trim & " " & row("TZ1 End").ToString.Trim & " " & _
            row("TZ2 Start").ToString.Trim & " " & row("TZ2 End").ToString.Trim & " " & _
            row("TZ3 Start").ToString.Trim & " " & row("TZ3 End").ToString.Trim & " " & _
            row("TZ4 Start").ToString.Trim & " " & row("TZ4 End").ToString.Trim & " " & _
            row("TZ5 Start").ToString.Trim & " " & row("TZ5 End").ToString.Trim & " " & _
            row("TZ6 Start").ToString.Trim & " " & row("TZ6 End").ToString.Trim
        'row("SAT Start").ToString.Trim & " " & row("SAT End").ToString.Trim

        TZDeviceType = ""
        e.Allow = False
        XtraTimeZoneEditBio.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        GetTZ()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GetUserWeekPassTimeValue(ByVal EnrollNumber As String, ByRef aUserWeekPassTime As USER_WEEK_PASS_TIME)
        aUserWeekPassTime.UserId = EnrollNumber 'GetInt(txtEnrollNumber.Text)
        aUserWeekPassTime.WeekPassTime(0) = CByte(GetInt(TextSun.Text))
        aUserWeekPassTime.WeekPassTime(1) = CByte(GetInt(TextMon.Text))
        aUserWeekPassTime.WeekPassTime(2) = CByte(GetInt(TextTue.Text))
        aUserWeekPassTime.WeekPassTime(3) = CByte(GetInt(TextWed.Text))
        aUserWeekPassTime.WeekPassTime(4) = CByte(GetInt(TextThu.Text))
        aUserWeekPassTime.WeekPassTime(5) = CByte(GetInt(TextFri.Text))
        aUserWeekPassTime.WeekPassTime(6) = CByte(GetInt(TextSat.Text))
    End Sub
End Class