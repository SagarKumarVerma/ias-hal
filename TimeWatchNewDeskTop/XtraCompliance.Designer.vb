﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCompliance
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SimpleButtonProcess = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButtonProcess
        '
        Me.SimpleButtonProcess.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButtonProcess.Appearance.Options.UseFont = True
        Me.SimpleButtonProcess.Location = New System.Drawing.Point(191, 79)
        Me.SimpleButtonProcess.Name = "SimpleButtonProcess"
        Me.SimpleButtonProcess.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonProcess.TabIndex = 48
        Me.SimpleButtonProcess.Text = "Process"
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(272, 29)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 24)
        Me.ComboNepaliYear.TabIndex = 47
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(165, 29)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra", "Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(101, 24)
        Me.ComboNEpaliMonth.TabIndex = 46
        '
        'ComboNepaliDate
        '
        Me.ComboNepaliDate.Location = New System.Drawing.Point(117, 29)
        Me.ComboNepaliDate.Name = "ComboNepaliDate"
        Me.ComboNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDate.Size = New System.Drawing.Size(42, 24)
        Me.ComboNepaliDate.TabIndex = 45
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(21, 31)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 18)
        Me.LabelControl6.TabIndex = 44
        Me.LabelControl6.Text = "From Date"
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = New Date(CType(0, Long))
        Me.DateEdit3.Location = New System.Drawing.Point(117, 29)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DateEdit3.Properties.Appearance.Options.UseFont = True
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit3.Size = New System.Drawing.Size(135, 24)
        Me.DateEdit3.TabIndex = 43
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(272, 79)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(61, 23)
        Me.SimpleButton1.TabIndex = 49
        Me.SimpleButton1.Text = "Close"
        '
        'XtraCompliance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(354, 137)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButtonProcess)
        Me.Controls.Add(Me.ComboNepaliYear)
        Me.Controls.Add(Me.ComboNEpaliMonth)
        Me.Controls.Add(Me.ComboNepaliDate)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.DateEdit3)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraCompliance"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compliance"
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SimpleButtonProcess As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
