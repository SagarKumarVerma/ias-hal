﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Public Class XtraCanteenMealMenu
    Dim ulf As UserLookAndFeel
    Dim cmd1 As OleDbCommand
    Dim cmd As SqlCommand
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraCanteenMealMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        loadList()
    End Sub
    Private Sub loadList()
        Dim sSql As String
        DateEdit3.DateTime = Now
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            sSql = "select * from tblmenu where FORMAT(tblmenu_date,'yyyy-MM-dd')='" & Now.ToString("yyyy-MM-dd") & "'"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            sSql = "select * from tblmenu where tblmenu_date='" & Now.ToString("yyyy-MM-dd") & "'"
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ListBoxControl1.Items.Add(ds.Tables(0).Rows(i).Item("tblmenu_item").ToString.Trim)
            Next
        End If
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        If TextEdit1.Text.Trim <> "" Then
            ListBoxControl1.Items.Add(TextEdit1.Text)
            TextEdit1.Text = ""
        End If
    End Sub
    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        ListBoxControl1.Items.RemoveAt(ListBoxControl1.SelectedIndex)
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim tblmenu_date As DateTime = DateEdit3.DateTime
        Dim tblmenu_mealtype As String
        If CheckEditBF.Checked = True Then
            tblmenu_mealtype = "B"
        ElseIf CheckEditD.Checked = True Then
            tblmenu_mealtype = "D"
        ElseIf CheckEditL.Checked = True Then
            tblmenu_mealtype = "L"
        End If
        Dim sSql As String
        If ListBoxControl1.Items.Count > 0 Then
            If Common.servername = "Access" Then
                sSql = "delete from tblmenu where FORMAT(tblmenu_date,'yyyy-MM-dd')='" & DateEdit3.DateTime.ToString("yyyy-MM-dd") & "'"
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = "delete from tblmenu where tblmenu_date='" & DateEdit3.DateTime.ToString("yyyy-MM-dd") & "'"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            For i As Integer = 0 To ListBoxControl1.Items.Count - 1
                sSql = "Insert into tblmenu(tblmenu_date,tblmenu_item,tblmenu_mealtype) values('" & tblmenu_date.ToString("yyyy-MM-dd") & "','" & ListBoxControl1.Items.Item(i).ToString.Trim & "','" & tblmenu_mealtype & "')"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            Next
        Else
            XtraMessageBox.Show(ulf, "<size=10>Please Add menu Items</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
    End Sub
End Class
