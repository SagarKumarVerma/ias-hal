﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Threading
'Imports DevExpress.XtraRichEdit.Forms.SearchHelper

Public Class XtraAutoDownloadLogs
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Delegate Sub invokeDelegate()
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
    End Sub
    Private Sub XtraAutoDownloadLogs_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin

        LabelControl1.Text = ""
        Timer1.Enabled = True
    End Sub
    Private Sub downloadLogsThread()
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer = 1 'new logs

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsM As DataSet = New DataSet
        Dim sSql As String = "select * from tblMachine"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If

        For k As Integer = 0 To dsM.Tables(0).Rows.Count - 1
            IP = dsM.Tables(0).Rows(k).Item("LOCATION").ToString.Trim
            ID_NO = dsM.Tables(0).Rows(k).Item("ID_NO").ToString.Trim
            DeviceType = dsM.Tables(0).Rows(k).Item("DeviceType").ToString.Trim
            Dim IN_OUT As String = dsM.Tables(0).Rows(k).Item("IN_OUT").ToString.Trim
            LabelControl1.Text = "Downloading data from " & IP & "  ..."
            Application.DoEvents()
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString    '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261

                Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, False, "T", "A", IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    If result.Contains("Invalid Serial number") Then
                        'SplashScreenManager.CloseForm(False)
                        XtraMessageBox.Show(ulf, "<size=10>" & result & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    Else
                        failIP.Add(IP.ToString)
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim sdwEnrollNumber As String = ""
                'Dim idwVerifyMode As Integer
                'Dim idwInOutMode As Integer
                'Dim idwYear As Integer
                'Dim idwMonth As Integer
                'Dim idwDay As Integer
                'Dim idwHour As Integer
                'Dim idwMinute As Integer
                'Dim idwSecond As Integer
                'Dim idwWorkcode As Integer

                'Dim comZK As CommonZK = New CommonZK
                'Dim bIsConnected = False
                'Dim iMachineNumber As Integer
                'Dim idwErrorCode As Integer
                'Dim com As Common = New Common
                'Dim axCZKEM1 As New zkemkeeper.CZKEM
                'bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                'If bIsConnected = True Then
                '    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                '    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                '    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                '    If axCZKEM1.ReadGeneralLogData(iMachineNumber) Then 'read all the attendance records to the memory
                '        XtraMaster.LabelControl4.Text = "Downloading from " & IP & "..."
                '        Application.DoEvents()
                '        'get records from the memory
                '        Dim x As Integer = 0
                '        Dim startdate As DateTime

                '        Dim paycodelist As New List(Of String)()
                '        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                '            paycodelist.Add(sdwEnrollNumber)
                '            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                '            If x = 0 Then
                '                startdate = punchdate
                '            End If
                '            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode)
                '            x = x + 1
                '        End While
                '        Dim paycodeArray = paycodelist.ToArray
                '        'Dim adapA As OleDbDataAdapter
                '        'Dim adap As SqlDataAdapter
                '        'Dim ds As DataSet = New DataSet
                '        For i As Integer = 0 To paycodeArray.Length - 1
                '            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & paycodeArray(i) & "'"
                '            ds = New DataSet
                '            If Common.servername = "Access" Then
                '                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                '                adapA.Fill(ds)
                '            Else
                '                adap = New SqlDataAdapter(sSqltmp, Common.con)
                '                adap.Fill(ds)
                '            End If

                '            If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                '                com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
                '            Else
                '                com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
                '            End If
                '        Next
                '    Else
                '        Cursor = Cursors.Default
                '        axCZKEM1.GetLastError(idwErrorCode)
                '        If idwErrorCode <> 0 Then
                '            'MsgBox("Reading data from terminal failed,ErrorCode: " & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                '            failIP.Add(IP.ToString)
                '        Else
                '            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                '        End If
                '    End If
                '    axCZKEM1.EnableDevice(iMachineNumber, True)
                'Else
                '    axCZKEM1.GetLastError(idwErrorCode)
                '    'MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                '    failIP.Add(IP.ToString)
                '    Continue For
                'End If
                'axCZKEM1.Disconnect()
                'If Common.servername = "Access" Then
                '    If Common.con1.State <> ConnectionState.Open Then
                '        Common.con1.Open()
                '    End If
                '    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                '    cmd1.ExecuteNonQuery()
                '    If Common.con1.State <> ConnectionState.Closed Then
                '        Common.con1.Close()
                '    End If
                'Else
                '    If Common.con.State <> ConnectionState.Open Then
                '        Common.con.Open()
                '    End If
                '    cmd = New SqlCommand("delete from Rawdata", Common.con)
                '    cmd.ExecuteNonQuery()
                '    If Common.con.State <> ConnectionState.Closed Then
                '        Common.con.Close()
                '    End If
                'End If

                Dim sdwEnrollNumber As String = ""
                Dim idwVerifyMode As Integer
                Dim idwInOutMode As Integer
                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim idwWorkcode As Integer

                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                    If axCZKEM1.ReadGeneralLogData(iMachineNumber) Then 'read all the attendance records to the memory
                        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                        Application.DoEvents()
                        'get records from the memory
                        Dim x As Integer = 0
                        Dim startdate As DateTime
                        'Dim LogResult As Boolean = axCZKEM1.ReadNewGLogData(iMachineNumber)
                        Dim paycodelist As New List(Of String)()
                        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                            paycodelist.Add(sdwEnrollNumber)
                            Dim punchdate = idwYear.ToString() & "-" & idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                            If x = 0 Then
                                startdate = punchdate
                            End If
                            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, "A", IN_OUT, ID_NO, datafile)
                            x = x + 1
                        End While

                        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(xsSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(xsSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                        Dim paycodeArray = paycodelist.ToArray
                        'Dim adapA As OleDbDataAdapter
                        'Dim adap As SqlDataAdapter
                        Dim ds As DataSet = New DataSet
                        'ds = New DataSet
                        For i As Integer = 0 To paycodeArray.Length - 1
                            Dim PRESENTCARDNO As String
                            If IsNumeric(paycodeArray(i)) Then
                                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                            Else
                                PRESENTCARDNO = paycodeArray(i)
                            End If
                            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
                            ' "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                            ds = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                adapA.Fill(ds)
                            Else
                                adap = New SqlDataAdapter(sSqltmp, Common.con)
                                adap.Fill(ds)
                            End If
                            If ds.Tables(0).Rows.Count > 0 Then
                                com.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        com.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                            End If
                        Next
                    Else
                        Cursor = Cursors.Default
                        axCZKEM1.GetLastError(idwErrorCode)
                        If idwErrorCode <> 0 Then
                            failIP.Add(IP.ToString)
                        Else
                            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                        End If
                    End If
                    'axCZKEM1.ClearGLog(iMachineNumber) 'only for javle bank

                    axCZKEM1.EnableDevice(iMachineNumber, True)
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    'MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                    failIP.Add(IP.ToString)
                    Continue For
                End If
                axCZKEM1.Disconnect()
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("delete from Rawdata", Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString    '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881

                Dim result As String = cn.funcGetGeneralLogData_f9(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, False, "T", "A", IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            End If
        Next
        Me.Close()
        'XtraMasterDashBoard.Show()
    End Sub
    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        If Common.LogDownLoadCounter = 1 Then  'if it is from login page
            'Dim downloadLogsThread = New Thread(AddressOf Me.downloadLogsThread)
            downloadLogsThread()
            Common.LogDownLoadCounter = 0
        End If
    End Sub
End Class