﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraTimeZoneEditBio
    Dim ulf As UserLookAndFeel
    'Dim cmd As New SqlCommand
    'Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub btnTZCreate_Click(sender As System.Object, e As System.EventArgs) Handles btnTZCreate.Click
        Dim lpszIPAddress As String = XtraTimeZoneBio.TZIP
        Me.Cursor = Cursors.WaitCursor
        'Dim sSql As String = ""
        'Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        'Dim result As Object() = New Object(selectedRows.Length - 1) {}
        'Dim LstMachineId As String

        'If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
        'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
        Dim bConn As Boolean = False
        Dim vnMachineNumber As Integer = 1 'LstMachineId
        Dim vnLicense As Integer = 1261 '1789 '
        'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
        Dim vpszIPAddress As String = Trim(lpszIPAddress)
        Dim vpszNetPort As Integer = CLng("5005")
        Dim vpszNetPassword As Integer = CLng("0")
        Dim vnTimeOut As Integer = CLng("5000")
        Dim vnProtocolType As Integer = 0 'PROTOCOL_TCPIP
        Dim nCommHandleIndex As Long
        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        Dim vRet As Integer
        'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
        '    vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
        'Else
        vpszIPAddress = Trim(lpszIPAddress)
        vpszNetPort = CLng("5005")
        vpszNetPassword = CLng("0")
        vnTimeOut = CLng("5000")
        vnProtocolType = 0
        vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        nCommHandleIndex = vRet
        'End If
        If vRet > 0 Then
            bConn = True
        Else
            bConn = False
        End If
        If bConn Then
            Dim bytTimeZone(SIZE_TIME_ZONE_STRUCT - 1) As Byte
            Dim vTimeZone As TIME_ZONE
            Dim nFKRetCode As Integer
            nFKRetCode = FK_EnableDevice(nCommHandleIndex, 0)            
            vTimeZone.Init()
            GetTimeZoneValue(vTimeZone)
            If (vTimeZone.TimeZoneId < 1) Or (vTimeZone.TimeZoneId > TIME_ZONE_COUNT) Then
                vTimeZone.TimeZoneId = 1
                'cmbTZN.SelectedIndex = 0
            End If
            ConvertStructureToByteArray(vTimeZone, bytTimeZone)
            nFKRetCode = FK_HS_SetTimeZone(nCommHandleIndex, bytTimeZone)
            If nFKRetCode = RUN_SUCCESS Then
                'lblMessage.Text = "Success !"
            Else
                'lblMessage.Text = ReturnResultPrint(nFKRetCode)
                XtraMessageBox.Show(ulf, "<size=10>" & ReturnResultPrint(nFKRetCode) & "</size>", "Error")
                Exit Sub
            End If

            FK_EnableDevice(nCommHandleIndex, 1)
            FK_DisConnect(nCommHandleIndex)
            Common.LogPost("Device TimeZone Set; Device Id='" & vpszIPAddress)
        Else
            'MsgBox("Device No: " & LstMachineId & " Not connected..")
            XtraMessageBox.Show(ulf, "<size=10>Device Not connected..</size>", "Information")
        End If
        'FK623Attend.Disconnect

      
        Me.Cursor = Cursors.Default
        Me.Close()
    End Sub
    Private Sub XtraTimeZoneEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEditTZCre.Text = XtraTimeZoneBio.TZID
        Dim TZarray() As String = XtraTimeZoneBio.TZTime.Split(" ")
        TxtSunStrtTime.Text = TZarray(0)
        TxtSunEndTime.Text = TZarray(1)

        TxtMonStrtTime.Text = TZarray(2)
        TxtMonEndTime.Text = TZarray(3)

        TxtTueStrtTime.Text = TZarray(4)
        TxtTueEndTime.Text = TZarray(5)

        TxtWedStrtTime.Text = TZarray(6)
        TxtWedEndTime.Text = TZarray(7)

        TxtThuStrtTime.Text = TZarray(8)
        TxtThuEndTime.Text = TZarray(9)

        TxtFriStrtTime.Text = TZarray(10)
        TxtFriEndTime.Text = TZarray(11)

        'TxtSatStrtTime.Text = TZarray(12)
        'TxtSatEndTime.Text = TZarray(13)

    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub GetTimeZoneValue(ByRef aTimeZone As TIME_ZONE)      
        aTimeZone.TimeZoneId = TextEditTZCre.Text 'cmbTZN.SelectedIndex + 1
        Dim x() As String = TxtSunStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(0).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(0).StartMinute = CByte(GetInt(x(1)))
        x = TxtSunEndTime.Text.Split(":")
        aTimeZone.TimeSlots(0).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(0).EndMinute = CByte(GetInt(x(1)))

        x = TxtMonStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(1).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(1).StartMinute = CByte(GetInt(x(1)))
        x = TxtMonEndTime.Text.Split(":")
        aTimeZone.TimeSlots(1).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(1).EndMinute = CByte(GetInt(x(1)))

        x = TxtTueStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(2).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(2).StartMinute = CByte(GetInt(x(1)))
        x = TxtTueEndTime.Text.Split(":")
        aTimeZone.TimeSlots(2).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(2).EndMinute = CByte(GetInt(x(1)))

        x = TxtWedStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(3).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(3).StartMinute = CByte(GetInt(x(1)))
        x = TxtWedEndTime.Text.Split(":")
        aTimeZone.TimeSlots(3).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(3).EndMinute = CByte(GetInt(x(1)))

        x = TxtThuStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(4).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(4).StartMinute = CByte(GetInt(x(1)))
        x = TxtThuEndTime.Text.Split(":")
        aTimeZone.TimeSlots(4).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(4).EndMinute = CByte(GetInt(x(1)))

        x = TxtFriStrtTime.Text.Split(":")
        aTimeZone.TimeSlots(5).StartHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(5).StartMinute = CByte(GetInt(x(1)))
        x = TxtFriEndTime.Text.Split(":")
        aTimeZone.TimeSlots(5).EndHour = CByte(GetInt(x(0)))
        aTimeZone.TimeSlots(5).EndMinute = CByte(GetInt(x(1)))
    End Sub
End Class