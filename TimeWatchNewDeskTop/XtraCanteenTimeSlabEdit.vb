﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraCanteenTimeSlabEdit
    Dim GrpId As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraCanteenTimeSlabEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GrpId = XtraCanteenTimeSlab.GpId
        If GrpId.Length = 0 Then
            setDefault()
        Else
            loadData()
        End If
    End Sub
    Private Sub setDefault()
        TextEditBFEnd.Text = "00:00"
        TextEditBFStrt.Text = "00:00"
        TextEditDEnd.Text = "00:00"
        TextEditDStrt.Text = "00:00"
        TextEditLEnd.Text = "00:00"
        TextEditLStrt.Text = "00:00"
        TextEditShift.Text = ""
    End Sub
    Private Sub loadData()
        Dim sSql As String = "select * from tblTimeSlab where Shift = '" & GrpId & "'"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            TextEditShift.Text = ds.Tables(0).Rows(0).Item("SHIFT").ToString.Trim
            TextEditBFStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("BStart").ToString.Trim).ToString("HH:mm")
            TextEditBFEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("BEnd").ToString.Trim).ToString("HH:mm")
            If ds.Tables(0).Rows(0).Item("MealType").ToString.Trim = "B" Then
                ComboBoxEdit1.SelectedIndex = 0
            ElseIf ds.Tables(0).Rows(0).Item("MealType").ToString.Trim = "L" Then
                ComboBoxEdit1.SelectedIndex = 1
            ElseIf ds.Tables(0).Rows(0).Item("MealType").ToString.Trim = "B" Then
                ComboBoxEdit1.SelectedIndex = 2
            End If
            'TextEditLStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("LStart").ToString.Trim).ToString("HH:mm")
            'TextEditLEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("LEnd").ToString.Trim).ToString("HH:mm")
            'TextEditDStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DStart").ToString.Trim).ToString("HH:mm")
            'TextEditDEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DEnd").ToString.Trim).ToString("HH:mm")
        Else
            setDefault()
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEditShift.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Break fast Start Time cannot be greater than End Time</size>", "<size=9>Error</size>")
            TextEditShift.Select()
            Exit Sub
        End If
        Dim MealType As String
        If ComboBoxEdit1.SelectedIndex = 0 Then
            MealType = "B"
        ElseIf ComboBoxEdit1.SelectedIndex = 1 Then
            MealType = "L"
        Else
            MealType = "D"
        End If
        Dim Shift As String = TextEditShift.Text.Trim '"01"
        Dim BStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditBFStrt.Text.Trim)
        Dim BEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditBFEnd.Text.Trim)
        'Dim LStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditLStrt.Text.Trim)
        'Dim LEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditLEnd.Text.Trim)
        'Dim DStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditDStrt.Text.Trim)
        'Dim DEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditDEnd.Text.Trim)
        Dim sSql As String = "select top 1 shift from tblTimeSlab ORDER by Shift desc"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        If BStart > BEnd Then
            XtraMessageBox.Show(ulf, "<size=10>Start Time cannot be greater than End Time</size>", "<size=9>Error</size>")
            TextEditBFStrt.Select()
            Exit Sub
        End If
        If BStart = BEnd Then
            XtraMessageBox.Show(ulf, "<size=10>Start Time and End Time cannot be same</size>", "<size=9>Error</size>")
            TextEditLStrt.Select()
            Exit Sub
        End If
        'If DStart > DEnd Then
        '    XtraMessageBox.Show(ulf, "<size=10>Dinner Start Time cannot be greater than End Time</size>", "<size=9>Error</size>")
        '    TextEditDStrt.Select()
        '    Exit Sub
        'End If
        'If BStart >= LStart And BStart <= LEnd Or BEnd >= LStart And BStart <= LEnd Or BStart >= DStart And BStart <= DEnd Or BEnd >= DStart And BStart <= DEnd Then
        '    XtraMessageBox.Show(ulf, "<size=10>Break fast cannot be in range of Lunch or Dinner</size>", "<size=9>Error</size>")
        '    TextEditBFStrt.Select()
        '    Exit Sub
        'End If
        'If LStart >= DStart And LStart <= DEnd Or LEnd >= DStart And LStart <= DEnd Then
        '    XtraMessageBox.Show(ulf, "<size=10>Lunch cannot be in range of Dinner</size>", "<size=9>Error</size>")
        '    TextEditLStrt.Select()
        '    Exit Sub
        'End If

        Dim sSqlX As String '= "select * from tblTimeSlab "
        If GrpId.Length = 0 Then
            sSqlX = "select * from tblTimeSlab "
        Else
            sSqlX = "select * from tblTimeSlab where shift <> '" & GrpId & "'"
        End If
        Dim adapX As SqlDataAdapter
        Dim adapAX As OleDbDataAdapter
        Dim dsX As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapAX = New OleDbDataAdapter(sSqlX, Common.con1)
            adapAX.Fill(dsX)
        Else
            adapX = New SqlDataAdapter(sSqlX, Common.con)
            adapX.Fill(dsX)
        End If

        Dim PURPOSECan As String
        If dsX.Tables(0).Rows.Count > 0 Then
            Dim BStartDB As DateTime
            Dim BEndDB As DateTime
            'Dim LStartDB As DateTime
            'Dim LEndDB As DateTime
            'Dim DStartDB As DateTime
            'Dim DEndDB As DateTime
            For i As Integer = 0 To dsX.Tables(0).Rows.Count - 1
                If Shift = dsX.Tables(0).Rows(i).Item("Shift").ToString.Trim Then
                    XtraMessageBox.Show(ulf, "<size=10>Slab ID already exists</size>", "<size=9>Error</size>")
                    TextEditShift.Select()
                    Exit Sub
                End If
                BStartDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BStart").ToString.Trim).ToString("HH:mm:00"))
                BEndDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BEnd").ToString.Trim).ToString("HH:mm:00"))
                'LStartDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("LStart").ToString.Trim).ToString("HH:mm:00"))
                'LEndDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("LEnd").ToString.Trim).ToString("HH:mm:00"))
                'DStartDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("DStart").ToString.Trim).ToString("HH:mm:00"))
                'DEndDB = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("DEnd").ToString.Trim).ToString("HH:mm:00"))
                If BStart >= BStartDB And BStart <= BEndDB Or BEnd >= BStartDB And BEnd <= BEndDB Then
                    XtraMessageBox.Show(ulf, "<size=10>Time slab already defined</size>", "<size=9>Error</size>")
                    TextEditBFStrt.Select()
                    Exit Sub
                    'Exit For
                    'ElseIf LStart >= LStartDB And LStart <= LEndDB Or LEnd >= LStartDB And LEnd <= LEndDB Then
                    '    XtraMessageBox.Show(ulf, "<size=10>Time slab already defined</size>", "<size=9>Error</size>")
                    '    TextEditLStrt.Select()
                    '    Exit Sub
                    '    'Exit For
                    'ElseIf DStart >= DStartDB And DStart <= DEndDB Or DEnd >= DStartDB And DEnd <= DEndDB Then
                    '    XtraMessageBox.Show(ulf, "<size=10>Time slab already defined</size>", "<size=9>Error</size>")
                    '    TextEditDStrt.Select()
                    '    Exit Sub
                    '    'Exit For
                End If
            Next
        End If
        If GrpId.Length = 0 Then
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Shift = Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString.Trim) + 1
            'Else
            '    Shift = "1"
            'End If
            'insert
            sSql = "insert into tblTimeSlab (Shift, [BStart], [BEnd],[MealType]) values('" & Shift & "', '" & BStart.ToString("yyyy-MM-dd HH:mm:ss") & "','" & BEnd.ToString("yyyy-MM-dd HH:mm:ss") & "','" & MealType & "')"
        Else
            'update
            sSql = "Update tblTimeSlab set BStart='" & BStart.ToString("yyyy-MM-dd HH:mm:ss") & "', BEnd='" & BEnd.ToString("yyyy-MM-dd HH:mm:ss") & "', MealType='" & MealType & "' where shift ='" & GrpId & "'"
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class