﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class XtraUserMgmt
    Dim ulf As UserLookAndFeel
    Public Shared UserID As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            Me.TblUser1TableAdapter1.Fill(Me.SSSDBDataSet.tblUser1)
            GridControl1.DataSource = SSSDBDataSet.tblUser1
        Else
            TblUserTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblUserTableAdapter.Fill(Me.SSSDBDataSet.tblUser)
            GridControl1.DataSource = SSSDBDataSet.tblUser
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("USER_R").ToString.Trim
                If CellId = "Admin" Then
                    Me.Validate()
                    e.Handled = True
                    XtraMessageBox.Show(ulf, "<size=10>Cannot delete Admin</size>", "<size=9>Error</size>")
                End If
                Common.LogPost("User Delete; User Name:" & CellId)
            End If
        End If
    End Sub
    Private Sub XtraUserMgmt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition
        If Common.servername = "Access" Then
            Me.TblUser1TableAdapter1.Fill(Me.SSSDBDataSet.tblUser1)
            GridControl1.DataSource = SSSDBDataSet.tblUser1
        Else
            TblUserTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblUserTableAdapter.Fill(Me.SSSDBDataSet.tblUser)
            GridControl1.DataSource = SSSDBDataSet.tblUser
        End If
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblUserTableAdapter.Update(Me.SSSDBDataSet.tblUser)
        Me.TblUser1TableAdapter1.Update(Me.SSSDBDataSet.tblUser1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblUserTableAdapter.Update(Me.SSSDBDataSet.tblUser)
        Me.TblUser1TableAdapter1.Update(Me.SSSDBDataSet.tblUser1)
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            UserID = row("USER_R").ToString.Trim
        Catch ex As Exception
            UserID = ""
        End Try
        e.Allow = False

        XtraUserMgmtEdit.ShowDialog()
        If Common.servername = "Access" Then   'to refresh the grid
            Me.TblUser1TableAdapter1.Fill(Me.SSSDBDataSet.tblUser1)
        Else
            Me.TblUserTableAdapter.Fill(Me.SSSDBDataSet.tblUser)
        End If

    End Sub
End Class
