﻿Imports System
Imports System.Runtime.InteropServices
Imports SyUserType = System.Byte
Imports SyDailyEntryTime = System.UInt32
Namespace HSeriesSampleCSharp
    Class SyFunctions        
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdOpenDevice(ByVal dk As Byte, ByVal param As IntPtr, ByVal devAddr As UInt32, ByVal password As UInt32, ByVal traceIO As Byte) As Integer
        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CloseCom() As Integer
        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdHardCloseDevice() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdTestConn2Device() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdLockDevice() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdUnLockDevice() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGotoISP() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdOpenDoor() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdOpenDoorByUDP(ByVal devID As UInt32) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdCloseDoor() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSet2Default() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdDeviceBeep() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdRebootDevice() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdClearAdmin() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdClearAllUser() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdClearAllTempUser() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdClearAllCoerceFingerPrint() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdClearAllDailyEntry() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetTime(ByRef Year As UInt16, ByRef Month As Byte, ByRef Date1 As Byte, ByRef Hour As Byte, ByRef Min As Byte, ByRef Sec As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetDeviceTime(ByVal Year As UInt16, ByVal Month As Byte, ByVal Date1 As Byte, ByVal Day As Byte, ByVal Hour As Byte, ByVal Min As Byte, ByVal Sec As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCorrectImage(ByVal fileName As Char()) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCommOption(ByRef co As SyCommOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetCommOption(ByRef co As SyCommOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetDailyEntryOption(ByRef deo As SyDailyEntryOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetDailyEntryOption(ByRef deo As SyDailyEntryOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetSystemOption(ByRef so As SySystemOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetSystemOption(ByRef so As SySystemOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetPowerManage(ByRef pm As SyPowerManage) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetPowerManage(ByRef pm As SyPowerManage) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetSystemInfo(ByRef si As SySystemInfo) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetSystemInfo(ByRef si As SySystemInfo) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetAccessOption(ByRef ao As SyAccessOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetAccessOption(ByRef ao As SyAccessOption) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetGroup(ByRef gr As SyGroupRestrict, ByVal grId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetGroup(ByRef gr As SyGroupRestrict, ByVal grId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCooperationTeam(ByRef ct As UInt16, ByVal ctId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetCooperationTeam(ByVal ct As UInt16, ByVal ctId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetAccessTimeParam(ByRef wt As SyWeekTime, ByVal wtId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetAccessTimeParam(ByRef wt As SyWeekTime, ByVal wtId As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCountOfValidUserID(ByRef cnt As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCount(ByRef cnt As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetAllValidUserID(<[In](), Out()> ByVal idIndex As UInt16(), ByVal cnt As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetFreeUserID(ByRef userId As UInt16, ByVal kind As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdDeleteUserInfo(ByVal userId As UInt16, ByVal kind As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdTemp2NormalUser(ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdDeleteCoerceFingerPrintByUserID(ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetUserInfo(ByRef uie As SyUserInfoExt, ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetUserInfo2(ByRef uie As SyUserInfoExt, ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetUserInfo(ByRef uie As SyUserInfoExt, ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdRegisterUser(ByRef uie As SyUserInfoExt, ByVal kind As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdReg(ByRef uie As SyUserInfoExt, ByVal kind As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetLastRegisterResult() As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCountOfUserFingerPrint(ByRef cnt As UInt16, ByVal userId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetUserFingerPrint(ByRef ft As SyFingerTemplate, ByVal userId As UInt16, ByVal ftId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetUserFingerPrintEx(ByRef ft As SyFingerTemplateEx, ByVal userId As UInt16, ByVal ftId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetCountOfUserFingerPrint(ByVal userId As UInt16, ByVal ftCnt As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetUserFingerPrint(ByRef ft As SyFingerTemplate, ByVal userId As UInt16, ByVal ftId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdSetUserFingerPrint(ByRef ft As SyFingerTemplateEx, ByVal userId As UInt16, ByVal ftId As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetCountOfValidDailyEntryRecord(ByRef totalCnt As Integer, ByRef unreadPos As Integer) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdGetValidDailyEntryRecord(
        <[In](), Out()> ByVal der As SyLogRecord(), ByVal location As Integer, ByVal cnt As Byte) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdStressTest(ByRef der As SyLogRecord, ByVal cnt As UInt16) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdStartDetectionNewDailyEntryRecord(ByRef nc As SyNetCfg) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdEndDetectionNewDailyEntryRecord()

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdTellNORec(ByVal devID As UInt32, ByVal recnum As UInt16, ByVal bh As UInt16()) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function CmdExtractLastDailyEntry(ByRef der As SyLogRecordEX, ByRef devId As UInt32) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function DecodeSyUserType(ByVal ut As SyUserType, ByRef power As Byte, ByRef isTemp As Byte, ByRef hasCPF As Byte)
            'Public Shared Function DecodeSyUserType(ByVal ut As Byte, ByRef power As Byte, ByRef isTemp As Byte, ByRef hasCPF As Byte) 'nitin
        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function EncodeSyUserType(ByRef ut As SyUserType, ByVal power As Byte, ByVal isTemp As Byte, ByVal hasCPF As Byte)
            'Public Shared Function EncodeSyUserType(ByRef ut As UInt32, ByVal power As Byte, ByVal isTemp As Byte, ByVal hasCPF As Byte)
        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function DecodeSyEnrollState(ByVal es As Byte, ByRef fpCnt As Byte, ByRef enablePswd As Byte, ByRef enableCard As Byte, ByRef enable As Byte)

        End Function
        <DllImport("xfic2.dll", BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function EncodeSyEnrollState(ByRef es As Byte, ByVal fpCnt As Byte, ByVal enablePswd As Byte, ByVal enableCard As Byte, ByVal enable As Byte)

        End Function
        '<DllImport("xfic2.dll")>
        'Public Declare Function DecodeSyDailyEntryType Lib "xfic2.dll" (ByVal det As Byte, ByRef kind As Byte, ByRef hasCooperated As Byte, ByRef hasSetup As Byte, ByRef hasOpenDoor As Byte, ByRef hasAlarm As Byte)

        Public Declare Sub DecodeSyDailyEntryType Lib "xfic2.dll" (ByVal det As Byte, ByRef kind As Byte, ByRef hasCooperated As Byte, ByRef hasSetup As Byte, ByRef hasOpenDoor As Byte, ByRef hasAlarm As Byte)
        'End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Sub DecodeSyDailyEntryTime(ByVal det As SyDailyEntryTime, ByRef sec As Byte, ByRef min As Byte, ByRef hour As Byte, ByRef date1 As Byte, ByRef month As Byte, ByRef year As UInt16)
            'Public Shared Function DecodeSyDailyEntryTime(ByVal det As UInt32, ByRef sec As Byte, ByRef min As Byte, ByRef hour As Byte, ByRef date1 As Byte, ByRef month As Byte, ByRef year As UInt16)

        End Sub
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function EncodeUserHoldTime(ByVal sec As Byte, ByVal min As Byte, ByVal hour As Byte, ByVal date1 As Byte, ByVal month As Byte, ByVal year As UInt16) As UInt32

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall, BestFitMapping:=True, ExactSpelling:=False, SetLastError:=False, ThrowOnUnmappableChar:=False)>
        Public Shared Function DecodeUserHoldTime(ByVal uht As UInt32, ByRef sec As Byte, ByRef min As Byte, ByRef hour As Byte, ByRef date1 As Byte, ByRef month As Byte, ByRef year As UInt16)

        End Function
        '<DllImport("xfic2.dll")>
        Public Shared Function CmdSetWorkingshift(ByVal workingShiftNum As Integer, ByVal workingShift As WorkingShift) As Integer

        End Function
        '<DllImport("xfic2.dll")>
        Public Declare Function CmdReadWorkingshift Lib "xfic2.dll" (ByVal workingShiftNum As Integer, ByRef workingShift As WorkingShift) As Integer
        '<DllImport("xfic2.dll")>
        Public Declare Function CmdReadLogDuringDate Lib "xfic2.dll" (ByVal formDate As SyDate, ByVal toDate As SyDate, ByRef logStartIndex As Integer, ByRef LogCnt As Integer) As Integer
        '<DllImport("xfic2.dll", CharSet:=CharSet.Ansi)>
        Public Declare Function CmdOpenDeviceByUsb Lib "xfic2.dll" (ByVal wcsDevName As String, ByVal devAddr As UInteger, ByVal password As UInteger, ByVal traceIO As Byte) As Integer

        '<DllImport("xfic2.dll")>
        Public Shared Function CmdSetAlarm(ByVal dayOfWeek As Integer, ByVal pSysBell As SYS_BELL) As Integer

        End Function
        '<DllImport("xfic2.dll")>
        Public Shared Function CmdReadAlarm(ByVal dayOfWeek As Integer, ByRef pSysBell As SYS_BELL) As Integer

        End Function
        <DllImport("xfic2.dll", CharSet:=CharSet.Unicode)>
        Public Shared Function OpenCom(ByVal port As String, ByVal devAddr As Integer, ByVal baudrate As UInteger) As Boolean

        End Function
    End Class
End Namespace
