﻿Imports System.Resources
Imports System.Globalization
Public Class XtraLeaveMgmt
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraLeaveMgmt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'MsgBox("master menu width " & Me.Parent.Width & vbCrLf & "height " & Me.Parent.Height)
        NavigationPane1.Width = NavigationPane1.Parent.Width
        NavigationPage1.Width = NavigationPage1.Parent.Width
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        'MsgBox(NavigationPane1.Width - NavigationPage1.Width)
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width

        If Common.LeaveMaster = "Y" Then
            NavigationPage1.PageVisible = True
        Else
            NavigationPage1.PageVisible = False
            NavigationPane1.SelectedPageIndex = 1
        End If

        If Common.LeaveAccural = "Y" Then
            NavigationPage2.PageVisible = True
        Else
            NavigationPage2.PageVisible = False
        End If
        If Common.LeaveApplication = "Y" Then
            NavigationPage3.PageVisible = True
        Else
            NavigationPage3.PageVisible = False
        End If
    End Sub
    Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs) Handles NavigationPane1.SelectedPageIndexChanged
        If NavigationPane1.SelectedPageIndex = 0 Then
            NavigationPage1.Controls.Clear()
            Dim form As UserControl = New XtraLeaveMaster
            form.Dock = DockStyle.Fill
            NavigationPage1.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 1 Then
            NavigationPage2.Controls.Clear()
            Dim form As UserControl = New XtraLeaveAccrual
            form.Dock = DockStyle.Fill
            NavigationPage2.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 2 Then
            NavigationPage3.Controls.Clear()
            Dim form As UserControl = New XtraLeaveApplication
            form.Dock = DockStyle.Fill
            NavigationPage3.Controls.Add(form)
            form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 3 Then
            '    NavigationPage4.Controls.Clear()
            '    Dim form As UserControl = New XtraShift
            '    form.Dock = DockStyle.Fill
            '    NavigationPage4.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 4 Then
            '    NavigationPage5.Controls.Clear()
            '    Dim form As UserControl = New XtraGrade
            '    form.Dock = DockStyle.Fill
            '    NavigationPage5.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 5 Then
            '    NavigationPage6.Controls.Clear()
            '    Dim form As UserControl = New XtraEmployeeGroup
            '    form.Dock = DockStyle.Fill
            '    NavigationPage6.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 6 Then
            '    NavigationPage7.Controls.Clear()
            '    Dim form As UserControl = New XtraBankMaster
            '    form.Dock = DockStyle.Fill
            '    NavigationPage7.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 7 Then
            '    NavigationPage8.Controls.Clear()
            '    Dim form As UserControl = New XtraDespansary
            '    form.Dock = DockStyle.Fill
            '    NavigationPage8.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 8 Then
            '    NavigationPage9.Controls.Clear()
            '    Dim form As UserControl = New XtraCategory
            '    form.Dock = DockStyle.Fill
            '    NavigationPage9.Controls.Add(form)
            '    form.Show()
        End If

    End Sub
End Class
