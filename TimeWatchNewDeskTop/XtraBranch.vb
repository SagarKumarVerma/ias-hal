﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text.RegularExpressions

Public Class XtraBranch
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared BrId As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControl1.DataSource = SSSDBDataSet.tblbranch1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControl1.DataSource = SSSDBDataSet.tblbranch
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraBranch_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Common.Company = "N" Then
            Me.Width = Common.NavWidth 'Me.Parent.Width
            Me.Height = Common.NavHeight 'Me.Parent.Height
            SplitContainerControl1.Width = Common.NavWidth
            SplitContainerControl1.SplitterPosition = (Common.NavWidth) * 85 / 100
            'not to do in all pages
            Common.splitforMasterMenuWidth = SplitContainerControl1.Width
            Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        Else
            Me.Width = Common.NavWidth 'Me.Parent.Width
            Me.Height = Common.NavHeight 'Me.Parent.Height
            'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
            SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        End If
      
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'LoadLocGrid()
        GridControl1.DataSource = Common.LocationNonAdmin

        If Common.BranchDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
    End Sub
    Private Sub LoadLocGrid()
        If Common.USERTYPE = "H" Then
            Dim com() As String = Common.Auth_dept.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                ls.Add(com(x).Trim)
            Next
            'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
            Dim gridselet As String = "select * from tblbranch where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                Dim WTDataTable As New DataTable("tblbranch")
                dataAdapter.Fill(WTDataTable)
                GridControl1.DataSource = WTDataTable
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                Dim WTDataTable As New DataTable("tblbranch")
                dataAdapter.Fill(WTDataTable)
                GridControl1.DataSource = WTDataTable
            End If
        End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblbranchTableAdapter.Update(Me.SSSDBDataSet.tblbranch)
        Me.Tblbranch1TableAdapter1.Update(Me.SSSDBDataSet.tblbranch1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated       
        Me.TblbranchTableAdapter.Update(Me.SSSDBDataSet.tblbranch)
        Me.Tblbranch1TableAdapter1.Update(Me.SSSDBDataSet.tblbranch1)
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("branchcode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("branchname", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        If row("Email").ToString.Trim <> "" Then
            If emailaddresscheck(row("Email").ToString.Trim) = False Then
                e.Valid = False
                e.ErrorText = "<size=10>Invalid Email Id ,"
            End If
        End If
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select branchcode from tblbranch where branchcode = '" & row(0).ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Location Code ,"
            End If
        End If
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("branchcode").ToString.Trim

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String = "select count(*) from TblEmployee where branchcode = '" & CellId & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Location already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Validate()
                    e.Handled = True
                    Exit Sub
                Else
                    sSql = "Delete from tblbranch where BRANCHCODE='" & CellId & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                    Common.LogPost("Location Delete; Location ID: " & CellId)
                    LoadLocGrid()
                End If
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            e.BindableControls(colBRANCHCODE).Enabled = True 'e.RowHandle Mod 2 = 0
        Else
            e.BindableControls(colBRANCHCODE).Enabled = False
        End If
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
        'Dim lb As LabelControl = New LabelControl
        'e.BindableControls.Add(lb)
    End Sub
    Private Function emailaddresscheck(ByVal emailaddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailaddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing

        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.BranchAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.BranchModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            BrId = row("BRANCHCODE").ToString.Trim
        Catch ex As Exception
            BrId = ""
        End Try
        e.Allow = False
        XtraBranchEdit.ShowDialog()
        Common.loadLocation()
        LoadLocGrid()
    End Sub
End Class
