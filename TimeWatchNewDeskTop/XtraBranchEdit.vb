﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Text.RegularExpressions

Public Class XtraBranchEdit
    Dim BranchId As String
    Dim ulf As UserLookAndFeel
    Dim previousBranchName As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraBranchEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Dim xe As XtraBranch = New XtraBranch
        BranchId = XtraBranch.BrId

        If BranchId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If

    End Sub
    Private Sub SetDefaultValue()
        TextLocCode.Enabled = True
        TextLocCode.Text = ""
        TextName.Text = ""
        TextEmail.Text = ""
    End Sub
    Private Sub SetFormValue()
        TextLocCode.Enabled = False
        Dim ds As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adap1 As OleDbDataAdapter

        Dim sSql As String = "select * from tblbranch where branchcode ='" & BranchId & "'"
        If Common.servername = "Access" Then
            adap1 = New OleDbDataAdapter(sSql, Common.con1)
            adap1.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        TextLocCode.Text = BranchId
        TextName.Text = ds.Tables(0).Rows(0).Item("BRANCHNAME").ToString.Trim
        TextEmail.Text = ds.Tables(0).Rows(0).Item("Email").ToString.Trim
        previousBranchName = ds.Tables(0).Rows(0).Item("BRANCHNAME").ToString.Trim
    End Sub
    Private Function emailaddresscheck(ByVal emailaddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailaddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextLocCode.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Location Code cannot be empty</size>", "<size=9>Error</size>")
            TextLocCode.Select()
            Exit Sub
        End If
        If TextName.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Location Name cannot be empty</size>", "<size=9>Error</size>")
            TextName.Select()
            Exit Sub
        End If
        If TextEmail.Text.Trim <> "" Then
            Dim emailArr() As String = TextEmail.Text.Trim.Split(",")
            For i As Integer = 0 To emailArr.Length - 1
                If emailaddresscheck(emailArr(i)) = False Then
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Email Id</size>", "<size=9>Error</size>")
                    TextEmail.Select()
                    Exit Sub
                End If
            Next
        End If

        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim sSql, sSql1 As String
        If BranchId.Length = 0 Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            sSql = "select branchcode from tblbranch where branchcode = '" & TextLocCode.Text.Trim & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Duplicate Location Code</size>", "<size=9>Error</size>")
                TextLocCode.Select()
                Exit Sub
            End If
            sSql = "INSERT into tblbranch (BRANCHCODE, BRANCHNAME, Email, LastModifiedBy, LastModifiedDate) VALUES('" & TextLocCode.Text.Trim & "','" & TextName.Text.Trim & "','" & TextEmail.Text.Trim & "','" & Common.USER_R & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "') "
            sSql1 = ""
            Common.LogPost("Location Add; Location ID: " & TextLocCode.Text.Trim)
        Else
            sSql = "Update tblbranch set BRANCHNAME='" & TextName.Text.Trim & "', Email ='" & TextEmail.Text.Trim & "', LastModifiedBy='" & Common.USER_R & "', LastModifiedDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' where BRANCHCODE ='" & BranchId & "'"
            sSql1 = "Update tblMachine set branch='" & TextName.Text.Trim & "' where branch='" & previousBranchName & "'"
            Common.LogPost("Location Update; Location ID: " & TextLocCode.Text.Trim)
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If sSql1 <> "" Then
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
            End If
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If sSql1 <> "" Then
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
            End If
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.loadDevice()
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class