﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraPieceEntry
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Public Sub New()
        InitializeComponent()        
        Common.SetGridFont(GridViewPiece, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraLeaveApplication_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        loadEmp()
        LoadPieceMaster()
        setDefault()
    End Sub
    Private Sub loadEmp()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String
        Dim ds1 As DataSet

        Dim dt As DataTable = New DataTable
        dt.Columns.Add("PAYCODE")

        sSql = "select PAYCODE from Pay_Master where vEmployeeType = 'P'"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If

        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            dt.Rows.Add(ds1.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
        Next
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        LookUpEdit1.EditValue = GridLookUpEditPiece.Properties.GetKeyValue(0)
    End Sub        
    Private Sub setDefault()
        TextEdit2.Text = ""
        LookUpEdit1.EditValue = ""
        lblCardNum.Text = ""
        lblCat.Text = ""
        lblComp.Text = ""
        lblDept.Text = ""
        lblDesi.Text = ""
        lblGrade.Text = ""
        lblName.Text = ""
        lblEmpGrp.Text = ""
        DateEditFrom.EditValue = Now
        DateEdit2.EditValue = Now
        GridLookUpEditPiece.EditValue = ""
        SimpleButton1.Enabled = False
        mskPrate.Text = ""
        mskPieceNo.Text = ""
        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = True
            ComboNEpaliMonthTo.Visible = True
            ComboNepaliDateTo.Visible = True
            DateEditFrom.Visible = False
            DateEdit2.Visible = False

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month, DateEditFrom.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateFrm.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliDateTo.Visible = False
            DateEditFrom.Visible = True
            'DateEdit2.Visible = True
        End If
    End Sub
    Private Sub LoadPieceMaster()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String
        Dim ds1 As DataSet

        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Piece Code")
        dt.Columns.Add("Piece Description")

        sSql = "  select * from tblPiece"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                dt.Rows.Add(ds1.Tables(0).Rows(i).Item("Pcode").ToString.Trim, ds1.Tables(0).Rows(i).Item("Pname").ToString.Trim)
            Next
            Dim datase As DataSet = New DataSet()
            datase.Tables.Add(dt)
            GridLookUpEditPiece.Properties.DataSource = dt
            GridLookUpEditPiece.Properties.DisplayMember = "Piece Code"
            GridLookUpEditPiece.Properties.ValueMember = "Piece Code"
            GridLookUpEditPiece.EditValue = GridLookUpEditPiece.Properties.GetKeyValue(0)
        End If
    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEditPiece.CustomDisplayText
        If (e.Value Is Nothing) Then
            Return
        End If

        Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
        Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
        If (dataRowView Is Nothing) Then
            Return
        End If

        Dim row = CType(dataRowView.Row, DataRow)
        If (row Is Nothing) Then
            Return
        End If
        e.DisplayText = (row("Piece Code").ToString & ("  " & row("Piece Description").ToString))
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim PAYCODE As String = LookUpEdit1.EditValue.ToString.Trim
        Dim FromDate As DateTime = DateEditFrom.DateTime
        Dim ToDate As DateTime = DateEdit2.DateTime

        If mskPieceNo.Text.Trim = "" Or mskPieceNo.Text.Trim = "0" Then
            XtraMessageBox.Show(ulf, "<size=10>No. of Piece cannot be empty or 0</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            mskPieceNo.Select()
            Exit Sub
        End If


        Dim LeaveCode As String = GridLookUpEditPiece.EditValue.ToString.Trim
        Dim halfttpe As String = ""
        
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEditFrom.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try
        End If

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter      

        Dim sSql As String
        Dim rsCheck As DataSet = New DataSet
        Dim Ans As Integer = 0


        If mskPieceNo.Text.Trim > 0 Then
            sSql = "select * from tblpiecedata where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'and pcode='" & GridLookUpEditPiece.EditValue.ToString.Trim & "' and entry_date = '" & DateEditFrom.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            'rsCheck = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsCheck)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsCheck)
            End If

            If rsCheck.Tables(0).Rows.Count > 0 Then
                'Ans = MsgBox("Already Exist ... Are sure to Update this record", vbQuestion + vbYesNo + vbDefaultButton1)
                If XtraMessageBox.Show(ulf, "<size=10>Already Exist ... Are sure to Update this record</size>", "<size=9>Information</size>", _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Ans = 6
                End If
                If Ans = 6 Then
                    sSql = "update tblpiecedata set pno=" & mskPieceNo.Text.Trim & ",pamount=" & Format(mskPrate.Text * mskPieceNo.Text, "00000.00") & ",prate=" & mskPrate.Text.Trim & " where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'and pcode='" & GridLookUpEditPiece.EditValue.ToString.Trim & "' and entry_date = '" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "'"
                    'Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        Common.con1.Open()
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Common.con1.Close()
                    Else
                        Common.con.Open()
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        Common.con.Close()
                    End If
                End If
            Else
                sSql = "insert into tblpiecedata (paycode,pcode,prate,pno,pamount,entry_date,mon_year) values('" & LookUpEdit1.EditValue.ToString.Trim & "','" & GridLookUpEditPiece.EditValue.ToString.Trim & "'," & mskPrate.Text.Trim & "," & mskPieceNo.Text.Trim & "," & Format(mskPrate.Text * mskPieceNo.Text, "00000.00") & ",'" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "','" & DateEditFrom.DateTime.ToString("yyyy-MM-01") & "')"
                If Common.servername = "Access" Then
                    Common.con1.Open()
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    Common.con.Open()
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
            End If
        End If
        'sSql = "select * from tblpiecedata where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' and entry_date = '" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "'"
        'adodcManual.Recordset = Cn.Execute(sSql)

        ''   grdManual.Refresh
        'txtPayCode.SetFocus()
        'mskPieceNo.Text = ""


        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10> Saved Successfully </size> ", "<size=9> Success </size>")
        setDefault()
        'MsgBox(GridLookUpEdit1.EditValue)
    End Sub
    Private Sub DateEditFrom_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditFrom.Leave
        'MsgBox(DateEdit1.DateTime.ToString)
        If DateEditFrom.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
            DateEditFrom.EditValue = Now
        End If
        DateEdit2.EditValue = DateEditFrom.EditValue
        LoadPeiceData()
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else
                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblComp.Text = ds1.Tables(0).Rows(0).Item("CompName").ToString
                lblCat.Text = ds1.Tables(0).Rows(0).Item("CATName").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
                lblEmpGrp.Text = ds1.Tables(0).Rows(0).Item("EmpGrp").ToString
                lblDesi.Text = ds1.Tables(0).Rows(0).Item("DESIGNATION").ToString
                lblGrade.Text = ds1.Tables(0).Rows(0).Item("GrdName").ToString

                SimpleButton1.Enabled = True
            End If
        End If
        leaveFlage = True
    End Sub
    Private Sub LoadPeiceData()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String
        Dim ds As DataSet = New DataSet
        sSql = "select * from tblPieceData where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and Year(MON_YEAR) = Year('" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "') AND Month(MON_YEAR) = Month('" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "')"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        GridControlPiece.DataSource = ds.Tables(0)
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControlPiece.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
            Else

                Dim row As System.Data.DataRow = GridViewPiece.GetDataRow(GridViewPiece.FocusedRowHandle)
                Dim LeaveCode As String = row("Paycode").ToString.Trim
                Dim sSql As String = "delete from tblPieceData where paycode='" & row("Paycode").ToString.Trim & "' and pcode='" & row("Pcode").ToString.Trim & "' and entry_date='" & Convert.ToDateTime(row("Entry_Date").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    Common.con1.Open()
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    Common.con.Open()
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If

                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                LoadPeiceData()
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub
    Private Sub ComboNepaliYearFrm_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYearFrm.Leave

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEditFrom.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try
        End If

        If DateEditFrom.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
            DateEditFrom.EditValue = Now
        End If
        DateEdit2.EditValue = DateEditFrom.EditValue

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)
        End If
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridViewPiece.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridViewPiece.GetDataRow(GridViewPiece.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If Common.IsNepali = "Y" Then
            Me.Cursor = Cursors.WaitCursor
            If e.Column.FieldName = "ForDate" Then
                If row("ForDate").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ForDate").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        'e.DisplayText = Vstart.Day & "-" & Common.NepaliMonth(Vstart.Month - 1).ToString & "-" & Vstart.Year
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0)

                    Catch ex As Exception
                    End Try
                End If
            End If

            If e.Column.FieldName = "ApprovedDate" Then
                If row("ApprovedDate").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ApprovedDate").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        'e.DisplayText = Vstart.Day & "-" & Common.NepaliMonth(Vstart.Month - 1).ToString & "-" & Vstart.Year
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0)
                    Catch ex As Exception
                    End Try
                End If
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub    
    Private Sub GridLookUpEditLeave_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles GridLookUpEditPiece.EditValueChanged
        If GridLookUpEditPiece.EditValue <> "" Then
            Dim sSql As String
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds1 As DataSet = New DataSet

            Dim ds As DataSet = New DataSet
            sSql = "select Prate from tblPiece where Pcode='" & GridLookUpEditPiece.EditValue & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If

            mskPrate.Text = ds.Tables(0).Rows(0).Item(0).ToString.Trim
        End If
    End Sub

End Class
