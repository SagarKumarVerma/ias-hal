/************************************************************************
 *
 *  Program : FK623Define.h
 *
 *  Purpose : Define structures and constants used for communication with Fingerkeeper family
 *
 *  Version : 2.4
 *
 ************************************************************************/

#ifndef _INC_FKDEFINEINFO
#define _INC_FKDEFINEINFO

//--------------------------------------------------------------------
// Bell Time
//--------------------------------------------------------------------
#define MAX_BELLCOUNT_DAY  24
typedef struct tagBELLINFO {
    BYTE    mValid[MAX_BELLCOUNT_DAY];
    BYTE    mHour[MAX_BELLCOUNT_DAY];
    BYTE    mMinute[MAX_BELLCOUNT_DAY];
} BELLINFO;


//--------------------------------------------------------------------
// User Info, Post/Shift Info
//--------------------------------------------------------------------
#define MAX_SHIFT_COUNT_FKDLL			24
#define MAX_POST_COUNT_FKDLL			16
#define MAX_DAY_IN_MONTH_FKDLL			31

#define VER2_POST_SHIFT_INFO_FKDLL			2
#define LEN_POST_SHIFT_INFO_V2_FKDLL		2476

typedef struct tagPOST_SHIFT_INFO_FKDLL {
	DWORD		    		StructSize;									// 0, 4
	DWORD		    		StructVer;									// 4, 4
	SHIFT_TIME_SLOT_FKDLL	ShiftTime[MAX_SHIFT_COUNT_FKDLL];			// 8, 288 (=12*24)
	POST_NAME_FKDLL			Post[MAX_POST_COUNT_FKDLL];					// 296, 2048 (=128*16)
	unsigned char			CompanyName[NAME_BYTE_COUNT_FKDLL];			// 2344, 128
	unsigned char			Reserved[4];								// 2472, 4
} POST_SHIFT_INFO_FKDLL;	// size = 2476 byte

#define VER2_USER_SHIFT_INFO_FKDLL			2
#define LEN_USER_SHIFT_INFO_V2_FKDLL		184

typedef struct tagUSER_SHIFT_INFO_FKDLL
{
	DWORD	StructSize;			// 0, 4
	DWORD	StructVer;			// 4, 4
	DWORD	UserID;				// 8, 4
	DWORD	Reserved;			// 12, 4
	BYTE	NameCode[NAME_BYTE_COUNT_FKDLL];	// 16, 128
	DWORD	PostID;								// 144, 4
	WORD	YearAssigned;						// 148, 2
	WORD	MonthAssigned;						// 150, 2
	BYTE	StartWeekdayOfMonth;				// 152, 1
	BYTE	ShiftID[MAX_DAY_IN_MONTH_FKDLL];	// 153, 31	
} USER_SHIFT_INFO_FKDLL;	// size = 184 bytes

//--------------------------------------------------------------------
// Various constants
//--------------------------------------------------------------------
#define PROTOCOL_TCPIP             0    // TCP/IP
#define PROTOCOL_UDP               1    // UDP

//=============== Backup Number Constant ===============//
#define BACKUP_FP_0                0    // Finger 0
#define BACKUP_FP_1                1    // Finger 1
#define BACKUP_FP_2                2    // Finger 2
#define BACKUP_FP_3                3    // Finger 3
#define BACKUP_FP_4                4    // Finger 4
#define BACKUP_FP_5                5    // Finger 5
#define BACKUP_FP_6                6    // Finger 6
#define BACKUP_FP_7                7    // Finger 7
#define BACKUP_FP_8                8    // Finger 8
#define BACKUP_FP_9                9    // Finger 9
#define BACKUP_PSW                10    // Password
#define BACKUP_CARD               11    // Card
#define BACKUP_FACE				  12	// Face data
#define BACKUP_VEIN_0			  20	// VEIN data 0
#define BACKUP_VEIN_19			  (BACKUP_VEIN_0 + 19)	// VEIN data 19
#define BACKUP_MAX				  40

//=============== Manipulation of SuperLogData ===============//
#define LOG_ENROLL_USER            3    // Enroll-User
#define LOG_ENROLL_MANAGER         4    // Enroll-Manager
#define LOG_ENROLL_DELFP           5    // FP Delete
#define LOG_ENROLL_DELPASS         6    // Pass Delete
#define LOG_ENROLL_DELCARD         7    // Card Delete
#define LOG_LOG_ALLDEL             8    // LogAll Delete
#define LOG_SETUP_SYS              9    // Setup Sys
#define LOG_SETUP_TIME            10    // Setup Time
#define LOG_SETUP_LOG             11    // Setup Log
#define LOG_SETUP_COMM            12    // Setup Comm
#define LOG_PASSTIME              13    // Pass Time Set
#define LOG_SETUP_DOOR            14    // Door Set Log

//=============== VerifyMode of GeneralLogData ===============//
#define LOG_FPVERIFY               1    // Fp Verify
#define LOG_PASSVERIFY             2    // Pass Verify
#define LOG_CARDVERIFY             3    // Card Verify
#define LOG_FPPASS_VERIFY          4    // Pass+Fp Verify
#define LOG_FPCARD_VERIFY          5    // Card+Fp Verify
#define LOG_PASSFP_VERIFY          6    // Pass+Fp Verify
#define LOG_CARDFP_VERIFY          7    // Card+Fp Verify
#define LOG_OPEN_DOOR             10    // Door Open
#define LOG_CLOSE_DOOR            11    // Door Close
#define LOG_OPEN_HAND             12    // Hand Open
#define LOG_OPEN_THREAT           13    // Door Open threat
#define LOG_PROG_OPEN             14    // PC   Open
#define LOG_PROG_CLOSE            15    // PC   Close
#define LOG_OPEN_IREGAL           16    // Iregal Open
#define LOG_CLOSE_IREGAL          17    // Iregal Close
#define LOG_OPEN_COVER            18    // Cover Open
#define LOG_CLOSE_COVER           19    // Cover Close

//=============== IOMode of GeneralLogData ===============//
#define LOG_IOMODE_IN              0
#define LOG_IOMODE_OUT             1
#define LOG_IOMODE_OVER_IN         2
#define LOG_IOMODE_OVER_OUT        3

//=============== Machine Privilege ===============//
#define MP_NONE                    0    // General user
#define MP_MANAGER_1               1    // Super Manager (MP_ALL = 1(for old version))
#define MP_MANAGER_2               2    // General Manager
#define MP_MANAGER_3               3    // Capable to register user

//=============== Index of  GetDeviceStatus ===============//
#define	GET_MANAGERS               1
#define	GET_USERS                  2
#define	GET_FPS                    3
#define	GET_PSWS                   4
#define	GET_SLOGS                  5
#define	GET_GLOGS                  6
#define	GET_ASLOGS                 7
#define	GET_AGLOGS                 8
#define	GET_CARDS                  9
#define	GET_FACES                  10

//=============== Index of  GetDeviceInfo ===============//
#define DI_CHKSUM				   0
#define DI_MANAGERS                1    // Numbers of Manager
#define	DI_DEVICEID				   2    // DeviceID    Insert of 2010.06.18
#define DI_LANGAUGE                3    // Language
#define DI_POWEROFF_TIME           4    // Auto-PowerOff Time
#define DI_LOCK_CTRL               5    // Lock Control
#define DI_GLOG_WARNING            6    // General-Log Warning
#define DI_SLOG_WARNING            7    // Super-Log Warning
#define DI_VERIFY_INTERVALS        8    // Verify Interval Time
#define DI_RSCOM_BPS               9    // Comm Buadrate
#define DI_DATE_SEPARATE          10    // Date Separate Symbol
#define DI_VOICE_OUT			  12		
#define DI_FACILITY_CODE          13  
#define DI_NETENABLE			  14    // Network Enable
#define DI_DATE_FORMAT            22    //  DI_DATE_FORMAT <-> DI_BELLCOUNT
#define DI_DISP_FALG			  16
#define DI_LCDKIND				  17	
#define DI_LCDCONTRAST			  18	
#define DI_CMOSKIND				  19
#define DI_TRESHOLD1N			  20
#define DI_TRESHOLD11			  21
#define DI_BELLCOUNT			  15    //  DI_DATE_FORMAT <-> DI_BELLCOUNT
#define DI_USEIDCARD			  23	
#define DI_VERIFY_KIND            24    // VerrifyKind
#define DI_VERIFYFUNC			  25
#define DI_USERS485				  26
#define DI_POWERKEYUSE			  27
#define DI_DOOROPENTIME			  28
#define DI_WIEGANDUSED			  29
#define DI_DOORSENSORKIND		  30
#define DI_LOCKDELAY			  31
#define DI_IPADDRESS		      32    // IP Address
#define DI_TCPPASSWORD			  36	
#define DI_PORTNO				  40	
#define DI_MACADDR0				  42	
#define DI_MACADDR1				  43
#define DI_MACADDR2				  44	
#define DI_MACADDR3				  45
#define DI_MACADDR4				  46
#define DI_MACADDR5				  47
#define DI_SUBNETMASK			  48	
#define DI_GATEWAY				  52	
#define DI_SERVERIPADDR			  56
#define DI_SERVERPORTNO			  60
#define DI_SERVERREQUEST		  62
#define DI_CHKSUMEX				  64
#define DI_USERS232				  65
#define DI_ALARMDELAY			  66
#define DI_SENSORDELAY			  67
#define DI_INVALIDALARM			  68
#define DI_KEYBEEP				  69
#define DI_PSTARTHOUR			  70
#define DI_PSTARTMIN			  71
#define DI_PENDHOUR				  72
#define DI_PENDMIN				  73	
#define DI_USEPREVIEW			  74
#define DI_LURKIN			      75
#define DI_UNLOCKMODE			  76
#define DI_MULTIUSERS		      77    // MultiUser
#define DI_USEJOBNO				  78			  	
#define DI_LOCKMODE				  79
#define DI_MENUPASS               80
#define DI_USESHIFTKEY			  84	
#define DI_SCREENSAVER			  85
#define DI_VOLUME				  86
#define DI_CARDREADER             87
#define DI_DEVICEID1			  88
#define DI_SHOWFPIMAGE			  90
#define DI_SLEEPTIME			  91
#define DI_DORRKIND				  92			
#define DI_USET9				  93	

//=============== Baudrate = value of DI_RSCOM_BPS ===============//
#define BPS_9600                   3
#define BPS_19200                  4
#define BPS_38400                  5
#define BPS_57600                  6
#define BPS_115200                 7

//=============== Product Data Index ===============//
#define PRODUCT_SERIALNUMBER       1    // Serial Number
#define PRODUCT_BACKUPNUMBER       2    // Backup Number
#define PRODUCT_CODE               3    // Product code
#define PRODUCT_NAME               4    // Product name
#define PRODUCT_WEB                5    // Product web
#define PRODUCT_DATE               6    // Product date
#define PRODUCT_SENDTO             7    // Product sendto

//=============== Door Status ===============//
#define DOOR_CONTROLRESET           0
#define DOOR_OPEND                 1
#define DOOR_CLOSED                2
#define DOOR_COMMNAD               3

//=============== Error code ===============//
#define RUN_SUCCESS                1    // Successful
#define RUNERR_NOSUPPORT           0    // No support
#define RUNERR_UNKNOWNERROR       -1    // Unknown error
#define RUNERR_NO_OPEN_COMM       -2    // No Open Comm
#define RUNERR_WRITE_FAIL         -3    // Write Error
#define RUNERR_READ_FAIL          -4    // Read Error
#define RUNERR_INVALID_PARAM      -5    // Parameter Error
#define RUNERR_NON_CARRYOUT       -6    // execution of command failed.
#define RUNERR_DATAARRAY_END      -7    // End of  data array
#define RUNERR_DATAARRAY_NONE     -8    // None of  data array
#define RUNERR_MEMORY             -9    // Memory Allocating Error
#define RUNERR_MIS_PASSWORD      -10    // mistake password
#define RUNERR_MEMORYOVER        -11    // full enrolldata & can`t put enrolldata
#define RUNERR_DATADOUBLE        -12    // this ID is already  existed.
#define RUNERR_MANAGEROVER       -14    // full manager & can`t put manager
#define RUNERR_FPDATAVERSION     -15    // mistake fp data version.
#define RUNERR_LOGINOUTMODE	     -16    // mistake logdata In/Out mode.  insert 2010/09/19 by chang chol hak

#endif //!_INC_FKDEFINEINFO
/************************************************************************
 *                                                                      *
 *                        End of file : FKDefine.h                      *
 *                                                                      *
 ************************************************************************/
