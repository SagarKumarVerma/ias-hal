﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraTimeOfficePolicyEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleFourPunchNight = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSetRegNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlETOP = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.TextEditAutoDwnDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleRealTime = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleIsPResentOnHLD = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsPresentOnWeekOff = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleWeekOffIncludeInDUtyRoster = New DevExpress.XtraEditors.ToggleSwitch()
        Me.TxtPermisLateMinAutoShift = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPermisEarlyMinAutoShift = New DevExpress.XtraEditors.TextEdit()
        Me.ToggleAutoShiftAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.TxtMaxEarlyDeparture = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxLateArrivalMin = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxWorkingMin = New DevExpress.XtraEditors.TextEdit()
        Me.TxtEndTimeForOutPunch = New DevExpress.XtraEditors.TextEdit()
        Me.TxtInTimeForINPunch = New DevExpress.XtraEditors.TextEdit()
        Me.TxtDuplicateCheck = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxWorkHourForShortDay = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxWorkHourForHalfDay = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPresentMarkingDur = New DevExpress.XtraEditors.TextEdit()
        Me.ToggleShortleaveMarking = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleHalfDayMarking = New DevExpress.XtraEditors.ToggleSwitch()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.TextTWIR102Port = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTimerDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchIsNepali = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchVisitor = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitchCanteen = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditZKPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBioPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleLeaveAsPerFinancialYear = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleDownloadAtStartUp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleOnlineEvents = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleOutWorkMins = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleMarkMisAsAbsent = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleMarkWOasAbsent = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleMarkAWAasAAA = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsAutoAbsentAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.TxtNoOffpresentOnweekOff = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlMarkWO = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSkipPageOnDept = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.TxtMsg = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPwd = New DevExpress.XtraEditors.TextEdit()
        Me.TxtUserId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleOverStayAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleOutWorkAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleOverTimeAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.TxtPermisEarlyDpt = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPermisLateArr = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxWorkDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleIsAtdUsedAsAccess = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsHelpApplicable = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsSmartMachine = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsInOutApplicable = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtLinesPerPage = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditOT3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOT2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOT1 = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleRoundOverTime = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleWhetherOTinMinus = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleOTisAllowedInEarlyComing = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.TxtDeductOTinWO = New DevExpress.XtraEditors.TextEdit()
        Me.TxtDeductOTinHLD = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.TxtOtRestrictDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtOtLateDur = New DevExpress.XtraEditors.TextEdit()
        Me.TxtOtEarlyDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.ToggleFourPunchNight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSetRegNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsPResentOnHLD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsPresentOnWeekOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleWeekOffIncludeInDUtyRoster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisLateMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisEarlyMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleAutoShiftAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxLateArrivalMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxWorkingMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEndTimeForOutPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtInTimeForINPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDuplicateCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxWorkHourForShortDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxWorkHourForHalfDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPresentMarkingDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleShortleaveMarking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleHalfDayMarking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOutWorkMins.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkMisAsAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkWOasAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkAWAasAAA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsAutoAbsentAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtNoOffpresentOnweekOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSkipPageOnDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.TxtMsg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUserId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOverStayAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOutWorkAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOverTimeAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisEarlyDpt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisLateArr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxWorkDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsAtdUsedAsAccess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsHelpApplicable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsSmartMachine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsInOutApplicable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLinesPerPage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckEditOT3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOT2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOT1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ToggleRoundOverTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleWhetherOTinMinus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOTisAllowedInEarlyComing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TxtDeductOTinWO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDeductOTinHLD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.TxtOtRestrictDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtLateDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtEarlyDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(11, 368)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(147, 14)
        Me.LabelControl18.TabIndex = 31
        Me.LabelControl18.Text = "Is Present On HLD Present"
        Me.LabelControl18.Visible = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(11, 344)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl17.TabIndex = 30
        Me.LabelControl17.Text = "Is Present On WO Present"
        Me.LabelControl17.Visible = False
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(10, 189)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl37.TabIndex = 29
        Me.LabelControl37.Text = "Max Late Arrival Duration "
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(10, 320)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(215, 14)
        Me.LabelControl16.TabIndex = 28
        Me.LabelControl16.Text = "Week Off include or not in Duty Roster"
        Me.LabelControl16.Visible = False
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(9, 295)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(145, 14)
        Me.LabelControl15.TabIndex = 27
        Me.LabelControl15.Text = "Permis Late Min Auto Shift"
        Me.LabelControl15.Visible = False
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(8, 269)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl14.TabIndex = 25
        Me.LabelControl14.Text = "Permis Early Min Auto Shift"
        Me.LabelControl14.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(9, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Setup Register No."
        '
        'ToggleFourPunchNight
        '
        Me.ToggleFourPunchNight.Location = New System.Drawing.Point(267, 62)
        Me.ToggleFourPunchNight.Name = "ToggleFourPunchNight"
        Me.ToggleFourPunchNight.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleFourPunchNight.Properties.Appearance.Options.UseFont = True
        Me.ToggleFourPunchNight.Properties.OffText = "No"
        Me.ToggleFourPunchNight.Properties.OnText = "Yes"
        Me.ToggleFourPunchNight.Size = New System.Drawing.Size(95, 25)
        Me.ToggleFourPunchNight.TabIndex = 2
        Me.ToggleFourPunchNight.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(8, 39)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(109, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Duplicate Check Min"
        Me.LabelControl2.Visible = False
        '
        'TxtSetRegNo
        '
        Me.TxtSetRegNo.Enabled = False
        Me.TxtSetRegNo.Location = New System.Drawing.Point(267, 10)
        Me.TxtSetRegNo.Name = "TxtSetRegNo"
        Me.TxtSetRegNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtSetRegNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSetRegNo.Properties.Appearance.Options.UseFont = True
        Me.TxtSetRegNo.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtSetRegNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtSetRegNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSetRegNo.Properties.MaxLength = 12
        Me.TxtSetRegNo.Size = New System.Drawing.Size(72, 20)
        Me.TxtSetRegNo.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(8, 67)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Four Punch in Night Shift"
        Me.LabelControl3.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(9, 96)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(125, 14)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "End Time for IN punch"
        Me.LabelControl4.Visible = False
        '
        'LabelControlETOP
        '
        Me.LabelControlETOP.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlETOP.Appearance.Options.UseFont = True
        Me.LabelControlETOP.Appearance.Options.UseTextOptions = True
        Me.LabelControlETOP.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LabelControlETOP.Location = New System.Drawing.Point(9, 119)
        Me.LabelControlETOP.Name = "LabelControlETOP"
        Me.LabelControlETOP.Size = New System.Drawing.Size(413, 14)
        Me.LabelControlETOP.TabIndex = 7
        Me.LabelControlETOP.Text = "End Time for Out punch(Next Date) for RTC Employee with Multiple Punch"
        Me.LabelControlETOP.Visible = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(10, 241)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(103, 14)
        Me.LabelControl13.TabIndex = 16
        Me.LabelControl13.Text = "Auto Shift Allowed"
        Me.LabelControl13.Visible = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(23, 581)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(210, 14)
        Me.LabelControl12.TabIndex = 15
        Me.LabelControl12.Text = "Maximum Working Hours for Short day"
        Me.LabelControl12.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(10, 163)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(172, 14)
        Me.LabelControl6.TabIndex = 9
        Me.LabelControl6.Text = "Maximum Working Min to Verify"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(23, 555)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(199, 14)
        Me.LabelControl11.TabIndex = 14
        Me.LabelControl11.Text = "Maximum Working Hours for half day"
        Me.LabelControl11.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(8, 213)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(159, 14)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "Max Early Departure Duration"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(22, 529)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl10.TabIndex = 13
        Me.LabelControl10.Text = "Present Marking Duration"
        Me.LabelControl10.Visible = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(23, 477)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl8.TabIndex = 11
        Me.LabelControl8.Text = "Half Day marking"
        Me.LabelControl8.Visible = False
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(23, 502)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl9.TabIndex = 12
        Me.LabelControl9.Text = "Short leave marking"
        Me.LabelControl9.Visible = False
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.TextEditAutoDwnDur)
        Me.PanelControl1.Controls.Add(Me.LabelControl60)
        Me.PanelControl1.Controls.Add(Me.ToggleRealTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl57)
        Me.PanelControl1.Controls.Add(Me.ToggleIsPResentOnHLD)
        Me.PanelControl1.Controls.Add(Me.ToggleIsPresentOnWeekOff)
        Me.PanelControl1.Controls.Add(Me.ToggleWeekOffIncludeInDUtyRoster)
        Me.PanelControl1.Controls.Add(Me.TxtPermisLateMinAutoShift)
        Me.PanelControl1.Controls.Add(Me.TxtPermisEarlyMinAutoShift)
        Me.PanelControl1.Controls.Add(Me.ToggleAutoShiftAllowed)
        Me.PanelControl1.Controls.Add(Me.TxtMaxEarlyDeparture)
        Me.PanelControl1.Controls.Add(Me.TxtMaxLateArrivalMin)
        Me.PanelControl1.Controls.Add(Me.TxtMaxWorkingMin)
        Me.PanelControl1.Controls.Add(Me.TxtEndTimeForOutPunch)
        Me.PanelControl1.Controls.Add(Me.TxtInTimeForINPunch)
        Me.PanelControl1.Controls.Add(Me.TxtDuplicateCheck)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.TxtSetRegNo)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.LabelControlETOP)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.ToggleFourPunchNight)
        Me.PanelControl1.Controls.Add(Me.LabelControl37)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Location = New System.Drawing.Point(4, 2)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(375, 450)
        Me.PanelControl1.TabIndex = 1
        '
        'TextEditAutoDwnDur
        '
        Me.TextEditAutoDwnDur.EditValue = "240"
        Me.TextEditAutoDwnDur.Location = New System.Drawing.Point(267, 419)
        Me.TextEditAutoDwnDur.Name = "TextEditAutoDwnDur"
        Me.TextEditAutoDwnDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditAutoDwnDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditAutoDwnDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditAutoDwnDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditAutoDwnDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditAutoDwnDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditAutoDwnDur.Properties.MaxLength = 3
        Me.TextEditAutoDwnDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditAutoDwnDur.TabIndex = 15
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl60.Appearance.Options.UseFont = True
        Me.LabelControl60.Location = New System.Drawing.Point(9, 422)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(172, 14)
        Me.LabelControl60.TabIndex = 74
        Me.LabelControl60.Text = "Auto Download Duration (Mins)"
        '
        'ToggleRealTime
        '
        Me.ToggleRealTime.Location = New System.Drawing.Point(267, 388)
        Me.ToggleRealTime.Name = "ToggleRealTime"
        Me.ToggleRealTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleRealTime.Properties.Appearance.Options.UseFont = True
        Me.ToggleRealTime.Properties.OffText = "No"
        Me.ToggleRealTime.Properties.OnText = "Yes"
        Me.ToggleRealTime.Size = New System.Drawing.Size(95, 25)
        Me.ToggleRealTime.TabIndex = 14
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl57.Appearance.Options.UseFont = True
        Me.LabelControl57.Location = New System.Drawing.Point(11, 393)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl57.TabIndex = 72
        Me.LabelControl57.Text = "Start Real Time On StartUp"
        '
        'ToggleIsPResentOnHLD
        '
        Me.ToggleIsPResentOnHLD.Location = New System.Drawing.Point(267, 363)
        Me.ToggleIsPResentOnHLD.Name = "ToggleIsPResentOnHLD"
        Me.ToggleIsPResentOnHLD.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsPResentOnHLD.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsPResentOnHLD.Properties.OffText = "No"
        Me.ToggleIsPResentOnHLD.Properties.OnText = "Yes"
        Me.ToggleIsPResentOnHLD.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsPResentOnHLD.TabIndex = 13
        Me.ToggleIsPResentOnHLD.Visible = False
        '
        'ToggleIsPresentOnWeekOff
        '
        Me.ToggleIsPresentOnWeekOff.Location = New System.Drawing.Point(267, 339)
        Me.ToggleIsPresentOnWeekOff.Name = "ToggleIsPresentOnWeekOff"
        Me.ToggleIsPresentOnWeekOff.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsPresentOnWeekOff.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsPresentOnWeekOff.Properties.OffText = "No"
        Me.ToggleIsPresentOnWeekOff.Properties.OnText = "Yes"
        Me.ToggleIsPresentOnWeekOff.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsPresentOnWeekOff.TabIndex = 12
        Me.ToggleIsPresentOnWeekOff.Visible = False
        '
        'ToggleWeekOffIncludeInDUtyRoster
        '
        Me.ToggleWeekOffIncludeInDUtyRoster.Location = New System.Drawing.Point(267, 315)
        Me.ToggleWeekOffIncludeInDUtyRoster.Name = "ToggleWeekOffIncludeInDUtyRoster"
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.Appearance.Options.UseFont = True
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.OffText = "No"
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.OnText = "Yes"
        Me.ToggleWeekOffIncludeInDUtyRoster.Size = New System.Drawing.Size(95, 25)
        Me.ToggleWeekOffIncludeInDUtyRoster.TabIndex = 11
        Me.ToggleWeekOffIncludeInDUtyRoster.Visible = False
        '
        'TxtPermisLateMinAutoShift
        '
        Me.TxtPermisLateMinAutoShift.EditValue = "240"
        Me.TxtPermisLateMinAutoShift.Location = New System.Drawing.Point(267, 292)
        Me.TxtPermisLateMinAutoShift.Name = "TxtPermisLateMinAutoShift"
        Me.TxtPermisLateMinAutoShift.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisLateMinAutoShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisLateMinAutoShift.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisLateMinAutoShift.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisLateMinAutoShift.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisLateMinAutoShift.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisLateMinAutoShift.Properties.MaxLength = 3
        Me.TxtPermisLateMinAutoShift.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisLateMinAutoShift.TabIndex = 10
        Me.TxtPermisLateMinAutoShift.Visible = False
        '
        'TxtPermisEarlyMinAutoShift
        '
        Me.TxtPermisEarlyMinAutoShift.EditValue = "240"
        Me.TxtPermisEarlyMinAutoShift.Location = New System.Drawing.Point(267, 266)
        Me.TxtPermisEarlyMinAutoShift.Name = "TxtPermisEarlyMinAutoShift"
        Me.TxtPermisEarlyMinAutoShift.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisEarlyMinAutoShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisEarlyMinAutoShift.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisEarlyMinAutoShift.Properties.MaxLength = 3
        Me.TxtPermisEarlyMinAutoShift.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisEarlyMinAutoShift.TabIndex = 9
        Me.TxtPermisEarlyMinAutoShift.Visible = False
        '
        'ToggleAutoShiftAllowed
        '
        Me.ToggleAutoShiftAllowed.Location = New System.Drawing.Point(267, 236)
        Me.ToggleAutoShiftAllowed.Name = "ToggleAutoShiftAllowed"
        Me.ToggleAutoShiftAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAutoShiftAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleAutoShiftAllowed.Properties.OffText = "No"
        Me.ToggleAutoShiftAllowed.Properties.OnText = "Yes"
        Me.ToggleAutoShiftAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAutoShiftAllowed.TabIndex = 8
        Me.ToggleAutoShiftAllowed.Visible = False
        '
        'TxtMaxEarlyDeparture
        '
        Me.TxtMaxEarlyDeparture.EditValue = "240"
        Me.TxtMaxEarlyDeparture.Location = New System.Drawing.Point(267, 210)
        Me.TxtMaxEarlyDeparture.Name = "TxtMaxEarlyDeparture"
        Me.TxtMaxEarlyDeparture.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxEarlyDeparture.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxEarlyDeparture.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxEarlyDeparture.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxEarlyDeparture.Properties.MaxLength = 3
        Me.TxtMaxEarlyDeparture.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxEarlyDeparture.TabIndex = 7
        '
        'TxtMaxLateArrivalMin
        '
        Me.TxtMaxLateArrivalMin.EditValue = "240"
        Me.TxtMaxLateArrivalMin.Location = New System.Drawing.Point(267, 186)
        Me.TxtMaxLateArrivalMin.Name = "TxtMaxLateArrivalMin"
        Me.TxtMaxLateArrivalMin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxLateArrivalMin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxLateArrivalMin.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxLateArrivalMin.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxLateArrivalMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxLateArrivalMin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxLateArrivalMin.Properties.MaxLength = 3
        Me.TxtMaxLateArrivalMin.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxLateArrivalMin.TabIndex = 6
        '
        'TxtMaxWorkingMin
        '
        Me.TxtMaxWorkingMin.EditValue = "1020"
        Me.TxtMaxWorkingMin.Location = New System.Drawing.Point(267, 160)
        Me.TxtMaxWorkingMin.Name = "TxtMaxWorkingMin"
        Me.TxtMaxWorkingMin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxWorkingMin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxWorkingMin.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxWorkingMin.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxWorkingMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxWorkingMin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxWorkingMin.Properties.MaxLength = 4
        Me.TxtMaxWorkingMin.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxWorkingMin.TabIndex = 5
        '
        'TxtEndTimeForOutPunch
        '
        Me.TxtEndTimeForOutPunch.EditValue = "05:00"
        Me.TxtEndTimeForOutPunch.Location = New System.Drawing.Point(267, 119)
        Me.TxtEndTimeForOutPunch.Name = "TxtEndTimeForOutPunch"
        Me.TxtEndTimeForOutPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtEndTimeForOutPunch.Properties.Appearance.Options.UseFont = True
        Me.TxtEndTimeForOutPunch.Properties.Mask.EditMask = "HH:mm"
        Me.TxtEndTimeForOutPunch.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtEndTimeForOutPunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtEndTimeForOutPunch.Properties.MaxLength = 5
        Me.TxtEndTimeForOutPunch.Size = New System.Drawing.Size(72, 20)
        Me.TxtEndTimeForOutPunch.TabIndex = 4
        Me.TxtEndTimeForOutPunch.Visible = False
        '
        'TxtInTimeForINPunch
        '
        Me.TxtInTimeForINPunch.EditValue = "05:00"
        Me.TxtInTimeForINPunch.Location = New System.Drawing.Point(267, 93)
        Me.TxtInTimeForINPunch.Name = "TxtInTimeForINPunch"
        Me.TxtInTimeForINPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtInTimeForINPunch.Properties.Appearance.Options.UseFont = True
        Me.TxtInTimeForINPunch.Properties.Mask.EditMask = "HH:mm"
        Me.TxtInTimeForINPunch.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtInTimeForINPunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtInTimeForINPunch.Properties.MaxLength = 5
        Me.TxtInTimeForINPunch.Size = New System.Drawing.Size(72, 20)
        Me.TxtInTimeForINPunch.TabIndex = 3
        Me.TxtInTimeForINPunch.Visible = False
        '
        'TxtDuplicateCheck
        '
        Me.TxtDuplicateCheck.EditValue = "5"
        Me.TxtDuplicateCheck.Location = New System.Drawing.Point(267, 36)
        Me.TxtDuplicateCheck.Name = "TxtDuplicateCheck"
        Me.TxtDuplicateCheck.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDuplicateCheck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDuplicateCheck.Properties.Appearance.Options.UseFont = True
        Me.TxtDuplicateCheck.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDuplicateCheck.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDuplicateCheck.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDuplicateCheck.Properties.MaxLength = 2
        Me.TxtDuplicateCheck.Size = New System.Drawing.Size(72, 20)
        Me.TxtDuplicateCheck.TabIndex = 1
        Me.TxtDuplicateCheck.Visible = False
        '
        'TxtMaxWorkHourForShortDay
        '
        Me.TxtMaxWorkHourForShortDay.EditValue = "120"
        Me.TxtMaxWorkHourForShortDay.Location = New System.Drawing.Point(280, 578)
        Me.TxtMaxWorkHourForShortDay.Name = "TxtMaxWorkHourForShortDay"
        Me.TxtMaxWorkHourForShortDay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxWorkHourForShortDay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxWorkHourForShortDay.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxWorkHourForShortDay.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxWorkHourForShortDay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxWorkHourForShortDay.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxWorkHourForShortDay.Properties.MaxLength = 3
        Me.TxtMaxWorkHourForShortDay.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxWorkHourForShortDay.TabIndex = 12
        Me.TxtMaxWorkHourForShortDay.Visible = False
        '
        'TxtMaxWorkHourForHalfDay
        '
        Me.TxtMaxWorkHourForHalfDay.EditValue = "300"
        Me.TxtMaxWorkHourForHalfDay.Location = New System.Drawing.Point(280, 552)
        Me.TxtMaxWorkHourForHalfDay.Name = "TxtMaxWorkHourForHalfDay"
        Me.TxtMaxWorkHourForHalfDay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxWorkHourForHalfDay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxWorkHourForHalfDay.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxWorkHourForHalfDay.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxWorkHourForHalfDay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxWorkHourForHalfDay.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxWorkHourForHalfDay.Properties.MaxLength = 3
        Me.TxtMaxWorkHourForHalfDay.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxWorkHourForHalfDay.TabIndex = 11
        Me.TxtMaxWorkHourForHalfDay.Visible = False
        '
        'TxtPresentMarkingDur
        '
        Me.TxtPresentMarkingDur.EditValue = "240"
        Me.TxtPresentMarkingDur.Location = New System.Drawing.Point(280, 526)
        Me.TxtPresentMarkingDur.Name = "TxtPresentMarkingDur"
        Me.TxtPresentMarkingDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPresentMarkingDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPresentMarkingDur.Properties.Appearance.Options.UseFont = True
        Me.TxtPresentMarkingDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPresentMarkingDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPresentMarkingDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPresentMarkingDur.Properties.MaxLength = 3
        Me.TxtPresentMarkingDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtPresentMarkingDur.TabIndex = 10
        Me.TxtPresentMarkingDur.Visible = False
        '
        'ToggleShortleaveMarking
        '
        Me.ToggleShortleaveMarking.Location = New System.Drawing.Point(283, 607)
        Me.ToggleShortleaveMarking.Name = "ToggleShortleaveMarking"
        Me.ToggleShortleaveMarking.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleShortleaveMarking.Properties.Appearance.Options.UseFont = True
        Me.ToggleShortleaveMarking.Properties.OffText = "No"
        Me.ToggleShortleaveMarking.Properties.OnText = "Yes"
        Me.ToggleShortleaveMarking.Size = New System.Drawing.Size(95, 25)
        Me.ToggleShortleaveMarking.TabIndex = 9
        Me.ToggleShortleaveMarking.Visible = False
        '
        'ToggleHalfDayMarking
        '
        Me.ToggleHalfDayMarking.Location = New System.Drawing.Point(280, 472)
        Me.ToggleHalfDayMarking.Name = "ToggleHalfDayMarking"
        Me.ToggleHalfDayMarking.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleHalfDayMarking.Properties.Appearance.Options.UseFont = True
        Me.ToggleHalfDayMarking.Properties.OffText = "No"
        Me.ToggleHalfDayMarking.Properties.OnText = "Yes"
        Me.ToggleHalfDayMarking.Size = New System.Drawing.Size(95, 25)
        Me.ToggleHalfDayMarking.TabIndex = 8
        Me.ToggleHalfDayMarking.Visible = False
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(1015, 431)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 8
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(927, 431)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 7
        Me.SimpleButtonSave.Text = "Save"
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.TextTWIR102Port)
        Me.PanelControl2.Controls.Add(Me.LabelControl59)
        Me.PanelControl2.Controls.Add(Me.TextEditTimerDur)
        Me.PanelControl2.Controls.Add(Me.LabelControl58)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitchIsNepali)
        Me.PanelControl2.Controls.Add(Me.LabelControl56)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitchVisitor)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitchCanteen)
        Me.PanelControl2.Controls.Add(Me.LabelControl54)
        Me.PanelControl2.Controls.Add(Me.LabelControl55)
        Me.PanelControl2.Controls.Add(Me.TextEditZKPort)
        Me.PanelControl2.Controls.Add(Me.LabelControl53)
        Me.PanelControl2.Controls.Add(Me.TextEditBioPort)
        Me.PanelControl2.Controls.Add(Me.LabelControl30)
        Me.PanelControl2.Controls.Add(Me.ToggleLeaveAsPerFinancialYear)
        Me.PanelControl2.Controls.Add(Me.ToggleDownloadAtStartUp)
        Me.PanelControl2.Controls.Add(Me.ToggleOnlineEvents)
        Me.PanelControl2.Controls.Add(Me.ToggleOutWorkMins)
        Me.PanelControl2.Controls.Add(Me.ToggleMarkMisAsAbsent)
        Me.PanelControl2.Controls.Add(Me.ToggleMarkWOasAbsent)
        Me.PanelControl2.Controls.Add(Me.ToggleMarkAWAasAAA)
        Me.PanelControl2.Controls.Add(Me.ToggleIsAutoAbsentAllowed)
        Me.PanelControl2.Controls.Add(Me.TxtNoOffpresentOnweekOff)
        Me.PanelControl2.Controls.Add(Me.LabelControl41)
        Me.PanelControl2.Controls.Add(Me.LabelControl40)
        Me.PanelControl2.Controls.Add(Me.LabelControl39)
        Me.PanelControl2.Controls.Add(Me.LabelControl22)
        Me.PanelControl2.Controls.Add(Me.LabelControl26)
        Me.PanelControl2.Controls.Add(Me.LabelControl27)
        Me.PanelControl2.Controls.Add(Me.LabelControlMarkWO)
        Me.PanelControl2.Controls.Add(Me.LabelControl34)
        Me.PanelControl2.Controls.Add(Me.LabelControl38)
        Me.PanelControl2.Location = New System.Drawing.Point(385, 2)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(357, 450)
        Me.PanelControl2.TabIndex = 2
        '
        'TextTWIR102Port
        '
        Me.TextTWIR102Port.EditValue = "15001"
        Me.TextTWIR102Port.Location = New System.Drawing.Point(225, 317)
        Me.TextTWIR102Port.Name = "TextTWIR102Port"
        Me.TextTWIR102Port.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextTWIR102Port.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTWIR102Port.Properties.Appearance.Options.UseFont = True
        Me.TextTWIR102Port.Properties.Mask.EditMask = "[0-9]*"
        Me.TextTWIR102Port.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTWIR102Port.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextTWIR102Port.Properties.MaxLength = 5
        Me.TextTWIR102Port.Size = New System.Drawing.Size(72, 20)
        Me.TextTWIR102Port.TabIndex = 13
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl59.Appearance.Options.UseFont = True
        Me.LabelControl59.Location = New System.Drawing.Point(2, 320)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(194, 14)
        Me.LabelControl59.TabIndex = 73
        Me.LabelControl59.Text = "TWIR102 Real Time Download Port"
        '
        'TextEditTimerDur
        '
        Me.TextEditTimerDur.EditValue = "10"
        Me.TextEditTimerDur.Location = New System.Drawing.Point(225, 195)
        Me.TextEditTimerDur.Name = "TextEditTimerDur"
        Me.TextEditTimerDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditTimerDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTimerDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditTimerDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTimerDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTimerDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTimerDur.Properties.MaxLength = 4
        Me.TextEditTimerDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditTimerDur.TabIndex = 8
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Location = New System.Drawing.Point(4, 198)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(158, 14)
        Me.LabelControl58.TabIndex = 72
        Me.LabelControl58.Text = "Cloud Events Timer(Minutes)"
        '
        'ToggleSwitchIsNepali
        '
        Me.ToggleSwitchIsNepali.Location = New System.Drawing.Point(225, 388)
        Me.ToggleSwitchIsNepali.Name = "ToggleSwitchIsNepali"
        Me.ToggleSwitchIsNepali.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchIsNepali.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchIsNepali.Properties.OffText = "No"
        Me.ToggleSwitchIsNepali.Properties.OnText = "Yes"
        Me.ToggleSwitchIsNepali.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchIsNepali.TabIndex = 16
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Location = New System.Drawing.Point(3, 393)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl56.TabIndex = 70
        Me.LabelControl56.Text = "Use Nepali Calendar"
        '
        'ToggleSwitchVisitor
        '
        Me.ToggleSwitchVisitor.Location = New System.Drawing.Point(225, 363)
        Me.ToggleSwitchVisitor.Name = "ToggleSwitchVisitor"
        Me.ToggleSwitchVisitor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchVisitor.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchVisitor.Properties.OffText = "No"
        Me.ToggleSwitchVisitor.Properties.OnText = "Yes"
        Me.ToggleSwitchVisitor.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchVisitor.TabIndex = 15
        '
        'ToggleSwitchCanteen
        '
        Me.ToggleSwitchCanteen.Location = New System.Drawing.Point(225, 338)
        Me.ToggleSwitchCanteen.Name = "ToggleSwitchCanteen"
        Me.ToggleSwitchCanteen.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchCanteen.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchCanteen.Properties.OffText = "No"
        Me.ToggleSwitchCanteen.Properties.OnText = "Yes"
        Me.ToggleSwitchCanteen.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchCanteen.TabIndex = 14
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Location = New System.Drawing.Point(3, 368)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl54.TabIndex = 67
        Me.LabelControl54.Text = "Enable Visitor"
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl55.Appearance.Options.UseFont = True
        Me.LabelControl55.Location = New System.Drawing.Point(3, 343)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl55.TabIndex = 66
        Me.LabelControl55.Text = "Enable Canteen"
        '
        'TextEditZKPort
        '
        Me.TextEditZKPort.EditValue = "8081"
        Me.TextEditZKPort.Location = New System.Drawing.Point(225, 294)
        Me.TextEditZKPort.Name = "TextEditZKPort"
        Me.TextEditZKPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditZKPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditZKPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditZKPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditZKPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditZKPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditZKPort.Properties.MaxLength = 4
        Me.TextEditZKPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditZKPort.TabIndex = 12
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(2, 297)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(199, 14)
        Me.LabelControl53.TabIndex = 64
        Me.LabelControl53.Text = "ZK/Bio Pro Real Time Download Port"
        '
        'TextEditBioPort
        '
        Me.TextEditBioPort.EditValue = "7005"
        Me.TextEditBioPort.Location = New System.Drawing.Point(225, 270)
        Me.TextEditBioPort.Name = "TextEditBioPort"
        Me.TextEditBioPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditBioPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBioPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditBioPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditBioPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditBioPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBioPort.Properties.MaxLength = 4
        Me.TextEditBioPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBioPort.TabIndex = 11
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(2, 273)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(194, 14)
        Me.LabelControl30.TabIndex = 62
        Me.LabelControl30.Text = "Bio Series Real Time Download Port"
        '
        'ToggleLeaveAsPerFinancialYear
        '
        Me.ToggleLeaveAsPerFinancialYear.Location = New System.Drawing.Point(225, 242)
        Me.ToggleLeaveAsPerFinancialYear.Name = "ToggleLeaveAsPerFinancialYear"
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Options.UseFont = True
        Me.ToggleLeaveAsPerFinancialYear.Properties.OffText = "No"
        Me.ToggleLeaveAsPerFinancialYear.Properties.OnText = "Yes"
        Me.ToggleLeaveAsPerFinancialYear.Size = New System.Drawing.Size(95, 25)
        Me.ToggleLeaveAsPerFinancialYear.TabIndex = 10
        '
        'ToggleDownloadAtStartUp
        '
        Me.ToggleDownloadAtStartUp.Location = New System.Drawing.Point(225, 217)
        Me.ToggleDownloadAtStartUp.Name = "ToggleDownloadAtStartUp"
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Options.UseFont = True
        Me.ToggleDownloadAtStartUp.Properties.OffText = "No"
        Me.ToggleDownloadAtStartUp.Properties.OnText = "Yes"
        Me.ToggleDownloadAtStartUp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleDownloadAtStartUp.TabIndex = 9
        '
        'ToggleOnlineEvents
        '
        Me.ToggleOnlineEvents.Location = New System.Drawing.Point(225, 168)
        Me.ToggleOnlineEvents.Name = "ToggleOnlineEvents"
        Me.ToggleOnlineEvents.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOnlineEvents.Properties.Appearance.Options.UseFont = True
        Me.ToggleOnlineEvents.Properties.OffText = "No"
        Me.ToggleOnlineEvents.Properties.OnText = "Yes"
        Me.ToggleOnlineEvents.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOnlineEvents.TabIndex = 7
        '
        'ToggleOutWorkMins
        '
        Me.ToggleOutWorkMins.Location = New System.Drawing.Point(225, 142)
        Me.ToggleOutWorkMins.Name = "ToggleOutWorkMins"
        Me.ToggleOutWorkMins.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOutWorkMins.Properties.Appearance.Options.UseFont = True
        Me.ToggleOutWorkMins.Properties.OffText = "No"
        Me.ToggleOutWorkMins.Properties.OnText = "Yes"
        Me.ToggleOutWorkMins.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOutWorkMins.TabIndex = 6
        Me.ToggleOutWorkMins.Visible = False
        '
        'ToggleMarkMisAsAbsent
        '
        Me.ToggleMarkMisAsAbsent.Location = New System.Drawing.Point(225, 117)
        Me.ToggleMarkMisAsAbsent.Name = "ToggleMarkMisAsAbsent"
        Me.ToggleMarkMisAsAbsent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkMisAsAbsent.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkMisAsAbsent.Properties.OffText = "No"
        Me.ToggleMarkMisAsAbsent.Properties.OnText = "Yes"
        Me.ToggleMarkMisAsAbsent.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkMisAsAbsent.TabIndex = 5
        Me.ToggleMarkMisAsAbsent.Visible = False
        '
        'ToggleMarkWOasAbsent
        '
        Me.ToggleMarkWOasAbsent.Location = New System.Drawing.Point(223, 82)
        Me.ToggleMarkWOasAbsent.Name = "ToggleMarkWOasAbsent"
        Me.ToggleMarkWOasAbsent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkWOasAbsent.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkWOasAbsent.Properties.OffText = "No"
        Me.ToggleMarkWOasAbsent.Properties.OnText = "Yes"
        Me.ToggleMarkWOasAbsent.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkWOasAbsent.TabIndex = 4
        Me.ToggleMarkWOasAbsent.Visible = False
        '
        'ToggleMarkAWAasAAA
        '
        Me.ToggleMarkAWAasAAA.Location = New System.Drawing.Point(223, 56)
        Me.ToggleMarkAWAasAAA.Name = "ToggleMarkAWAasAAA"
        Me.ToggleMarkAWAasAAA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkAWAasAAA.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkAWAasAAA.Properties.OffText = "No"
        Me.ToggleMarkAWAasAAA.Properties.OnText = "Yes"
        Me.ToggleMarkAWAasAAA.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkAWAasAAA.TabIndex = 3
        Me.ToggleMarkAWAasAAA.Visible = False
        '
        'ToggleIsAutoAbsentAllowed
        '
        Me.ToggleIsAutoAbsentAllowed.Location = New System.Drawing.Point(223, 32)
        Me.ToggleIsAutoAbsentAllowed.Name = "ToggleIsAutoAbsentAllowed"
        Me.ToggleIsAutoAbsentAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsAutoAbsentAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsAutoAbsentAllowed.Properties.OffText = "No"
        Me.ToggleIsAutoAbsentAllowed.Properties.OnText = "Yes"
        Me.ToggleIsAutoAbsentAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsAutoAbsentAllowed.TabIndex = 2
        Me.ToggleIsAutoAbsentAllowed.Visible = False
        '
        'TxtNoOffpresentOnweekOff
        '
        Me.TxtNoOffpresentOnweekOff.EditValue = "3"
        Me.TxtNoOffpresentOnweekOff.Location = New System.Drawing.Point(223, 10)
        Me.TxtNoOffpresentOnweekOff.Name = "TxtNoOffpresentOnweekOff"
        Me.TxtNoOffpresentOnweekOff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtNoOffpresentOnweekOff.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtNoOffpresentOnweekOff.Properties.Appearance.Options.UseFont = True
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtNoOffpresentOnweekOff.Properties.MaxLength = 1
        Me.TxtNoOffpresentOnweekOff.Size = New System.Drawing.Size(72, 20)
        Me.TxtNoOffpresentOnweekOff.TabIndex = 1
        Me.TxtNoOffpresentOnweekOff.Visible = False
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(3, 247)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl41.TabIndex = 41
        Me.LabelControl41.Text = "Leave as per Financial Year"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(3, 222)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(114, 14)
        Me.LabelControl40.TabIndex = 40
        Me.LabelControl40.Text = "Download at Startup"
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(3, 173)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl39.TabIndex = 39
        Me.LabelControl39.Text = "Cloud Events"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(2, 147)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(203, 14)
        Me.LabelControl22.TabIndex = 31
        Me.LabelControl22.Text = "Out Work Minus From Working Hours"
        Me.LabelControl22.Visible = False
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(2, 122)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl26.TabIndex = 30
        Me.LabelControl26.Text = "Mark MIS as Absent"
        Me.LabelControl26.Visible = False
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(1, 16)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(120, 14)
        Me.LabelControl27.TabIndex = 16
        Me.LabelControl27.Text = "No of Present for WO"
        Me.LabelControl27.Visible = False
        '
        'LabelControlMarkWO
        '
        Me.LabelControlMarkWO.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlMarkWO.Appearance.Options.UseFont = True
        Me.LabelControlMarkWO.Location = New System.Drawing.Point(2, 83)
        Me.LabelControlMarkWO.Name = "LabelControlMarkWO"
        Me.LabelControlMarkWO.Size = New System.Drawing.Size(352, 14)
        Me.LabelControlMarkWO.TabIndex = 28
        Me.LabelControlMarkWO.Text = "Mark WO as Absent when No of Present<No of Present for WO"
        Me.LabelControlMarkWO.Visible = False
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(2, 37)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(130, 14)
        Me.LabelControl34.TabIndex = 25
        Me.LabelControl34.Text = "Is Auto Absent Allowed"
        Me.LabelControl34.Visible = False
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(2, 60)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl38.TabIndex = 27
        Me.LabelControl38.Text = "Mark AWA as AAA"
        Me.LabelControl38.Visible = False
        '
        'ToggleSkipPageOnDept
        '
        Me.ToggleSkipPageOnDept.EditValue = True
        Me.ToggleSkipPageOnDept.Location = New System.Drawing.Point(816, 483)
        Me.ToggleSkipPageOnDept.Name = "ToggleSkipPageOnDept"
        Me.ToggleSkipPageOnDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSkipPageOnDept.Properties.Appearance.Options.UseFont = True
        Me.ToggleSkipPageOnDept.Properties.OffText = "No"
        Me.ToggleSkipPageOnDept.Properties.OnText = "Yes"
        Me.ToggleSkipPageOnDept.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSkipPageOnDept.TabIndex = 51
        Me.ToggleSkipPageOnDept.Visible = False
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(593, 488)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(141, 14)
        Me.LabelControl31.TabIndex = 13
        Me.LabelControl31.Text = "Skip Page on Department"
        Me.LabelControl31.Visible = False
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.TxtMsg)
        Me.GroupControl5.Controls.Add(Me.TxtPwd)
        Me.GroupControl5.Controls.Add(Me.TxtUserId)
        Me.GroupControl5.Controls.Add(Me.LabelControl50)
        Me.GroupControl5.Controls.Add(Me.LabelControl51)
        Me.GroupControl5.Controls.Add(Me.LabelControl52)
        Me.GroupControl5.Location = New System.Drawing.Point(806, 551)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(342, 109)
        Me.GroupControl5.TabIndex = 7
        Me.GroupControl5.Text = "SMS Setting"
        Me.GroupControl5.Visible = False
        '
        'TxtMsg
        '
        Me.TxtMsg.Location = New System.Drawing.Point(75, 76)
        Me.TxtMsg.Name = "TxtMsg"
        Me.TxtMsg.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMsg.Properties.Appearance.Options.UseFont = True
        Me.TxtMsg.Properties.MaxLength = 255
        Me.TxtMsg.Size = New System.Drawing.Size(229, 20)
        Me.TxtMsg.TabIndex = 72
        '
        'TxtPwd
        '
        Me.TxtPwd.Location = New System.Drawing.Point(167, 51)
        Me.TxtPwd.Name = "TxtPwd"
        Me.TxtPwd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPwd.Properties.Appearance.Options.UseFont = True
        Me.TxtPwd.Properties.MaxLength = 25
        Me.TxtPwd.Size = New System.Drawing.Size(137, 20)
        Me.TxtPwd.TabIndex = 71
        '
        'TxtUserId
        '
        Me.TxtUserId.Location = New System.Drawing.Point(167, 25)
        Me.TxtUserId.Name = "TxtUserId"
        Me.TxtUserId.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtUserId.Properties.Appearance.Options.UseFont = True
        Me.TxtUserId.Properties.MaxLength = 25
        Me.TxtUserId.Size = New System.Drawing.Size(137, 20)
        Me.TxtUserId.TabIndex = 70
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl50.Appearance.Options.UseFont = True
        Me.LabelControl50.Location = New System.Drawing.Point(6, 79)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl50.TabIndex = 69
        Me.LabelControl50.Text = "Message"
        '
        'LabelControl51
        '
        Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl51.Appearance.Options.UseFont = True
        Me.LabelControl51.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl51.TabIndex = 64
        Me.LabelControl51.Text = "Password"
        '
        'LabelControl52
        '
        Me.LabelControl52.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl52.Appearance.Options.UseFont = True
        Me.LabelControl52.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl52.TabIndex = 62
        Me.LabelControl52.Text = "User ID"
        '
        'ToggleOverStayAllowed
        '
        Me.ToggleOverStayAllowed.Location = New System.Drawing.Point(248, 806)
        Me.ToggleOverStayAllowed.Name = "ToggleOverStayAllowed"
        Me.ToggleOverStayAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOverStayAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleOverStayAllowed.Properties.OffText = "No"
        Me.ToggleOverStayAllowed.Properties.OnText = "Yes"
        Me.ToggleOverStayAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOverStayAllowed.TabIndex = 46
        Me.ToggleOverStayAllowed.Visible = False
        '
        'ToggleOutWorkAllowed
        '
        Me.ToggleOutWorkAllowed.Location = New System.Drawing.Point(248, 781)
        Me.ToggleOutWorkAllowed.Name = "ToggleOutWorkAllowed"
        Me.ToggleOutWorkAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOutWorkAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleOutWorkAllowed.Properties.OffText = "No"
        Me.ToggleOutWorkAllowed.Properties.OnText = "Yes"
        Me.ToggleOutWorkAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOutWorkAllowed.TabIndex = 45
        Me.ToggleOutWorkAllowed.Visible = False
        '
        'ToggleOverTimeAllowed
        '
        Me.ToggleOverTimeAllowed.Location = New System.Drawing.Point(248, 757)
        Me.ToggleOverTimeAllowed.Name = "ToggleOverTimeAllowed"
        Me.ToggleOverTimeAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOverTimeAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleOverTimeAllowed.Properties.OffText = "No"
        Me.ToggleOverTimeAllowed.Properties.OnText = "Yes"
        Me.ToggleOverTimeAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOverTimeAllowed.TabIndex = 44
        Me.ToggleOverTimeAllowed.Visible = False
        '
        'TxtPermisEarlyDpt
        '
        Me.TxtPermisEarlyDpt.EditValue = "10"
        Me.TxtPermisEarlyDpt.Location = New System.Drawing.Point(245, 666)
        Me.TxtPermisEarlyDpt.Name = "TxtPermisEarlyDpt"
        Me.TxtPermisEarlyDpt.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisEarlyDpt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisEarlyDpt.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisEarlyDpt.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisEarlyDpt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisEarlyDpt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisEarlyDpt.Properties.MaxLength = 3
        Me.TxtPermisEarlyDpt.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisEarlyDpt.TabIndex = 43
        Me.TxtPermisEarlyDpt.Visible = False
        '
        'TxtPermisLateArr
        '
        Me.TxtPermisLateArr.EditValue = "10"
        Me.TxtPermisLateArr.Location = New System.Drawing.Point(245, 641)
        Me.TxtPermisLateArr.Name = "TxtPermisLateArr"
        Me.TxtPermisLateArr.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisLateArr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisLateArr.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisLateArr.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisLateArr.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisLateArr.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisLateArr.Properties.MaxLength = 3
        Me.TxtPermisLateArr.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisLateArr.TabIndex = 42
        Me.TxtPermisLateArr.Visible = False
        '
        'TxtMaxWorkDur
        '
        Me.TxtMaxWorkDur.EditValue = "1140"
        Me.TxtMaxWorkDur.Location = New System.Drawing.Point(245, 615)
        Me.TxtMaxWorkDur.Name = "TxtMaxWorkDur"
        Me.TxtMaxWorkDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxWorkDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxWorkDur.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxWorkDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxWorkDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxWorkDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxWorkDur.Properties.MaxLength = 4
        Me.TxtMaxWorkDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxWorkDur.TabIndex = 38
        Me.TxtMaxWorkDur.Visible = False
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(23, 615)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(120, 14)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Max Working Duration"
        Me.LabelControl19.Visible = False
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(22, 672)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(121, 14)
        Me.LabelControl20.TabIndex = 3
        Me.LabelControl20.Text = "Permissable Early Dep "
        Me.LabelControl20.Visible = False
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(26, 762)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(102, 14)
        Me.LabelControl21.TabIndex = 5
        Me.LabelControl21.Text = "Over Time allowed"
        Me.LabelControl21.Visible = False
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Appearance.Options.UseTextOptions = True
        Me.LabelControl23.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LabelControl23.Location = New System.Drawing.Point(26, 786)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(101, 14)
        Me.LabelControl23.TabIndex = 7
        Me.LabelControl23.Text = "Out Work Allowed"
        Me.LabelControl23.Visible = False
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(22, 644)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(117, 14)
        Me.LabelControl25.TabIndex = 2
        Me.LabelControl25.Text = "Permisable Late Arival"
        Me.LabelControl25.Visible = False
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(27, 811)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl32.TabIndex = 9
        Me.LabelControl32.Text = "OverStay Allowed"
        Me.LabelControl32.Visible = False
        '
        'ToggleIsAtdUsedAsAccess
        '
        Me.ToggleIsAtdUsedAsAccess.Location = New System.Drawing.Point(644, 548)
        Me.ToggleIsAtdUsedAsAccess.Name = "ToggleIsAtdUsedAsAccess"
        Me.ToggleIsAtdUsedAsAccess.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsAtdUsedAsAccess.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsAtdUsedAsAccess.Properties.OffText = "No"
        Me.ToggleIsAtdUsedAsAccess.Properties.OnText = "Yes"
        Me.ToggleIsAtdUsedAsAccess.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsAtdUsedAsAccess.TabIndex = 50
        Me.ToggleIsAtdUsedAsAccess.Visible = False
        '
        'ToggleIsHelpApplicable
        '
        Me.ToggleIsHelpApplicable.Location = New System.Drawing.Point(644, 524)
        Me.ToggleIsHelpApplicable.Name = "ToggleIsHelpApplicable"
        Me.ToggleIsHelpApplicable.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsHelpApplicable.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsHelpApplicable.Properties.OffText = "No"
        Me.ToggleIsHelpApplicable.Properties.OnText = "Yes"
        Me.ToggleIsHelpApplicable.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsHelpApplicable.TabIndex = 49
        Me.ToggleIsHelpApplicable.Visible = False
        '
        'ToggleIsSmartMachine
        '
        Me.ToggleIsSmartMachine.Location = New System.Drawing.Point(644, 500)
        Me.ToggleIsSmartMachine.Name = "ToggleIsSmartMachine"
        Me.ToggleIsSmartMachine.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsSmartMachine.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsSmartMachine.Properties.OffText = "No"
        Me.ToggleIsSmartMachine.Properties.OnText = "Yes"
        Me.ToggleIsSmartMachine.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsSmartMachine.TabIndex = 48
        Me.ToggleIsSmartMachine.Visible = False
        '
        'ToggleIsInOutApplicable
        '
        Me.ToggleIsInOutApplicable.Location = New System.Drawing.Point(644, 476)
        Me.ToggleIsInOutApplicable.Name = "ToggleIsInOutApplicable"
        Me.ToggleIsInOutApplicable.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsInOutApplicable.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsInOutApplicable.Properties.OffText = "No"
        Me.ToggleIsInOutApplicable.Properties.OnText = "Yes"
        Me.ToggleIsInOutApplicable.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsInOutApplicable.TabIndex = 47
        Me.ToggleIsInOutApplicable.Visible = False
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(423, 553)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(188, 14)
        Me.LabelControl24.TabIndex = 12
        Me.LabelControl24.Text = "Is Attendance used as Access also"
        Me.LabelControl24.Visible = False
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(422, 529)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl28.TabIndex = 11
        Me.LabelControl28.Text = "Is Help Applicable"
        Me.LabelControl28.Visible = False
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(422, 481)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl29.TabIndex = 29
        Me.LabelControl29.Text = "Is In-Out Applicable"
        Me.LabelControl29.Visible = False
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(422, 505)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(93, 14)
        Me.LabelControl35.TabIndex = 10
        Me.LabelControl35.Text = "Is Smart Machine"
        Me.LabelControl35.Visible = False
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(426, 585)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl36.TabIndex = 14
        Me.LabelControl36.Text = "Lines Per Page"
        Me.LabelControl36.Visible = False
        '
        'TxtLinesPerPage
        '
        Me.TxtLinesPerPage.EditValue = "58"
        Me.TxtLinesPerPage.Location = New System.Drawing.Point(647, 582)
        Me.TxtLinesPerPage.Name = "TxtLinesPerPage"
        Me.TxtLinesPerPage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtLinesPerPage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLinesPerPage.Properties.Appearance.Options.UseFont = True
        Me.TxtLinesPerPage.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtLinesPerPage.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtLinesPerPage.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtLinesPerPage.Properties.MaxLength = 3
        Me.TxtLinesPerPage.Size = New System.Drawing.Size(72, 20)
        Me.TxtLinesPerPage.TabIndex = 52
        Me.TxtLinesPerPage.Visible = False
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.CheckEditOT3)
        Me.GroupControl1.Controls.Add(Me.CheckEditOT2)
        Me.GroupControl1.Controls.Add(Me.CheckEditOT1)
        Me.GroupControl1.Location = New System.Drawing.Point(748, 2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(342, 103)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = " OT Options"
        Me.GroupControl1.Visible = False
        '
        'CheckEditOT3
        '
        Me.CheckEditOT3.Location = New System.Drawing.Point(6, 76)
        Me.CheckEditOT3.Name = "CheckEditOT3"
        Me.CheckEditOT3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT3.Properties.Caption = "OT = Early Coming + Late Dep"
        Me.CheckEditOT3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT3.Properties.RadioGroupIndex = 0
        Me.CheckEditOT3.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT3.TabIndex = 2
        Me.CheckEditOT3.TabStop = False
        '
        'CheckEditOT2
        '
        Me.CheckEditOT2.EditValue = True
        Me.CheckEditOT2.Location = New System.Drawing.Point(6, 51)
        Me.CheckEditOT2.Name = "CheckEditOT2"
        Me.CheckEditOT2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT2.Properties.Caption = "OT = Working Hrs - ShiftHrs "
        Me.CheckEditOT2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT2.Properties.RadioGroupIndex = 0
        Me.CheckEditOT2.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT2.TabIndex = 1
        '
        'CheckEditOT1
        '
        Me.CheckEditOT1.Location = New System.Drawing.Point(6, 26)
        Me.CheckEditOT1.Name = "CheckEditOT1"
        Me.CheckEditOT1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT1.Properties.Caption = "OT = OutTime - ShiftEndTime"
        Me.CheckEditOT1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT1.Properties.RadioGroupIndex = 0
        Me.CheckEditOT1.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT1.TabIndex = 0
        Me.CheckEditOT1.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.ToggleRoundOverTime)
        Me.GroupControl2.Controls.Add(Me.LabelControl44)
        Me.GroupControl2.Controls.Add(Me.ToggleWhetherOTinMinus)
        Me.GroupControl2.Controls.Add(Me.LabelControl43)
        Me.GroupControl2.Controls.Add(Me.ToggleOTisAllowedInEarlyComing)
        Me.GroupControl2.Controls.Add(Me.LabelControl42)
        Me.GroupControl2.Location = New System.Drawing.Point(748, 111)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(342, 106)
        Me.GroupControl2.TabIndex = 4
        Me.GroupControl2.Text = "OT Parameter Options"
        Me.GroupControl2.Visible = False
        '
        'ToggleRoundOverTime
        '
        Me.ToggleRoundOverTime.Location = New System.Drawing.Point(232, 80)
        Me.ToggleRoundOverTime.Name = "ToggleRoundOverTime"
        Me.ToggleRoundOverTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleRoundOverTime.Properties.Appearance.Options.UseFont = True
        Me.ToggleRoundOverTime.Properties.OffText = "No"
        Me.ToggleRoundOverTime.Properties.OnText = "Yes"
        Me.ToggleRoundOverTime.Size = New System.Drawing.Size(95, 25)
        Me.ToggleRoundOverTime.TabIndex = 3
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl44.Appearance.Options.UseFont = True
        Me.LabelControl44.Location = New System.Drawing.Point(6, 81)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl44.TabIndex = 66
        Me.LabelControl44.Text = "Round Over Time"
        '
        'ToggleWhetherOTinMinus
        '
        Me.ToggleWhetherOTinMinus.Location = New System.Drawing.Point(232, 55)
        Me.ToggleWhetherOTinMinus.Name = "ToggleWhetherOTinMinus"
        Me.ToggleWhetherOTinMinus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleWhetherOTinMinus.Properties.Appearance.Options.UseFont = True
        Me.ToggleWhetherOTinMinus.Properties.OffText = "No"
        Me.ToggleWhetherOTinMinus.Properties.OnText = "Yes"
        Me.ToggleWhetherOTinMinus.Size = New System.Drawing.Size(95, 25)
        Me.ToggleWhetherOTinMinus.TabIndex = 2
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl43.Appearance.Options.UseFont = True
        Me.LabelControl43.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(185, 14)
        Me.LabelControl43.TabIndex = 64
        Me.LabelControl43.Text = "Whether OT in Minus ( - ) Figures"
        '
        'ToggleOTisAllowedInEarlyComing
        '
        Me.ToggleOTisAllowedInEarlyComing.Location = New System.Drawing.Point(232, 29)
        Me.ToggleOTisAllowedInEarlyComing.Name = "ToggleOTisAllowedInEarlyComing"
        Me.ToggleOTisAllowedInEarlyComing.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOTisAllowedInEarlyComing.Properties.Appearance.Options.UseFont = True
        Me.ToggleOTisAllowedInEarlyComing.Properties.OffText = "No"
        Me.ToggleOTisAllowedInEarlyComing.Properties.OnText = "Yes"
        Me.ToggleOTisAllowedInEarlyComing.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOTisAllowedInEarlyComing.TabIndex = 1
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(197, 14)
        Me.LabelControl42.TabIndex = 62
        Me.LabelControl42.Text = "OT is allowed incase of early coming"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.TxtDeductOTinWO)
        Me.GroupControl3.Controls.Add(Me.TxtDeductOTinHLD)
        Me.GroupControl3.Controls.Add(Me.LabelControl46)
        Me.GroupControl3.Controls.Add(Me.LabelControl47)
        Me.GroupControl3.Location = New System.Drawing.Point(748, 223)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(342, 85)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "OT Deductions"
        Me.GroupControl3.Visible = False
        '
        'TxtDeductOTinWO
        '
        Me.TxtDeductOTinWO.EditValue = "0"
        Me.TxtDeductOTinWO.Location = New System.Drawing.Point(232, 50)
        Me.TxtDeductOTinWO.Name = "TxtDeductOTinWO"
        Me.TxtDeductOTinWO.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDeductOTinWO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDeductOTinWO.Properties.Appearance.Options.UseFont = True
        Me.TxtDeductOTinWO.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDeductOTinWO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDeductOTinWO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDeductOTinWO.Properties.MaxLength = 2
        Me.TxtDeductOTinWO.Size = New System.Drawing.Size(72, 20)
        Me.TxtDeductOTinWO.TabIndex = 2
        '
        'TxtDeductOTinHLD
        '
        Me.TxtDeductOTinHLD.EditValue = "0"
        Me.TxtDeductOTinHLD.Location = New System.Drawing.Point(232, 24)
        Me.TxtDeductOTinHLD.Name = "TxtDeductOTinHLD"
        Me.TxtDeductOTinHLD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDeductOTinHLD.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDeductOTinHLD.Properties.Appearance.Options.UseFont = True
        Me.TxtDeductOTinHLD.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDeductOTinHLD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDeductOTinHLD.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDeductOTinHLD.Properties.MaxLength = 2
        Me.TxtDeductOTinHLD.Size = New System.Drawing.Size(72, 20)
        Me.TxtDeductOTinHLD.TabIndex = 1
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl46.TabIndex = 64
        Me.LabelControl46.Text = "Deduct OT on WO"
        '
        'LabelControl47
        '
        Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl47.Appearance.Options.UseFont = True
        Me.LabelControl47.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl47.TabIndex = 62
        Me.LabelControl47.Text = "Deduct OT in HLD"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.TxtOtRestrictDur)
        Me.GroupControl4.Controls.Add(Me.LabelControl49)
        Me.GroupControl4.Controls.Add(Me.TxtOtLateDur)
        Me.GroupControl4.Controls.Add(Me.TxtOtEarlyDur)
        Me.GroupControl4.Controls.Add(Me.LabelControl45)
        Me.GroupControl4.Controls.Add(Me.LabelControl48)
        Me.GroupControl4.Location = New System.Drawing.Point(748, 314)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(342, 111)
        Me.GroupControl4.TabIndex = 6
        Me.GroupControl4.Text = "OT Durations"
        Me.GroupControl4.Visible = False
        '
        'TxtOtRestrictDur
        '
        Me.TxtOtRestrictDur.EditValue = "0"
        Me.TxtOtRestrictDur.Location = New System.Drawing.Point(232, 76)
        Me.TxtOtRestrictDur.Name = "TxtOtRestrictDur"
        Me.TxtOtRestrictDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtRestrictDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtRestrictDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtRestrictDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtRestrictDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtRestrictDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtRestrictDur.Properties.MaxLength = 3
        Me.TxtOtRestrictDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtRestrictDur.TabIndex = 3
        '
        'LabelControl49
        '
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl49.Appearance.Options.UseFont = True
        Me.LabelControl49.Location = New System.Drawing.Point(6, 79)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl49.TabIndex = 69
        Me.LabelControl49.Text = "OT Restrict End Duration"
        '
        'TxtOtLateDur
        '
        Me.TxtOtLateDur.EditValue = "0"
        Me.TxtOtLateDur.Location = New System.Drawing.Point(232, 50)
        Me.TxtOtLateDur.Name = "TxtOtLateDur"
        Me.TxtOtLateDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtLateDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtLateDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtLateDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtLateDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtLateDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtLateDur.Properties.MaxLength = 3
        Me.TxtOtLateDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtLateDur.TabIndex = 2
        '
        'TxtOtEarlyDur
        '
        Me.TxtOtEarlyDur.EditValue = "0"
        Me.TxtOtEarlyDur.Location = New System.Drawing.Point(232, 24)
        Me.TxtOtEarlyDur.Name = "TxtOtEarlyDur"
        Me.TxtOtEarlyDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtEarlyDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtEarlyDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtEarlyDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtEarlyDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtEarlyDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtEarlyDur.Properties.MaxLength = 3
        Me.TxtOtEarlyDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtEarlyDur.TabIndex = 1
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl45.TabIndex = 64
        Me.LabelControl45.Text = "OT Late Coming Duration"
        '
        'LabelControl48
        '
        Me.LabelControl48.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl48.Appearance.Options.UseFont = True
        Me.LabelControl48.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(140, 14)
        Me.LabelControl48.TabIndex = 62
        Me.LabelControl48.Text = "OT Early Coming Duration"
        '
        'XtraTimeOfficePolicyEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1104, 461)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.TxtMaxWorkHourForShortDay)
        Me.Controls.Add(Me.TxtLinesPerPage)
        Me.Controls.Add(Me.TxtMaxWorkHourForHalfDay)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.ToggleOverStayAllowed)
        Me.Controls.Add(Me.TxtPresentMarkingDur)
        Me.Controls.Add(Me.ToggleOutWorkAllowed)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.ToggleOverTimeAllowed)
        Me.Controls.Add(Me.ToggleShortleaveMarking)
        Me.Controls.Add(Me.TxtPermisEarlyDpt)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.TxtPermisLateArr)
        Me.Controls.Add(Me.ToggleHalfDayMarking)
        Me.Controls.Add(Me.LabelControl36)
        Me.Controls.Add(Me.ToggleSkipPageOnDept)
        Me.Controls.Add(Me.LabelControl29)
        Me.Controls.Add(Me.LabelControl35)
        Me.Controls.Add(Me.TxtMaxWorkDur)
        Me.Controls.Add(Me.LabelControl28)
        Me.Controls.Add(Me.LabelControl19)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.LabelControl31)
        Me.Controls.Add(Me.ToggleIsAtdUsedAsAccess)
        Me.Controls.Add(Me.LabelControl21)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.ToggleIsHelpApplicable)
        Me.Controls.Add(Me.LabelControl23)
        Me.Controls.Add(Me.ToggleIsInOutApplicable)
        Me.Controls.Add(Me.LabelControl25)
        Me.Controls.Add(Me.ToggleIsSmartMachine)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl32)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl12)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraTimeOfficePolicyEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.ToggleFourPunchNight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSetRegNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsPResentOnHLD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsPresentOnWeekOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleWeekOffIncludeInDUtyRoster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisLateMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisEarlyMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleAutoShiftAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxLateArrivalMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxWorkingMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEndTimeForOutPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtInTimeForINPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDuplicateCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxWorkHourForShortDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxWorkHourForHalfDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPresentMarkingDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleShortleaveMarking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleHalfDayMarking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOutWorkMins.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkMisAsAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkWOasAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkAWAasAAA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsAutoAbsentAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtNoOffpresentOnweekOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSkipPageOnDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.TxtMsg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUserId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOverStayAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOutWorkAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOverTimeAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisEarlyDpt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisLateArr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxWorkDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsAtdUsedAsAccess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsHelpApplicable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsSmartMachine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsInOutApplicable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLinesPerPage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.CheckEditOT3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOT2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOT1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.ToggleRoundOverTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleWhetherOTinMinus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOTisAllowedInEarlyComing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.TxtDeductOTinWO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDeductOTinHLD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.TxtOtRestrictDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtLateDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtEarlyDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleFourPunchNight As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSetRegNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlETOP As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TxtDuplicateCheck As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxWorkingMin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtEndTimeForOutPunch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtInTimeForINPunch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxEarlyDeparture As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxLateArrivalMin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleShortleaveMarking As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleHalfDayMarking As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtPresentMarkingDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleIsPResentOnHLD As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleIsPresentOnWeekOff As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleWeekOffIncludeInDUtyRoster As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtPermisLateMinAutoShift As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPermisEarlyMinAutoShift As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleAutoShiftAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtMaxWorkHourForShortDay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxWorkHourForHalfDay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMaxWorkDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlMarkWO As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtPermisEarlyDpt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPermisLateArr As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleIsAtdUsedAsAccess As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleIsHelpApplicable As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleIsSmartMachine As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleIsInOutApplicable As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOverStayAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOutWorkAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOverTimeAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSkipPageOnDept As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtLinesPerPage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtNoOffpresentOnweekOff As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleIsAutoAbsentAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkWOasAbsent As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkAWAasAAA As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleLeaveAsPerFinancialYear As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleDownloadAtStartUp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOnlineEvents As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOutWorkMins As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkMisAsAbsent As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditOT3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOT2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOT1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ToggleRoundOverTime As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleWhetherOTinMinus As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleOTisAllowedInEarlyComing As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtDeductOTinWO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtDeductOTinHLD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TxtOtRestrictDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtOtLateDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtOtEarlyDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TxtMsg As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPwd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtUserId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditZKPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBioPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchVisitor As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchCanteen As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchIsNepali As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleRealTime As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTimerDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextTWIR102Port As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditAutoDwnDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
End Class
