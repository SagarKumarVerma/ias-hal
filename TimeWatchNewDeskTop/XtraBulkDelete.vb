﻿Imports DevExpress.LookAndFeel
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraBulkDelete
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'con1 = New OleDbConnection(ConnectionString)
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridViewComp, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewLocation, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraBulkDelete_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GridControlEmp.DataSource = Common.EmpNonAdmin
        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1
            GridControlLocation.DataSource = SSSDBDataSet.tblbranch1
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1
        Else
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment
            GridControlLocation.DataSource = SSSDBDataSet.tblbranch
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee
        End If

        'If CheckEdit5.Checked = True Then
        CheckEdit5.Checked = True
        LabelControl5.Visible = False
        LabelControl7.Visible = False
        ComboBoxEdit1.Visible = False
        PopupContainerEdit1.Visible = False
        'End If
        If Common.USERTYPE <> "A" Then
            CheckEdit6.Checked = True
            CheckEdit5.Visible = False
            ComboBoxEdit1.Properties.Items.Remove("Company")
            ComboBoxEdit1.Properties.Items.Remove("Department")
            ComboBoxEdit1.Properties.Items.Remove("Location")
        End If
    End Sub
    Private Sub CheckEdit5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit5.CheckedChanged
        If CheckEdit5.Checked = True Then
            LabelControl5.Visible = False
            LabelControl7.Visible = False
            ComboBoxEdit1.Visible = False
            PopupContainerEdit1.Visible = False
        End If
    End Sub
    Private Sub CheckEdit6_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit6.CheckedChanged
        If CheckEdit6.Checked = True Then
            ComboBoxEdit1.EditValue = "Paycode"
            LabelControl5.Visible = True
            LabelControl7.Visible = True
            ComboBoxEdit1.Visible = True
            PopupContainerEdit1.Visible = True
            selectpopupdropdown()
        End If
    End Sub
    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        selectpopupdropdown()
    End Sub
    Private Sub selectpopupdropdown()
        PopupContainerEdit1.EditValue = ""
        If ComboBoxEdit1.EditValue = "Paycode" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Paycode"
            'TextEdit3.Visible = True
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlEmp
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Company"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlCompany
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Location"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlLocation
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Department"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlDept
        End If
    End Sub

    Private Sub SimpleDelete_Click(sender As System.Object, e As System.EventArgs) Handles SimpleDelete.Click
        Me.Cursor = Cursors.WaitCursor
        Dim emp As String = "delete  from TblEmployee"
        Dim empShft As String = "delete from tblEmployeeShiftMaster"
        Dim empLeav As String = "delete from tblLeaveLedger"
        Dim empTimeReg As String = "delete from tblTimeRegister"
        Dim empMAchin As String = "delete from MachineRawPunch"
        Dim empLeaveApp As String = "delete from LeaveApplication"
        Dim PayMaster As String = "DELETE from Pay_Master"
        Dim PAYRESULT As String = "DELETE from PAY_RESULT"
        Dim fptable As String = "DELETE from fptable"

        If CheckEdit5.Checked = True Then
            XtraMasterTest.LabelControlStatus.Text = "Deleting All Employee"
            Application.DoEvents()

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(emp, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(empShft, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(empLeav, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(empTimeReg, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(empMAchin, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(empLeaveApp, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(PayMaster, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(PAYRESULT, Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(fptable, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(emp, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(empShft, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(empLeav, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(empTimeReg, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(empMAchin, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(empLeaveApp, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(PayMaster, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(PAYRESULT, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(fptable, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            If Directory.Exists("./EmpImages") Then
                'Delete all files from the Directory
                For Each filepath As String In Directory.GetFiles("./EmpImages")
                    File.Delete(filepath)
                Next
            End If
        Else
            Dim sSql As String
            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                For i As Integer = 0 To paycode.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Deleting Employee " & paycode(i).Trim
                    Application.DoEvents()
                    emp = "delete  from TblEmployee where paycode='" & paycode(i).Trim & "'"
                    empShft = "delete from tblEmployeeShiftMaster  where paycode='" & paycode(i).Trim & "'"
                    empLeav = "delete from tblLeaveLedger  where paycode='" & paycode(i).Trim & "'"
                    empTimeReg = "delete from tblTimeRegister  where paycode='" & paycode(i).Trim & "'"
                    empMAchin = "delete from MachineRawPunch  where paycode='" & paycode(i).Trim & "'"
                    empLeaveApp = "delete from LeaveApplication where paycode='" & paycode(i).Trim & "'"
                    PayMaster = "DELETE from Pay_Master where paycode='" & paycode(i).Trim & "'"
                    PAYRESULT = "DELETE from PAY_RESULT where paycode='" & paycode(i).Trim & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(emp, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(empShft, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(empLeav, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(empTimeReg, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(empMAchin, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(empLeaveApp, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(PayMaster, Common.con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(PAYRESULT, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(emp, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(empShft, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(empLeav, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(empTimeReg, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(empMAchin, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(empLeaveApp, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(PayMaster, Common.con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(PAYRESULT, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                    If System.IO.File.Exists("./EmpImages/" & paycode(i).Trim & ".jpg") = True Then
                        My.Computer.FileSystem.DeleteFile("./EmpImages/" & paycode(i).Trim & ".jpg")
                    End If
                Next
                GoTo tmp
            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                sSql = "select PAYCODE  from TblEmployee where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "') "
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If

            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select PAYCODE from TblEmployee where DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "') "
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select PAYCODE from TblEmployee where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            End If


            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlStatus.Text = "Deleting Employee " & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                Application.DoEvents()
                emp = "delete  from TblEmployee where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                empShft = "delete from tblEmployeeShiftMaster  where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                empLeav = "delete from tblLeaveLedger  where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                empTimeReg = "delete from tblTimeRegister  where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                empMAchin = "delete from MachineRawPunch  where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                empLeaveApp = "delete from LeaveApplication where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                PayMaster = "DELETE from Pay_Master where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                PAYRESULT = "DELETE from PAY_RESULT where paycode='" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "'"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(emp, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(empShft, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(empLeav, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(empTimeReg, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(empMAchin, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(empLeaveApp, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(PayMaster, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(PAYRESULT, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(emp, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(empShft, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(empLeav, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(empTimeReg, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(empMAchin, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(empLeaveApp, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(PayMaster, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(PAYRESULT, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                If System.IO.File.Exists("./EmpImages/" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & ".jpg") = True Then
                    My.Computer.FileSystem.DeleteFile("./EmpImages/" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & ".jpg")
                End If
            Next

        End If
tmp:    XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()

        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Delete Success</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewEmp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("PAYCODE", text)
                    GridViewEmp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewComp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("COMPANYCODE", text)
                    GridViewComp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewLocation.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("BRANCHCODE", text)
                    GridViewLocation.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewDept.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("DEPARTMENTCODE", text)
                    GridViewDept.SelectRow(rowHandle)
                Next
            End If
        End If

    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("Paycode"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("COMPANYCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim selectedRows() As Integer = GridViewLocation.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewLocation.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("BRANCHCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("DEPARTMENTCODE"))
            Next
            e.Value = sb.ToString
        End If

    End Sub
End Class